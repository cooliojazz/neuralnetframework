kernel void multiplyVector(global const double* matrix, global const double* vector, global double* result, int width, int total) {
    int cur = get_global_id(0);
    if (cur >= total) return;

    // TODO: Is grabbing this range better than just calculating the whole index at once? It probably would be cleaner, but does it add register pressure? I also feel like all the examples I was looking at just use 2d indexing instead though, so idk, maybe there's something else I'm missing.
    //global const double* a = &matrix[cur * width];
    result[cur] = 0;
    for (int i = 0; i < width; i++) {
        result[cur] += matrix[cur * width + i] * vector[i];
    }
}

kernel void multiplyMatrix(global const double* matrixA, global const double* matrixB, global double* result, int widthA, int widthB) {
    int col = get_global_id(0);
    int row = get_global_id(1);

    result[col * widthA + row] = 0;
    for (int i = 0; i < widthA; i++) {
        result[col * widthA + row] += matrixA[col * widthA + i] * matrixB[i * widthB + row];
    }
}