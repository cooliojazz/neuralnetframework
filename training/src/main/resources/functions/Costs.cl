double maeCost(global const double* xs, global const double* as, int length) {
    double ret = 0;
    for (int i = 0; i < length; i++) {
        ret += fabs(as[i] - xs[i]);
    }
    return ret / length;
}

double maeCostDerivative(double x, double a) {
    return x > 0 ? 1 : -1;
}

double mseCost(global const double* xs, global const double* as, int length) {
    double ret = 0;
    for (int i = 0; i < length; i++) {
        ret += pow(as[i] - xs[i], 2) / 2;
    }
    return ret / length;
}

double mseCostDerivative(double x, double a) {
    return x - a;
}


double crossEntropyCost(global const double* xs, global const double* as, int length) {
    double ret = 0;
    for (int i = 0; i < length; i++) {
        ret += log(xs[i]) * as[i];
    }
    return -ret;
}

double crossEntropyCostDerivative(double x, double a) {
    return -a / x;
}
	