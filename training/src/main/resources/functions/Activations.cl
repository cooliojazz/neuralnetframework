double getSigmoid(double xs) {
    return 1 / (1 + exp(-xs));
}

double getSimpleDerivativeSigmoid(double xs) {
    return exp(xs) / pow(1 + exp(xs), 2);
}

kernel void sigmoidActivationMany(global const double* vector, global double* result, int size, int layerOffset, int total) {
    int cur = get_global_id(0);
    if (cur >= total) return;

    for (int i = 0; i < size; i++) {
        result[layerOffset + cur * size + i] = getSigmoid(vector[cur * size + i]);
    }
}

kernel void sigmoidActivationDerivativeMany(global const double* vector, global double* result, int size, int layerOffset, int total) {
    int cur = get_global_id(0);
    if (cur >= total) return;

    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            result[layerOffset + cur * size * size + i * size + j] = (i == j ? getSimpleDerivativeSigmoid(vector[cur * size + i]) : 0);
        }
    }
}


double getSwish(double xs, double beta) {
    return xs / (1 + exp(-beta * xs));
}

double getSimpleDerivativeSwish(double xs, double beta) {
    return exp(beta * xs) * (1 + exp(beta * xs) + beta * xs) / pow(1 + exp(beta * xs), 2);
}

kernel void swishActivationMany(global const double* vector, global double* result, int size, int layerOffset, int total, double beta) {
    int cur = get_global_id(0);
    if (cur >= total) return;

    for (int i = 0; i < size; i++) {
        result[layerOffset + cur * size + i] = getSwish(vector[cur * size + i], beta);
    }
}

// Can currently access activations at result[layerOffset - total * size] if needed. Feels a bit strange, but preferable for now to doing the opposite offset to set the derivative in every other location.
// Maybe the derivative should just be merged into one call with the activation now? Still reasons not to though so I'm not sure.
kernel void swishActivationDerivativeMany(global const double* vector, global double* result, int size, int layerOffset, int total, double beta) {
    int cur = get_global_id(0);
    if (cur >= total) return;

    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            result[layerOffset + cur * size * size + i * size + j] = (i == j ? getSimpleDerivativeSwish(vector[cur * size + i], beta) : 0);
        }
    }
}


double getSoftmax(double xs, double total) {
    return exp(xs) / total;
}

kernel void softmaxActivationMany(global const double* vector, global double* result, int size, int layerOffset, int total) {
    int cur = get_global_id(0);
    if (cur >= total) return;

    double sum = 0;
    for (int i = 0; i < size; i++) {
        sum += exp(vector[cur * size + i]);
    }
    
    if (total != 0) {
        for (int i = 0; i < size; i++) {
            result[layerOffset + cur * size + i] = getSoftmax(vector[cur * size + i], sum);
        }
    }
}

kernel void softmaxActivationDerivativeMany(global const double* vector, global double* result, int size, int layerOffset, int total) {
    int cur = get_global_id(0);
    if (cur >= total) return;

    double sum = 0;
    for (int i = 0; i < size; i++) {
        sum += exp(vector[cur * size + i]);
    }
    
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            result[layerOffset + cur * size * size + i * size + j] = getSoftmax(vector[cur * size + j], sum) * ((i == j ? 1 : 0) - getSoftmax(vector[cur * size + i], sum));
        }
    }
}