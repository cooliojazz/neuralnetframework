#pragma OPENCL EXTENSION cl_khr_int64_base_atomics : enable

#include 'functions/Activations.cl'
#include 'functions/Costs.cl'

// TODO: Rename these
kernel void biasedMultiplyVector(global const double* weightMatrix, global const double* biases, global const double* vector, global double* result, int width, int total) {
    int cur = get_global_id(0);
    if (cur >= total) return;

    result[cur] = biases[cur];
    for (int i = 0; i < width; i++) {
        result[cur] += weightMatrix[cur * width + i] * vector[i];
    }
}

// TODO: I think to maximise efficiency this should be converted to a 2d range kernel. It also doesn't really matter for now though because it is by far the fastest kernel.
kernel void biasedMultiplyVectorMany(global const double* weightMatrix, global const double* biases, global const double* vector, global double* result, int width, int size, int total) {
    int cur = get_global_id(0);
    if (cur >= total) return;

    result[cur] = biases[cur % size];
    for (int i = 0; i < width; i++) {
        result[cur] += weightMatrix[(cur % size) * width + i] * vector[(cur / size) * width + i];
//        printf("%d|%d: %d, %f\n", size, cur, (cur / size) * width + i, vector[(cur / size) * width + i]);
    }
//    printf("%d: %f\n", cur, result[cur]);
}



// Since the equivalent base OpenCL function requires a very new extension, this patch makes it a bit more accessible
void atomic_add_double(volatile global double* addr, double val) {
    union {
            unsigned long u64;
            double d64;
        } next, expected, current;
    current.d64 = *addr;
    do {
        expected.d64 = current.d64;
        next.d64 = expected.d64 + val;
        current.u64 = atomic_cmpxchg((volatile global unsigned long*)addr, expected.u64, next.u64);
    } while (current.u64 != expected.u64);
}
// BUT ALSO JUST NEVER USE THIS, ITS VERY BAD AND SLOW


kernel void costError(global const double* last, global const double* act, global double* dCost, int size, int layerOffset, int func, int total) {
    int cur = get_global_id(0);
    if (cur >= total) return;

    // TODO: Split into smaller kernel
    for (int i = 0; i < size; i++) {
//        dCost[cur * size + i] = mseCostDerivative(last[layerOffset + cur * size + i], act[cur * size + i]);
//        dCost[cur * size + i] = crossEntropyCostDerivative(last[layerOffset + cur * size + i], act[cur * size + i]);
        switch (func) {
            case 0:
                dCost[cur * size + i] = maeCostDerivative(last[layerOffset + cur * size + i], act[cur * size + i]);
                break;
            case 1:
                dCost[cur * size + i] = mseCostDerivative(last[layerOffset + cur * size + i], act[cur * size + i]);
                break;
            case 2:
                dCost[cur * size + i] = crossEntropyCostDerivative(last[layerOffset + cur * size + i], act[cur * size + i]);
                break;
        }
    }
}

kernel void neuronError(global const double* dLast, global const double* inError, global double* outError, int size, int layerOffset, int total) {
    int cur = get_global_id(0);
    if (cur >= total) return;

    // TODO: Split into smaller kernel?
    for (int n = 0; n < size; n++) {
        outError[cur * size + n] = 0;
        for (int j = 0; j < size; j++) {
            // TODO: Check order of indices of Jacobian; see other TODO.
            outError[cur * size + n] += dLast[layerOffset + total * size + cur * size * size + n * size + j] * inError[cur * size + j];
        }
    }
}

kernel void inputError(global const double* weightMatrix, global const double* inError, global double* outError, int size, int inSize, int total) {
    int cur = get_global_id(0);
    if (cur >= total) return;

    // TODO: Split into smaller kernel?
    for (int i = 0; i < inSize; i++) {
        outError[cur * inSize + i] = 0;
        for (int n = 0; n < size; n++) {
            outError[cur * inSize + i] += weightMatrix[n * inSize + i] * inError[cur * size + n];
        }
    }
}

kernel void updateWeightChanges(global const double* lastError, global double* input, global double* layerNetCvals, int size, int inSize, int ncvsLength, int inputOffset, int total) {
    int cur = get_global_id(0);
    if (cur >= total) return;

    for (int n = 0; n < size; n++) {
        for (int i = 0; i < inSize; i++) {
            layerNetCvals[cur * ncvsLength + size + n * inSize + i] = input[inputOffset + cur * inSize + i] * lastError[cur * size + n];
        }
    }
}

kernel void updateBiasChanges(global const double* lastError, global double* layerNetCvals, int size, int ncvsLength, int total) {
    int cur = get_global_id(0);
    if (cur >= total) return;

    for (int n = 0; n < size; n++) {
        layerNetCvals[cur * ncvsLength + n] = lastError[cur * size + n];
    }
}



double diffMag(const global double* a, const global double* b, int length) {
    double sum = 0;
    for (int i = 0; i < length; i++) {
        sum += pow(a[i] - b[i], 2);
    }
    return sqrt(sum);
}

kernel void calculateStats(global const double* outputs, global const double* netOutputs, global double* stats, int size, int layerOffset, int layer, int total) {
    int cur = get_global_id(0);
    if (cur >= total) return;

//    // TODO: Split into smaller kernel?
    stats[cur] = 1 - diffMag(netOutputs + (layerOffset + cur * size), outputs + (cur * size), size);
    stats[total + cur] = mseCost(netOutputs + (layerOffset + cur * size), outputs + (cur * size), size);
}

kernel void calculateLayerStats(global const double* netOutputs, global double* stats, int size, int layerOffset, int layers, int total) {
    int cur = get_global_id(0);
    if (cur >= total) return;

    // TODO: Split into smaller kernel?

    double mean = 0;
    for (int i = 0; i < size; i++) {
        mean += netOutputs[layerOffset + cur * size + i] / size;
    }
    stats[cur] = mean / total;

    double dev = 0;
    for (int i = 0; i < size; i++) {
        dev += pow(mean - netOutputs[layerOffset + cur * size + i], 2) / size;
    }
    stats[total + cur] = sqrt(dev) / total;
//    stats[total + cur] = 1;
}


kernel void binaryVectorSummation(global double* data, int offset, int size, int step, int total) {
    int cur = get_global_id(0);
    if (cur >= total) return;

    for (int i = 0; i < size; i++) {
        int subStride = size * (1 << step);
        int pos = subStride * cur * 2;
        data[i + pos] = data[i + pos] + data[i + pos + subStride];
    }
}

kernel void binarySummation(global double* data, int offset, int stride, int step, int total) {
    int cur = get_global_id(0);
    if (cur >= total) return;

    int subStride = stride * (1 << step);
    int pos = subStride * cur * 2;
    data[offset + pos] = data[offset + pos] + data[offset + pos + subStride];
}



// Replicates clEnqueueFillBuffer(3)
kernel void fillBuffer(global char* buffer, long pattern, int patternSize, int offset, int size) {
    int cur = get_global_id(0);
    if (cur >= size / patternSize) return;

    for (int i = 0; i < patternSize; i++) {
        buffer[offset + cur * patternSize + i] = (&pattern)[i];
    }
}