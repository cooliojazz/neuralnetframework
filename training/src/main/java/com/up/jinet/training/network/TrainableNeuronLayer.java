package com.up.jinet.training.network;

import com.up.jinet.NeuronLayer;
import com.up.jinet.training.network.data.TrainableLayerOutput;
import com.up.jinet.function.activation.ActivationFunction;
import com.up.jinet.function.cost.CostFunction;

/**
 *
 * @author Ricky
 */
public class TrainableNeuronLayer implements NeuronLayer {

    private static final long serialVersionUID = 1;

    private TrainableNeuron[] neurons;
	private ActivationFunction act;

    public TrainableNeuronLayer(TrainableNeuron[] neurons, ActivationFunction act) {
        this.neurons = neurons;
		this.act = act;
    }

    public TrainableNeuronLayer(int size, int prev, ActivationFunction act) {
        neurons = new TrainableNeuron[size];
		this.act = act;
        for (int i = 0; i < neurons.length; i++) {
            neurons[i] = new TrainableNeuron(prev, act.randomRange(prev, size));
        }
    }

	public ActivationFunction getActivation() {
		return act;
	}
    
    public TrainableLayerOutput get(double[] input) {
        double[] ret;
        double[][] dret;
        double[] zs = new double[neurons.length];
		
        for (int i = 0; i < neurons.length; i++) {
			zs[i] = neurons[i].getInternal(input);
        }
		
		ret = act.get(zs);
		dret = act.getDerivative(zs);

        return new TrainableLayerOutput(ret, dret);
    }
    
	/**
	 * Finds the error in the neurons for this layer treating this as the last layer that uses the cost function directly.
	 * 
	 * @param act Intended activations
	 * @param cost
	 * @return 
	 */
    public double[] error(double[] last, double[][] dLast, double[] act, CostFunction cost) {
        double[] err = new double[neurons.length];
		double[] dCost = new double[neurons.length];
        for (int i = 0; i < neurons.length; i++) {
			dCost[i] = cost.getDerivative(last[i], act[i]);
		}
        for (int i = 0; i < neurons.length; i++) {
			err[i] = 0;
			for (int j = 0; j < neurons.length; j++) {
				err[i] += dLast[i][j] * dCost[j];
			}
		}
        return err;
    }
	
    /**
	 * Finds the error in the neurons for this layer assuming it is an internal layer.
	 * 
	 * @param lNl The layer above this one
	 * @param lErr The error from the layer above
	 * @return 
	 */
    public double[] error(TrainableNeuronLayer lNl, double[] lErr, double[][] dLast) {
        double[] err = new double[neurons.length];
        double[] downErr = new double[neurons.length];
		for (int i = 0; i < neurons.length; i++) {
			downErr[i] = 0;
			for (int j = 0; j < lNl.neurons.length; j++) {
				downErr[i] += lNl.neurons[j].getWeights()[i] * lErr[j];
            }
        }
        for (int i = 0; i < neurons.length; i++) {
			err[i] = 0;
			for (int j = 0; j < neurons.length; j++) {
				err[i] += dLast[i][j] * downErr[j];
			}
		}
        return err;
    }
    
    public void correct(double[][] cWeights, double[][] lastCWs, double[] cBiases, double[] lastCBs, HyperParameters params) {
        for (int i = 0; i < neurons.length; i++) {
            neurons[i].correct(cWeights[i], lastCWs[i], cBiases[i], lastCBs[i], params);
        }
    }

    public int inSize() {
        return neurons[0].size();
    }

    public int size() {
        return neurons.length;
    }
    
    public TrainableNeuron[] getNeurons() {
        return neurons;
    }

	public void setActivation(ActivationFunction act) {
		this.act = act;
	}
}
