package com.up.jinet.training;

import com.jogamp.opencl.*;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.DoubleBuffer;
import java.nio.file.Path;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class CLRunner {

    private static final Pattern includeRegex = Pattern.compile("#include '([^']+)'");
    public static final CLRunner instance = new CLRunner(Path.of("/NeuralNet.cl"));

    // Adjust until no card stalling
    public static final int maxKernelGroups = 1024;

    private final CLContext ctx = CLContext.create(CLPlatform.listCLPlatforms()[0]);
    private final CLDevice device = ctx.getMaxFlopsDevice();
    private CLProgram program;

    //getClass().getResourceAsStream("/Matrix.cl")
    public CLRunner(Path program) {

        System.out.println(ctx.getPlatform().version.fullversion);
        try {
            this.program = ctx.createProgram(loadProgramFile(program)).build();
            System.out.println(this.program.getBuildLog());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int maxWorkGroupSize() {
        return device.getMaxWorkGroupSize();
    }

    public CLKernel createKernel(String function) {
        return program.createCLKernel(function);
    }

    public CLCommandQueue createQueue(CLCommandQueue.Mode... modes) {
        return device.createCommandQueue(modes);
    }

    public CLBuffer<DoubleBuffer> createBuffer(int size, CLMemory.Mem... flags) {
        return ctx.createDoubleBuffer(size, flags);
    }

    // TODO: Although returning an automatic event list for each of these kernels is convenient, it should probably have some way to be optional.
    // lol yeah cause otherwise it causes memory leaks
    public CLEventList[] enqueueMultiKernel(CLKernel kernel, CLCommandQueue queue, int totalWorkItems) {
        int workGroupSize = Math.min(totalWorkItems, device.getMaxWorkGroupSize());
        if (totalWorkItems % workGroupSize != 0) {
            totalWorkItems = workGroupSize * (totalWorkItems / workGroupSize + 1);
        }

        int groupCount = (int)Math.ceil((double)totalWorkItems / workGroupSize);
        int kernelCount = (int)Math.ceil((double)groupCount / CLRunner.maxKernelGroups);

        CLEventList events = new CLEventList(kernelCount);
        for (int k = 0; k < kernelCount; k++) {
            queue.put1DRangeKernel(kernel, k * CLRunner.maxKernelGroups, groupCount * workGroupSize, workGroupSize, events);
        }
        queue.putBarrier();
        return new CLEventList[] {events};
    }

//    public CLEventList enqueueMulti2dKernel(CLKernel kernel, CLCommandQueue queue, int totalWorkItems) {
//        int workGroupSize = Math.min(totalWorkItems, device.getMaxWorkGroupSize());
//        if (totalWorkItems % workGroupSize != 0) {
//            totalWorkItems = workGroupSize * (totalWorkItems / workGroupSize + 1);
//        }
//
//        int groupCount = (int)Math.ceil((double)totalWorkItems / workGroupSize);
//        int kernelCount = (int)Math.ceil((double)groupCount / CLRunner.maxKernelGroups);
//
//        CLEventList events = new CLEventList(kernelCount);
//        for (int k = 0; k < kernelCount; k++) {
//            queue.put1DRangeKernel(kernel, k * CLRunner.maxKernelGroups, groupCount * workGroupSize, workGroupSize, events);
//        }
//        queue.putBarrier();
//        return events;
//    }

    public void runKernel(String function, int totalWorkItems, CLBuffer<?>[] inputs, CLBuffer<?>[] outputs, Number... args) {
        int workGroupSize = Math.min(totalWorkItems, device.getMaxWorkGroupSize());

        CLKernel kernel = createKernel(function);
        kernel.putArgs(inputs);
        kernel.putArgs(outputs);
        for (Number a : args) {
            switch (a) {
                case Byte b -> kernel.putArg(b);
                case Short s -> kernel.putArg(s);
                case Integer i -> kernel.putArg(i);
                case Long l -> kernel.putArg(l);
                case Float f -> kernel.putArg(f);
                case Double d -> kernel.putArg(d);
                default -> throw new NumberFormatException("Only accepts basic number types.");
            }

        }
        kernel.putArg(totalWorkItems);

        if (totalWorkItems % workGroupSize != 0) {
            totalWorkItems = workGroupSize * (totalWorkItems / workGroupSize + 1);
        }

        int groupCount = (int)Math.ceil((double)totalWorkItems / workGroupSize);
        int kernelCount = (int)Math.ceil((double)groupCount / CLRunner.maxKernelGroups);

        CLCommandQueue queue = device.createCommandQueue();
        for (CLBuffer<?> in : inputs) queue.putWriteBuffer(in, false);
        for (int k = 0; k < kernelCount; k++) {
            queue.put1DRangeKernel(kernel, k * CLRunner.maxKernelGroups, groupCount * workGroupSize, workGroupSize);
        }
        for (CLBuffer<?> out : outputs) queue.putReadBuffer(out, false);
        queue.finish();

        queue.release();
        kernel.release();
    }

    public static long getProfileTime(CLEventList events) {
        return StreamSupport.stream(events.spliterator(), true).mapToLong(e -> {
            try {
                return e.getProfilingInfo(CLEvent.ProfilingCommand.END) - e.getProfilingInfo(CLEvent.ProfilingCommand.START);
            } catch (CLException.CLProfilingInfoNotAvailableException ex) {
                System.out.println("Error getting profile time: " + ex.getMessage());
                return 0;
            }
        }).sum();
    }

    /**
     * Note that includes from this will mess up line numbers for compilation errors; unsure if there's a way around that.
     * @param file
     * @return
     */
    private String loadProgramFile(Path file) {
        String path = "/" + StreamSupport.stream(file.spliterator(), false).map(Path::toString).collect(Collectors.joining("/"));
        InputStream program = getClass().getResourceAsStream(path);
        return new BufferedReader(new InputStreamReader(program)).lines().map(l -> {
                Matcher m = includeRegex.matcher(l);
                if (m.matches()) {
                    return loadProgramFile(file.resolveSibling(m.group(1)));
                }
                return l;
            }).collect(Collectors.joining("\n"));
    }

    /**
     * Sums all the values in data back into the first slot via binary reduction.
     * @param queue
     * @param data
     * @param count
     * @return
     */
    public KernelInfos setupBinarySummationKernel(CLCommandQueue queue, CLBuffer<DoubleBuffer> data, int offset, int count) {
        return setupBinarySummationKernel(queue, data, offset, 1, count);
    }
    
    /**
     * Sums all the values in data back into the first slot via binary reduction.
     * @param queue
     * @param data
     * @param stride
     * @param count
     * @return
     */
    public KernelInfos setupBinarySummationKernel(CLCommandQueue queue, CLBuffer<DoubleBuffer> data, int offset, int stride, int count) {
        CLKernel kernel = CLRunner.instance.createKernel("binarySummation");
        
        ArrayList<CLEventList> events = new ArrayList<>();
        int steps = (int)Math.ceil(Math.log(count) / Math.log(2));
        int remaining = count;
        for (int i = 0; i < steps; i++) {
            int items = remaining / 2;
            remaining -= items;
            if (items > 0) {
                kernel.setArgs(data, offset, stride, i, items);
                events.addAll(Arrays.asList(enqueueMultiKernel(kernel, queue, items)));
            }
        }
        
        return new KernelInfos(new CLKernel[] {kernel}, Collections.singletonMap("binarySummation", events.toArray(CLEventList[]::new)));
    }
    
    public KernelInfos setupBinaryVectorSummationKernel(CLCommandQueue queue, CLBuffer<DoubleBuffer> data, int offset, int size, int count) {
        CLKernel kernel = CLRunner.instance.createKernel("binaryVectorSummation");
        
        ArrayList<CLEventList> events = new ArrayList<>();
        int steps = (int)Math.ceil(Math.log(count) / Math.log(2));
        int remaining = count;
        for (int i = 0; i < steps; i++) {
            int items = remaining / 2;
            remaining -= items;
            if (items > 0) {
                kernel.setArgs(data, offset, size, i, items);
                events.addAll(Arrays.asList(enqueueMultiKernel(kernel, queue, items)));
            }
        }
        
        return new KernelInfos(new CLKernel[] {kernel}, Collections.singletonMap("binaryVectorSummation", events.toArray(CLEventList[]::new)));
    }

    public KernelInfos putFillBuffer(CLCommandQueue queue, CLBuffer<DoubleBuffer> buffer, double pattern, int offset, int size) {
        // Follows the convention of clEnqueueFillBuffer in hopes of being able to use it if ever it is added to the JOCL API.
        CLKernel kernel = CLRunner.instance.createKernel("fillBuffer");
        kernel.setArgs(buffer, pattern, 8, offset * 8, size * 8);

        HashMap<String, CLEventList[]> events = new HashMap<>();
        events.put("fillBuffer", enqueueMultiKernel(kernel, queue, size));
        
        return new KernelInfos(new CLKernel[] {kernel}, events);
    }

    public record KernelInfos(CLKernel[] kernels, Map<String, CLEventList[]> events) {

        public KernelInfos merge(KernelInfos... infos) {
            ArrayList<CLKernel> kernels = new ArrayList<>();
            HashMap<String, ArrayList<CLEventList>> events = new HashMap<>();
            kernels.addAll(Arrays.asList(this.kernels));
            for (Map.Entry<String, CLEventList[]> e : this.events.entrySet()) {
                if (!events.containsKey(e.getKey())) events.put(e.getKey(), new ArrayList<>());
                events.get(e.getKey()).addAll(Arrays.asList(e.getValue()));
            }
            for (KernelInfos i : infos) {
                kernels.addAll(Arrays.asList(i.kernels));
                for (Map.Entry<String, CLEventList[]> e : i.events.entrySet()) {
                    if (!events.containsKey(e.getKey())) events.put(e.getKey(), new ArrayList<>());
                    events.get(e.getKey()).addAll(Arrays.asList(e.getValue()));
                }
            }
            return new KernelInfos(kernels.toArray(CLKernel[]::new), events.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().toArray(CLEventList[]::new))));
        }

        public void release() {
            Stream.of(kernels).forEach(CLKernel::release);
            events.values().stream().flatMap(Stream::of).forEach(CLEventList::release);
        }

        public static KernelInfos empty() {
            return new KernelInfos(new CLKernel[0], new HashMap<>());
        }
    }
}
