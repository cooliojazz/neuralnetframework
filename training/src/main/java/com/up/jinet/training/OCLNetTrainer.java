package com.up.jinet.training;

import com.jogamp.opencl.CLBuffer;
import com.up.jinet.NetLoader;
import com.up.jinet.training.network.HyperParameters;
import com.up.jinet.training.network.data.NetCVals;
import com.up.jinet.training.network.TrainableOCLNeuralNet;

import java.io.*;
import java.nio.DoubleBuffer;
import java.util.function.BiConsumer;

/**
 * Only properly trains for single-class classification currently
 *
 * @author Ricky
 */
public class OCLNetTrainer extends NetTrainer<TrainableOCLNeuralNet, CLBuffer<DoubleBuffer>> {

	//TODO: Finalize OCL API now that this mostly works and move to main training repo.
	
	PIDController trainPid = new PIDController(0.15, 1, 0.000000005, 0.9);
	CLBuffer<DoubleBuffer> netOutputs;
	
	public OCLNetTrainer(TrainableOCLNeuralNet net, int samples, int epochs, int checkpoints, HyperParameters params, boolean randomBatches, File saveFolder, BiConsumer<OCLNetTrainer, NetStatistics> batchCallback) {
		super(net, samples, epochs, checkpoints, params, randomBatches, saveFolder, (t, s) -> batchCallback.accept((OCLNetTrainer)t, s));
	}
	
	protected void setup(TrainableOCLNeuralNet net) {
		netOutputs = net.createOutputBuffer(Math.max(params.batchSize(), params.testSize()));
	}
	
	protected CLBuffer<DoubleBuffer> evaluate(TrainableOCLNeuralNet net, BatchedDataSet data) {
		net.getManyFast(data.inputs(), data.size(), netOutputs);
		return netOutputs;
	}
	
	protected NetCVals findError(TrainableOCLNeuralNet net, BatchedDataSet data, CLBuffer<DoubleBuffer> output) {
		return net.backPropogate(data.inputs(), output, data.outputs(), data.size());
	}
	
	protected void correct(TrainableOCLNeuralNet net, NetCVals netCvals, NetCVals lastCvals) {
		net.correct(netCvals, lastCvals, params);
	}
	
	protected NetStatistics statistics(TrainableOCLNeuralNet net, BatchedDataSet data, CLBuffer<DoubleBuffer> output) {
		double[][] stats = net.getStats(data.outputs(), output, data.size());
		double[] means = new double[net.height()];
		double[] devs = new double[net.height()];
		for (int i = 0; i < net.height(); i++) {
			means[i] = stats[i][0];
			devs[i] = stats[i][1];
		}
		return new NetStatistics(means, devs, stats[net.height()][1], stats[net.height()][0]);
	}
	
	protected void updateParameters(TrainableOCLNeuralNet net, NetStatistics stats) {

////			params.trainRatio *= params.ratioDecay;
////			params = new HyperParameters(params.batchSize(), params.trainRatio() * params.ratioDecay(), params.ratioDecay(), params.momentum(), params.regularization());
//			double meanMagLayerMean = DoubleStream.of(stats.layerMean()).map(Math::abs).average().orElse(0);
//
//			// PID stuff, should be put in its own function probably
////			double meanTarget = 0.3;
////			List<Double> lastErrs = statMeanMean.getData().stream().skip(Math.max(0, statMeanMean.getSize() - 10)).map(p -> p.getY() / params.batchSize() - meanTarget).toList();
////			List<Double> lastErrs = statMeanMean.getData().stream().map(p -> p.getY() / params.batchSize() - meanTarget).toList();
////			double slopeMagLayerMean = IntStream.range(Math.max(statMeanMean.getSize() - 10, 0), statMeanMean.getSize()).mapToDouble(i -> (i + 1 < lastErrs.size() ? lastErrs.get(i + 1) : 0) - (i < lastErrs.size() ? lastErrs.get(i) : 0)).average().orElse(0);
////			double sumMagLayerMean = lastErrs.stream().mapToDouble(d -> d).sum();
////			double pid = -(2 * (meanTarget - meanMagLayerMean) + 0.2 * sumMagLayerMean + 0.75 * slopeMagLayerMean) * params.ratioDecay();
//
////			trainPid.target = 0.05 + 0.25 * Math.log(batch + 1) / Math.log(iterLen);
//			double smooth = 0.003;
//			trainPid.target = 0.01 + 0.34 * Math.log(batch * smooth + 1) / Math.log(iterLen * smooth + 1);
//			params = new HyperParameters(params.batchSize(), params.testSize(), params.trainRatio() * (1 + trainPid.next(meanMagLayerMean) * params.ratioDecay()), params.ratioDecay(), params.momentum(), params.regularization());

	}
	
	protected void cleanup() {
		netOutputs.release();
	}
	
	
	/**
	 * Loads the net trainer config and last state from a file
	 *
	 */
	public static OCLNetTrainer fromRun(File saveFolder, BiConsumer<OCLNetTrainer, NetStatistics> batchCallback) throws IOException, ClassNotFoundException {
		ObjectInputStream is = new ObjectInputStream(new FileInputStream(new File(saveFolder, "run-info.ntd")));
		OCLNetTrainer trainer = new OCLNetTrainer(NetLoader.load(new File(saveFolder, "current-net.nwa")), is.readInt(), is.readInt(), is.readInt(), (HyperParameters)is.readObject(), is.readBoolean(), saveFolder, batchCallback);
		trainer.batch = is.readInt();
		is.close();
		return trainer;
	}
	
	private class PIDController {
		
		double target;
		double kP;
		double kI;
		double kD;
		
		double lastValue = 0;
		double rollingSlope = 0;
		double totalError = 0;
		
		public PIDController(double target, double kP, double kI, double kD) {
			this.target = target;
			this.kP = kP;
			this.kI = kI;
			this.kD = kD;
		}
		
		public double next(double value) {
			double error = target - value;
			double lError = target - lastValue;
			rollingSlope = rollingSlope * 0.9 + (error - lError) * 0.1;
			totalError += error;
			lastValue = value;
			return kP * error + kI * totalError + kD * rollingSlope;
		}
	}

}
