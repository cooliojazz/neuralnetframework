package com.up.jinet.training.gui;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import javax.swing.JFrame;

/**
 *
 * @author Ricky
 */
public class Graph {
	
	
	private JFrame f = new JFrame("Graph");
	private Canvas c = new Canvas() {
			@Override
			public void paint(Graphics g) {
				draw((Graphics2D)g);
			}

			@Override
			public void update(Graphics g) {
				paint(g);
			}
			
		};
	private ArrayList<DataSet> dataSets = new ArrayList<>();

	public Graph() {
		f.add(c);
		f.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		f.setSize(900, 1000);
		f.setVisible(true);
	}
	
	private void draw(Graphics2D g) {
		BufferedImage buffer = new BufferedImage(c.getWidth(), c.getHeight(), BufferedImage.TYPE_INT_ARGB);
		Graphics2D bg = (Graphics2D)buffer.getGraphics();
		
		Rectangle dataBounds = new Rectangle(1, 1);
		for (DataSet set : dataSets) {
			dataBounds = dataBounds.union(set.getBounds());
		}
		Rectangle graphBounds = new Rectangle(100, 100, c.getWidth() - 200, c.getHeight() - 200);
		
		// Background
		bg.setColor(Color.white);
		bg.fillRect((int)graphBounds.getX(), (int)graphBounds.getY(), (int)graphBounds.getWidth(), (int)graphBounds.getHeight());
		
		// Grid
		double xScale = graphBounds.getWidth() / dataBounds.getWidth();
		double gridXWidth = xScale * Math.pow(10, Math.floor(Math.log10(dataBounds.getWidth() / 25)));
		
		bg.setColor(new Color(224, 224, 224));
		for (int x = 0; x < graphBounds.getWidth() / gridXWidth; x++) {
			bg.drawLine((int)(graphBounds.getX() + x * gridXWidth), (int)graphBounds.getY(), (int)(graphBounds.getX() + x * gridXWidth), (int)graphBounds.getY() + (int)graphBounds.getHeight());
		}
		for (int y = 0; y < 100; y++) {
			bg.drawLine((int)graphBounds.getX(), (int)(graphBounds.getY() + y * graphBounds.getHeight() / 100), (int)graphBounds.getX() + (int)graphBounds.getWidth(), (int)(graphBounds.getY() + y * graphBounds.getHeight() / 100));
		}
		bg.setColor(new Color(160, 160, 160));
		for (int x = 0; x < graphBounds.getWidth() / gridXWidth / 10; x++) {
			bg.drawLine((int)(graphBounds.getX() + x * gridXWidth * 10), (int)graphBounds.getY(), (int)(graphBounds.getX() + x * gridXWidth * 10), (int)graphBounds.getY() + (int)graphBounds.getHeight());
		}
		for (int y = 0; y < 10; y++) {
			bg.drawLine((int)graphBounds.getX(), (int)(graphBounds.getY() + y * graphBounds.getHeight() / 10), (int)graphBounds.getX() + (int)graphBounds.getWidth(), (int)(graphBounds.getY() + y * graphBounds.getHeight() / 10));
		}
		bg.setColor(Color.black);
		bg.drawLine((int)graphBounds.getX(), (int)graphBounds.getY(), (int)graphBounds.getX() + (int)graphBounds.getWidth(), (int)graphBounds.getY());
		bg.drawLine((int)graphBounds.getX(), (int)graphBounds.getY(), (int)graphBounds.getX(), (int)graphBounds.getY() + (int)graphBounds.getHeight());
		
		// Labels
		for (int x = 0; x <= 10; x++) {
//			g.drawString(Math.round(x * dataBounds.getWidth() / 10 * 100) / 100d + "", (int)(graphBounds.getX() + x * graphBounds.getWidth() / 10) - 10, (int)graphBounds.getY() - 10);
			bg.drawString(Math.round(x * dataBounds.getWidth() / 10) + "", (int)(graphBounds.getX() + x * graphBounds.getWidth() / 10) - 10, (int)graphBounds.getY() - 10);
		}
		for (int y = 0; y <= 10; y++) {
//			g.drawString(Math.round(y * dataBounds.getHeight() / 10 * 100) / 100d + "", (int)graphBounds.getX() - 50, (int)(graphBounds.getY() + y * graphBounds.getHeight() / 10) + 5);
			bg.drawString(Math.round(y * dataBounds.getHeight() / 10) + "", (int)graphBounds.getX() - 50, (int)(graphBounds.getY() + y * graphBounds.getHeight() / 10) + 5);
		}
		
		// Data
		Color col = Color.black;
		for (DataSet set : dataSets) {
			bg.setColor(col);
			for (Point2D point : set.getData()) {
				if (dataBounds.contains(point)) {
					bg.drawRect((int)((point.getX() - dataBounds.getX()) / dataBounds.getWidth() * graphBounds.getWidth() + graphBounds.getX()) - 1, (int)((point.getY() - dataBounds.getY()) / dataBounds.getHeight() * graphBounds.getHeight() + graphBounds.getY()) - 1, 3, 3);
				}
			}
			col = col.brighter();
		}
		
		g.clearRect(0, 0, c.getWidth(), c.getHeight());
		g.drawImage(buffer, 0, 0, null);
	}

	public void clearDataSets() {
		dataSets.clear();
		c.repaint();
	}

	public void addDataSet(DataSet dataSet) {
		dataSets.add(dataSet);
		c.repaint();
	}
	
	public static class DataSet {
		
		private final Rectangle bounds;
		private final Point2D[] data;

		public DataSet(Rectangle bounds, Point2D[] data) {
			this.bounds = bounds;
			this.data = data;
		}

		public Rectangle getBounds() {
			return bounds;
		}

		public Point2D[] getData() {
			return data;
		}
		
	}
}
