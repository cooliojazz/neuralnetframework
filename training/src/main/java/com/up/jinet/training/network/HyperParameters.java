package com.up.jinet.training.network;

import java.io.Serializable;

/**
 * Network learning parameters
 *
 * @param batchSize Number of samples used per correction.
 * @param trainRatio How quickly the network will learn, between zero and one.
 * @param ratioDecay How much the training ratio should be reduced (as a multiplier) after each batch.
 * @param momentum
 * @param regularization
 */
public record HyperParameters(int batchSize, int testSize, double trainRatio, double ratioDecay, double momentum, double regularization) implements Serializable {

    public HyperParameters(int batchSize, int testSize, double trainRatio) {
        this(batchSize, testSize, trainRatio, 0.99999, 0.5, 0.05);
    }

    public HyperParameters(int batchSize, int testSize, double trainRatio, double ratioDecay) {
        this(batchSize, testSize, trainRatio, ratioDecay, 0.5, 0.05);
    }

//    public HyperParameters(int batchSize, double trainRatio, double momentum) {
//        this(batchSize, trainRatio, 0.99999, momentum, 0.05);
//    }
}
