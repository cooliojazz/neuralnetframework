package com.up.jinet.training.network;

import com.jogamp.opencl.*;
import com.up.jinet.FixedNeuron;
import com.up.jinet.Neuron;
import com.up.jinet.NeuronLayer;
import com.up.jinet.function.activation.ActivationFunction;
import com.up.jinet.function.activation.SigmoidActivation;
import com.up.jinet.function.activation.SoftmaxActivation;
import com.up.jinet.function.activation.SwishActivation;
import com.up.jinet.function.cost.CostFunction;
import com.up.jinet.function.cost.CrossEntropyCost;
import com.up.jinet.function.cost.MaeCost;
import com.up.jinet.function.cost.MseCost;
import com.up.jinet.training.network.data.TrainableLayerOutput;
import com.up.jinet.training.CLRunner;

import java.nio.DoubleBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

/**
 *
 * @author Ricky
 */
public class TrainableOCLNeuronLayer implements NeuronLayer {

    private static final long serialVersionUID = -1; // Don't serialize directly

    private final int size;
    private final int inSize;
//    private final double[] weights;
//    private final double[] biases;
    private final CLBuffer<DoubleBuffer> weights;
    private final CLBuffer<DoubleBuffer> biases;
	private ActivationFunction act;

    public TrainableOCLNeuronLayer(int size, double[] weights, double[] biases, ActivationFunction act) {
        this(size, weights, biases);
		this.act = act;
    }

    public TrainableOCLNeuronLayer(int size, int prev, ActivationFunction act) {
        this(size, prev);
		this.act = act;
        DoubleBuffer wBuf = this.weights.getBuffer();
        for (int i = 0; i < size * inSize; i++) {
            wBuf.put(i, act.randomWeight(prev, size));
        }
    }

    private TrainableOCLNeuronLayer(int size, double[] weights, double[] biases) {
        this(size, weights.length / size);

        DoubleBuffer wBuf = this.weights.getBuffer();
        wBuf.put(weights);
        wBuf.flip();
        DoubleBuffer bBuf = this.biases.getBuffer();
        bBuf.put(biases);
        bBuf.flip();
    }

    private TrainableOCLNeuronLayer(int size, int inSize) {
        this.size = size;
        this.inSize = inSize;
        this.weights = CLRunner.instance.createBuffer(size * inSize, CLMemory.Mem.READ_ONLY);
        this.biases = CLRunner.instance.createBuffer(size, CLMemory.Mem.READ_ONLY);
    }

	public ActivationFunction getActivation() {
		return act;
	}

    public TrainableLayerOutput get(double[] input) {
        double[] ret;
        double[][] dret;
        double[] zs = new double[size];

        kernelMultiplyVector(input, zs);

        ret = act.get(zs);
        dret = act.getDerivative(zs);

        return new TrainableLayerOutput(ret, dret);
    }

    public TrainableLayerOutput[] getMany(double[][] inputs, int count) {
        double[][] zs = new double[count][size];

        kernelMultiplyVectorMany(inputs, zs, count);

        TrainableLayerOutput[] outputs = new TrainableLayerOutput[count];
        for (int i = 0; i < count; i++) {
            outputs[i] = new TrainableLayerOutput(act.get(zs[i]), act.getDerivative(zs[i]));
        }
        return outputs;
    }

    synchronized public CLRunner.KernelInfos setupForwardKernels(CLCommandQueue queue, CLBuffer<DoubleBuffer> inputs, CLBuffer<DoubleBuffer> outputs, CLBuffer<DoubleBuffer> netOutput, int count, int layerOffset) {
        CLKernel bmvmKernel = CLRunner.instance.createKernel("biasedMultiplyVectorMany");
        bmvmKernel.setArgs(weights, biases, inputs, outputs, inSize, size, size * count);
        Map.Entry<String, Object[]>[] acts = getActivationSetup(act);
        if (acts == null) throw new RuntimeException("Unimplemented activation function " + act.getClass().getName());
        CLKernel amKernel = CLRunner.instance.createKernel(acts[0].getKey());
        Object[] amArgs = new Object[] {outputs, netOutput, size, layerOffset, count};
        Object[] fullAmArgs = new Object[amArgs.length + acts[0].getValue().length];
        System.arraycopy(amArgs, 0, fullAmArgs, 0, amArgs.length);
        if (acts[0].getValue().length > 0) System.arraycopy(acts[0].getValue(), 0, fullAmArgs, amArgs.length, acts[0].getValue().length);
        amKernel.setArgs(fullAmArgs);
        CLKernel admKernel = CLRunner.instance.createKernel(acts[1].getKey());
        Object[] admArgs = new Object[] {outputs, netOutput, size, layerOffset + size * count, count};
        Object[] fullAdmArgs = new Object[admArgs.length + acts[1].getValue().length];
        System.arraycopy(admArgs, 0, fullAdmArgs, 0, admArgs.length);
        if (acts[1].getValue().length > 0) System.arraycopy(acts[1].getValue(), 0, fullAdmArgs, admArgs.length, acts[1].getValue().length);
        admKernel.setArgs(fullAdmArgs);

        HashMap<String, CLEventList[]> events = new HashMap<>();
        // TODO: Maybe don't write these each time somehow?
        queue.putWriteBuffer(weights, false);
        queue.putWriteBuffer(biases, false);
        events.put("biasedMultiplyVector", CLRunner.instance.enqueueMultiKernel(bmvmKernel, queue, size * count));
        events.put(acts[0].getKey(), CLRunner.instance.enqueueMultiKernel(amKernel, queue, count));
        events.put(acts[1].getKey(), CLRunner.instance.enqueueMultiKernel(admKernel, queue, count));
        events.put("copy", new CLEventList[] {new CLEventList(1)});
        queue.putCopyBuffer(netOutput, inputs, layerOffset * 8, 0, (long)count * size * 8, events.get("copy")[0]);
        queue.putBarrier();

//        return new CLRunner.KernelInfos(new CLKernel[] {bmvmKernel, samKernel, sadmKernel}, events);
        return new CLRunner.KernelInfos(new CLKernel[] {bmvmKernel, amKernel, admKernel}, events);
    }
    
    private Map.Entry<String, Object[]>[] getActivationSetup(ActivationFunction act) {
        return switch (act) {
                case SigmoidActivation sa -> new Map.Entry[] {Map.entry("sigmoidActivationMany", new Object[0]), Map.entry("sigmoidActivationDerivativeMany", new Object[0])};
                case SoftmaxActivation sa -> new Map.Entry[] {Map.entry("softmaxActivationMany", new Object[0]), Map.entry("softmaxActivationDerivativeMany", new Object[0])};
                case SwishActivation sa -> new Map.Entry[] {Map.entry("swishActivationMany", new Object[] {sa.getBeta()}), Map.entry("swishActivationDerivativeMany", new Object[] {sa.getBeta()})};
                default -> null;
            };
    }

    /**
     *
     * @param queue
     * @param outputs Intended output from the network for the last run
     * @param lastError The error returned from the cost function
     * @param netOutputs The set of network activations & derivatives from the last run
     * @param count
     * @param layerOffset
     */
    public CLRunner.KernelInfos setupReverseKernelTop(CLCommandQueue queue, CLBuffer<DoubleBuffer> outputs, CLBuffer<DoubleBuffer> lastError, CLBuffer<DoubleBuffer> netOutputs, int count, int layerOffset, CostFunction func) {
        CLKernel ceKernel = CLRunner.instance.createKernel("costError");
        ceKernel.setArgs(netOutputs, outputs, lastError, size, layerOffset, func instanceof MaeCost ? 0 : (func instanceof MseCost ? 1 : (func instanceof CrossEntropyCost ? 2 : -1)), count);

        HashMap<String, CLEventList[]> events = new HashMap<>();
        queue.putWriteBuffer(outputs, false);
        events.put("costError", CLRunner.instance.enqueueMultiKernel(ceKernel, queue, count));
        // TODO: Probably should convert this to an out of order queue and then these barriers will be needed to synchronize between multi-kernel calls
        queue.putBarrier();

        return new CLRunner.KernelInfos(new CLKernel[] {ceKernel}, events);
    }

    /**
     *
     * @param queue
     * @param inputs Inputs to the network for the last run
     * @param lastError Input as the error at the outputs of this layer, output as the error at its inputs
     * @param tempError Working memory for inter-kernel error calculation
     * @param netOutputs  The set of network activations & derivatives from the last run
     * @param netCvals
     * @param count
     * @param layerOffset
     * @param prevLayerOffset
     * @param cvalLayerOffset
     * @param layer
     * @return
     */
    synchronized public CLRunner.KernelInfos setupReverseKernels(CLCommandQueue queue, CLBuffer<DoubleBuffer> inputs, CLBuffer<DoubleBuffer> lastError, CLBuffer<DoubleBuffer> tempError, CLBuffer<DoubleBuffer> netOutputs, CLBuffer<DoubleBuffer> layerNetCvals, CLBuffer<DoubleBuffer> netCvals, int count, int layerOffset, int prevLayerOffset, int cvalLayerOffset, int layer) {
        int ncvsLength = size * (inSize + 1);
        
        CLKernel neKernel = CLRunner.instance.createKernel("neuronError");
        neKernel.setArgs(netOutputs, lastError, tempError, size, layerOffset, count);
        
        CLKernel ieKernel = null;
        if (layer > 0) {
            ieKernel = CLRunner.instance.createKernel("inputError");
            ieKernel.setArgs(weights, tempError, lastError, size, inSize, count);
        }

        CLKernel uwcKernel = CLRunner.instance.createKernel("updateWeightChanges");
        if (layer == 0) {
            uwcKernel.setArgs(tempError, inputs, layerNetCvals, size, inSize, ncvsLength, 0, count);
        } else {
            uwcKernel.setArgs(tempError, netOutputs, layerNetCvals, size, inSize, ncvsLength, prevLayerOffset, count);
        }

        CLKernel ubcKernel = CLRunner.instance.createKernel("updateBiasChanges");
        ubcKernel.setArgs(tempError, layerNetCvals, size, ncvsLength, count);

        HashMap<String, CLEventList[]> events = new HashMap<>();
        queue.putWriteBuffer(weights, false);
        events.put("neuronError", CLRunner.instance.enqueueMultiKernel(neKernel, queue, count));
        if (ieKernel != null) events.put("inputError", CLRunner.instance.enqueueMultiKernel(ieKernel, queue, count));
//        CLRunner.KernelInfos clearKIs = CLRunner.instance.putFillBuffer(queue, layerNetCvals, 0, 0, count * ncvsLength);
        events.put("updateWeights", CLRunner.instance.enqueueMultiKernel(uwcKernel, queue, count));
        events.put("updateBiases", CLRunner.instance.enqueueMultiKernel(ubcKernel, queue, count));
        CLRunner.KernelInfos reductionKIs = CLRunner.instance.setupBinaryVectorSummationKernel(queue, layerNetCvals, 0, ncvsLength, count);
        events.put("copy", new CLEventList[] {new CLEventList(1)});
        queue.putCopyBuffer(layerNetCvals, netCvals, 0, cvalLayerOffset * 8, ncvsLength * 8l, events.get("copy")[0]);
        queue.putBarrier();

        return new CLRunner.KernelInfos(ieKernel != null ? new CLKernel[] {neKernel, ieKernel, uwcKernel, ubcKernel} : new CLKernel[] {neKernel, uwcKernel, ubcKernel}, events)/*.merge(clearKIs)*/.merge(reductionKIs);
    }

    public CLRunner.KernelInfos setupLayerStatsKernel(CLCommandQueue queue, CLBuffer<DoubleBuffer> netOutputs, CLBuffer<DoubleBuffer> result, int layerOffset, int layer, int count) {
        CLKernel kernel = CLRunner.instance.createKernel("calculateLayerStats");
        kernel.setArgs(netOutputs, result, size, layerOffset, layer, count);

        HashMap<String, CLEventList[]> events = new HashMap<>();
        events.put("layerStats", CLRunner.instance.enqueueMultiKernel(kernel, queue, count));

        return new CLRunner.KernelInfos(new CLKernel[] {kernel}, events);
    }

    synchronized private void kernelMultiplyVector(double[] input, double[] output) {
        CLBuffer<DoubleBuffer> vector = CLRunner.instance.createBuffer(inSize, CLMemory.Mem.READ_ONLY);
        CLBuffer<DoubleBuffer> result = CLRunner.instance.createBuffer(size, CLMemory.Mem.WRITE_ONLY);

        DoubleBuffer vBuf = vector.getBuffer();
        vBuf.clear();
        vBuf.put(input);
        vBuf.flip();

        CLRunner.instance.runKernel("biasedMultiplyVector", size, new CLBuffer[] {weights, biases, vector}, new CLBuffer[] {result}, inSize);

        DoubleBuffer rBuf = result.getBuffer();
        rBuf.rewind();
        rBuf.get(output);

        vector.release();
        result.release();
    }

    synchronized private void kernelMultiplyVectorMany(double[][] inputs, double[][] outputs, int count) {
        CLBuffer<DoubleBuffer> vector = CLRunner.instance.createBuffer(inSize * count, CLMemory.Mem.READ_ONLY);
        CLBuffer<DoubleBuffer> result = CLRunner.instance.createBuffer(size * count, CLMemory.Mem.WRITE_ONLY);

        DoubleBuffer vBuf = vector.getBuffer();
        vBuf.clear();
        for (double[] input : inputs) vBuf.put(input);
        vBuf.flip();

        CLRunner.instance.runKernel("biasedMultiplyVectorMany", size * count, new CLBuffer[] {weights, biases, vector}, new CLBuffer[] {result}, inSize, size);

        DoubleBuffer rBuf = result.getBuffer();
//        rBuf.rewind();
        for (int i = 0; i < count; i++) rBuf.get(outputs[i]);

        vector.release();
        result.release();
    }

    public double[] costError(double[] last, double[] act, CostFunction cost) {
        double[] dCost = new double[size];
        for (int i = 0; i < size; i++) {
            dCost[i] = cost.getDerivative(last[i], act[i]);
        }
        return dCost;
    }
    
    public void correct(double[][] cWeights, double[][] lastCWs, double[] cBiases, double[] lastCBs, HyperParameters params) {
        DoubleBuffer wBuf = weights.getBuffer();
        DoubleBuffer bBuf = biases.getBuffer();
        for (int n = 0; n < size; n++) {
////            for (int j = 0; j < inSize; j++) {
////                lastCWs[i][j] = lastCWs[i][j] * params.momentum() + params.trainRatio() * cWeights[i][j] / params.batchSize();
////                wBuf.put(i * inSize + j, (1 - params.trainRatio() * params.regularization() / params.batchSize()) * wBuf.get(i * inSize + j) - lastCWs[i][j]);
////            }
////            lastCBs[n] = lastCBs[n] * params.momentum() + params.trainRatio() * cBiases[n] / params.batchSize();
////            bBuf.put(i, bBuf.get(i) - lastCBs[n]);
//            correctNeuron(wBuf, bBuf, i, cWeights[i], lastCWs[i], cBiases[i], lastCBs[i], params);

            for (int j = 0; j < inSize; j++) {
                lastCWs[n][j] = lastCWs[n][j] * params.momentum() + params.trainRatio() * cWeights[n][j] / params.batchSize();
                wBuf.put(n * inSize + j, (1 - params.trainRatio() * params.regularization() / params.batchSize()) * wBuf.get(n * inSize + j)  - lastCWs[n][j]);
            }
            lastCBs[n] = lastCBs[n] * params.momentum() + params.trainRatio() * cBiases[n] / params.batchSize();
            bBuf.put(n, bBuf.get(n) - lastCBs[n]);
        }
    }
//    private void correctNeuron(DoubleBuffer wBuf, DoubleBuffer bBuf, int n, double[] cWeight, double[] lastCW, double cBias, double lastCB, HyperParameters params) {
//        for (int i = 0; i < inSize; i++) {
//            lastCW[i] = lastCW[i] * params.momentum() + params.trainRatio() * cWeight[i] / params.batchSize();
//            wBuf.put(n * inSize + i, (1 - params.trainRatio() * params.regularization() / params.batchSize()) * wBuf.get(n * inSize + i)  - lastCW[i]);
//        }
//        lastCB = lastCB * params.momentum() + params.trainRatio() * cBias / params.batchSize();
//        bBuf.put(n, bBuf.get(n) - lastCB);
//    }

    // TODO: Maybe this should be part of the main layer interface? Equivalents of it are used all over.
    public int inSize() {
        return inSize;
    }

    public int size() {
        return size;
    }

    /**
     * Returns faked neurons to allow *some* interoperability with APIs needing this
     * @return
     */
    public Neuron[] getNeurons() {
        return IntStream.range(0, size)
                        .mapToObj(i -> {
                            double[] d = new double[inSize];
                            weights.getBuffer().get(i * inSize, d);
                            return new double[][] {d, new double[] {biases.getBuffer().get(i)}};
                        })
                        .map((double[][] ds) -> new FixedNeuron(ds[0], ds[1][0])).toArray(FixedNeuron[]::new);
    }

	public void setActivation(ActivationFunction act) {
		this.act = act;
	}

    public double[] getWeights() {
        double[] ws = new double[size * inSize];
        weights.getBuffer().get(0, ws);
        return ws;
    }

    public double[] getBiases() {
        double[] bs = new double[size];
        biases.getBuffer().get(0, bs);
        return bs;
    }

    @Override
    protected void finalize() throws Throwable {
        weights.release();
        biases.release();
    }
}
