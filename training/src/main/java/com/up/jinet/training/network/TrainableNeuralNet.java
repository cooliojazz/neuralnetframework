package com.up.jinet.training.network;

import com.up.jinet.NeuralNet;
import com.up.jinet.data.*;
import com.up.jinet.function.activation.ActivationFunction;
import com.up.jinet.function.cost.CostFunction;
import com.up.jinet.training.network.data.CVals;
import com.up.jinet.training.network.data.NetCVals;
import com.up.jinet.training.network.data.TrainableLayerOutput;

/**
 *
 * @author Ricky
 */
public class TrainableNeuralNet implements NeuralNet {

    private static final long serialVersionUID = 1;

    private TrainableNeuronLayer[] layers;
	private CostFunction cost;

    /**
     * Construct with the layers directly.
     *
     * @param layers All the neuron layers
     * @param cost Cost function for the output
     */
    public TrainableNeuralNet(TrainableNeuronLayer[] layers, CostFunction cost) {
        this.layers = layers;
		this.cost = cost;
    }

	/**
	 * Simpler constructor for a single layer network since it won't have an internal width.
	 *
	 * @param in Width of the input
	 * @param out Width of the output
	 * @param act Activation function for the layer
	 * @param cost Cost function for the output
	 */
    public TrainableNeuralNet(int in, int out, ActivationFunction act, CostFunction cost) {
        this(in, out, 0, 1, act, cost);
    }

    /**
     * Construct with the same activation for each layer.
     *
     * @param in Width of the input
     * @param out Width of the output
     * @param width Width of the internal layers
     * @param height Amount of layers, including output
     * @param act Activation function for all layers
     * @param cost Cost function for the output
     */
    public TrainableNeuralNet(int in, int out, int width, int height, ActivationFunction act, CostFunction cost) {
        layers = new TrainableNeuronLayer[height];
        if (height == 1) {
            layers[0] = new TrainableNeuronLayer(out, in, act);
        } else {
            layers[0] = new TrainableNeuronLayer(width, in, act);
            for (int i = 1; i < height - 1; i++) {
                layers[i] = new TrainableNeuronLayer(width, width, act);
            }
            layers[height - 1] = new TrainableNeuronLayer(out, width, act);
        }
		this.cost = cost;
    }

    /**
     *
     *
     * @param in Width of the input
     * @param out Width of the output
     * @param width Width of all the internal layers
     * @param height Amount of layers, including output
     * @param act Activation function for each layer
     * @param cost Cost function for the output
     */
    public TrainableNeuralNet(int in, int out, int width, int height, ActivationFunction[] act, CostFunction cost) {
        layers = new TrainableNeuronLayer[height];
        if (height == 1) {
            layers[0] = new TrainableNeuronLayer(out, in, act[0]);
        } else {
            layers[0] = new TrainableNeuronLayer(width, in, act[0]);
            for (int i = 1; i < height - 1; i++) {
                layers[i] = new TrainableNeuronLayer(width, width, act[i]);
            }
            layers[height - 1] = new TrainableNeuronLayer(out, width, act[height - 1]);
        }
		this.cost = cost;
    }

    /**
     *
     *
     * @param in Width of the input
     * @param out Width of the output
     * @param width Width of each of the internal layers
     * @param act Activation function for each layer
     * @param cost Cost function for the output
     */
    public TrainableNeuralNet(int in, int out, int[] width, ActivationFunction[] act, CostFunction cost) {
        int height = width.length + 1;
        layers = new TrainableNeuronLayer[height];
        if (height == 1) {
            layers[0] = new TrainableNeuronLayer(out, in, act[0]);
        } else {
            layers[0] = new TrainableNeuronLayer(width[0], in, act[0]);
            for (int i = 1; i < height - 1; i++) {
                layers[i] = new TrainableNeuronLayer(width[i], width[i - 1], act[i]);
            }
            layers[height - 1] = new TrainableNeuronLayer(out, width[height - 2], act[height - 1]);
        }
		this.cost = cost;
    }

	public CostFunction getCost() {
		return cost;
	}
    
    public FullNetOutput<TrainableLayerOutput> get(double[] input) {
        TrainableLayerOutput[] outs = new TrainableLayerOutput[layers.length];
        for (int i = 0; i < layers.length; i++) {
			outs[i] = layers[i].get(input);
			input = outs[i].getOut();
        }
        return new FullNetOutput<TrainableLayerOutput>(outs);
    }
    
    public NetCVals backPropagate(double[] input, FullNetOutput<TrainableLayerOutput> out, double[] act) {
		//TODO: Is there a better way to use/write NetCvals now?
        CVals[] cvals = new CVals[layers.length];

        double[] lerr = null;
        for (int i = layers.length - 1; i >= 0; i--) {
            TrainableNeuronLayer layer = layers[i];
            if (i == layers.length - 1) {
                lerr = layer.error(out.getLayerOutputs()[i].getOut(), out.getLayerOutputs()[i].getDOut(), act, cost);
            } else {
                lerr = layer.error(layers[i + 1], lerr, out.getLayerOutputs()[i].getDOut());
            }

            double[][] cws = new double[layer.size()][layer.getNeurons()[0].size()];
            for (int j = 0; j < layer.size(); j++) {
                for (int k = 0; k < layer.getNeurons()[0].size(); k++) {
                    cws[j][k] = (i == 0 ? input[k] : out.getLayerOutputs()[i - 1].getOut()[k]) * lerr[j];
                }
            }
            cvals[i] = new CVals(cws, lerr);
        }
        return new NetCVals(cvals);
    }
    
    public void correct(NetCVals cvals, NetCVals lastCvals, HyperParameters params) {
        for (int i = 0; i < layers.length; i++) {
            layers[i].correct(cvals.getCVal(i).cWeights, lastCvals.getCVal(i).cWeights, cvals.getCVal(i).cBiases, lastCvals.getCVal(i).cBiases, params);
        }
    }
    
    public TrainableNeuronLayer[] getLayers() {
        return layers;
    }
    
    public int height() {
        return layers.length;
    }
	
	public double[][][][] toArray() {
		double[][][][] net = new double[layers.length][][][];
        for (int l = 0; l < layers.length; l++) {
            TrainableNeuron[] neurons = layers[l].getNeurons();
			net[l] = new double[neurons.length][][];
			for (int n = 0; n < neurons.length; n++) {
				double[] weights = neurons[n].getWeights();
				net[l][n] = new double[2][];
				net[l][n][0] = new double[weights.length];
				System.arraycopy(weights, 0, net[l][n][0], 0, weights.length);
				net[l][n][1] = new double[1];
				net[l][n][1][0] = neurons[n].getBias();
			}
		}
		return net;
	}
	
	public static TrainableNeuralNet fromArray(double[][][][] net, ActivationFunction[] acts, CostFunction cost) {
		TrainableNeuronLayer[] layers = new TrainableNeuronLayer[net.length];
        for (int l = 0; l < net.length; l++) {
            TrainableNeuron[] neurons = new TrainableNeuron[net[l].length];
			for (int n = 0; n < neurons.length; n++) {
				neurons[n] = new TrainableNeuron(net[l][n][0], net[l][n][1][0]);
			}
			layers[l] = new TrainableNeuronLayer(neurons, acts[l]);
		}
		return new TrainableNeuralNet(layers, cost);
	}
}
