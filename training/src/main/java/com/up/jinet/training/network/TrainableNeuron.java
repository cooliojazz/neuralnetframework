package com.up.jinet.training.network;

import com.up.jinet.Neuron;

/**
 *
 * @author Ricky
 */
public class TrainableNeuron implements Neuron {

    private static final long serialVersionUID = 1;

    private double[] weights;
    private double bias;

    public TrainableNeuron(double[] weights, double bias) {
        this.weights = weights;
        this.bias = bias;
    }

    public TrainableNeuron(int size, double randomRange) {
        weights = new double[size];
        for (int i = 0; i < weights.length; i++) {
            weights[i] = (Math.random() * 2 - 1) * randomRange;
        }
//        bias = (Math.random() * 2 - 1) * 1;
        bias = 0;
    }
    
    public double getInternal(double[] input) {
        if (input.length != weights.length) throw new IndexOutOfBoundsException("Input size must match weight size");
        double total = 0;
        for (int i = 0; i < weights.length; i++) {
            total += weights[i] * input[i];
        }
        return total + bias;
    }

    public void correct(double[] cWeight, double[] lastCW, double cBias, double lastCB, HyperParameters params) {
//		// Without momentum
//        for (int i = 0; i < weights.length; i++) {
//            weights[i] -= rate * cWeight[i] / batchSize;
//        }
//        bias -= rate * cBias / batchSize;

        for (int i = 0; i < weights.length; i++) {
			lastCW[i] = lastCW[i] * params.momentum() + params.trainRatio() * cWeight[i] / params.batchSize();
            weights[i] = (1 - params.trainRatio() * params.regularization() / params.batchSize()) * weights[i] - lastCW[i];
        }
		lastCB = lastCB * params.momentum() + params.trainRatio() * cBias / params.batchSize();
        bias -= lastCB;
		
    }
    
    public int size() {
        return weights.length;
    }
    
    public double[] getWeights() {
        return weights;
    }
    
    public void setWeights(double[] weights) {
		if (this.weights.length != weights.length) System.out.println("Warning: weights length changed; something is probably wrong. (" + this.weights.length + " to " + weights.length + ")");
        this.weights = weights;
    }

	public double getBias() {
		return bias;
	}
    
    public void setBias(double b) {
        this.bias = b;
    }
}
