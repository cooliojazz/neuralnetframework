package com.up.jinet.training;

import com.up.jinet.NetLoader;
import com.up.jinet.data.*;
import com.up.jinet.training.network.HyperParameters;
import com.up.jinet.training.network.data.NetCVals;
import com.up.jinet.training.network.data.TrainableLayerOutput;
import com.up.jinet.training.network.TrainableNeuralNet;

import java.io.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;

/**
 * Only properly trains for single-class classification currently
 *
 * @author Ricky
 */
public class DefaultNetTrainer extends NetTrainer<TrainableNeuralNet, FullNetOutput<TrainableLayerOutput>[]> {

	public DefaultNetTrainer(TrainableNeuralNet net, int samples, int epochs, int checkpoints, HyperParameters params, boolean randomBatches, File saveFolder, BiConsumer<DefaultNetTrainer, NetStatistics> batchCallback) {
		super(net, samples, epochs, checkpoints, params, randomBatches, saveFolder, (t, s) -> batchCallback.accept((DefaultNetTrainer)t, s));
	}
	
	@Override
	protected void setup(TrainableNeuralNet net) {
		
	}
	
	@Override
	protected FullNetOutput<TrainableLayerOutput>[] evaluate(TrainableNeuralNet net, BatchedDataSet data) {
		FullNetOutput<TrainableLayerOutput>[] out = new FullNetOutput[data.size()];
		IntStream.range(0, data.size()).parallel().forEach(i -> out[i] = net.get(data.inputs()[i]));
		return out;
	}
	
	@Override
	protected NetCVals findError(TrainableNeuralNet net, BatchedDataSet data, FullNetOutput<TrainableLayerOutput>[] output) {
		return IntStream.range(0, data.size()).parallel()
						.mapToObj(i -> net.backPropagate(data.inputs()[i], output[i], data.outputs()[i]))
						.reduce(NetCVals.initializeForNet(net), NetCVals::merge);
	}
	
	@Override
	protected void correct(TrainableNeuralNet net, NetCVals netCvals, NetCVals lastCvals) {
		net.correct(netCvals, lastCvals, params);
	}
	
	@Override
	protected NetStatistics statistics(TrainableNeuralNet net, BatchedDataSet data, FullNetOutput<TrainableLayerOutput>[] output) {
		Object[] stats = IntStream.range(0, data.size()).parallel()
						.mapToObj(j -> {
							double[] outs = new double[net.height()];
							double[] outDs = new double[net.height()];
							for (int l = 0; l < net.height(); l++) {
								double mean = DoubleStream.of(output[j].getLayerOutputs()[l].getOut()).sum() / output[j].getLayerOutputs()[l].getOut().length;
								outs[l] += mean / params.batchSize();
								outDs[l] += DoubleStream.of(output[j].getLayerOutputs()[l].getOut()).map(d -> Math.pow(d - mean, 2)).sum() / output[j].getLayerOutputs()[l].getOut().length / params.batchSize();
							}
							double cost = net.getCost().get(output[j].getLayerOutputs()[net.height() - 1].getOut(), data.outputs()[j]);
							double correct = 1 - diffMag(output[j].getLayerOutputs()[net.getLayers().length - 1].getOut(), data.outputs()[j]);
							
							return new Object[] {outs, outDs, cost, correct};
						})
						.reduce(new Object[] {new double[net.height()], new double[net.height()], 0d, 0d}, (a, b) ->
							new Object[] {sum((double[])a[0], (double[])b[0]), sum((double[])a[1], (double[])b[1]), (double)a[2] + (double)b[2], (double)a[3] + (double)b[3]});
		return new NetStatistics((double[])stats[0], (double[])stats[1], (double)stats[2], (double)stats[3]);
	}
	
	private double[] sum(double[] a, double[] b) {
		double[] o = new double[a.length];
		IntStream.range(0, a.length).parallel().forEach(i -> o[i] = a[i] + b[i]);
		return o;
	}
	
	@Override
	protected void updateParameters(TrainableNeuralNet net, NetStatistics stats) {
		params = new HyperParameters(params.batchSize(), params.testSize(), params.trainRatio() * params.ratioDecay(), params.ratioDecay(), params.momentum(), params.regularization());
	}
	
	@Override
	protected void cleanup() {
		
	}
	
	/**
	 * Loads the network, training config, and last state from a file
	 *
	 */
	public static DefaultNetTrainer fromRun(File saveFolder, BiConsumer<DefaultNetTrainer, NetStatistics> batchCallback) throws IOException, ClassNotFoundException {
		ObjectInputStream is = new ObjectInputStream(new FileInputStream(new File(saveFolder, "run-info.ntd")));
		DefaultNetTrainer trainer = new DefaultNetTrainer(NetLoader.load(new File(saveFolder, "current-net.nwa")), is.readInt(), is.readInt(), is.readInt(), (HyperParameters)is.readObject(), is.readBoolean(), saveFolder, batchCallback);
		trainer.batch = is.readInt();
		is.close();
		return trainer;
	}
}
