package com.up.jinet.training.network.data;

import com.up.jinet.NeuralNet;
import com.up.jinet.training.network.TrainableNeuralNet;

/**
 *
 * @author Ricky
 */
public class NetCVals {
	
	private CVals[] cvals;

	public NetCVals(int... layerSizes) {
		cvals = new CVals[layerSizes.length - 1];
		for (int i = 0; i < layerSizes.length - 1; i++) {
			cvals[i] = new CVals(layerSizes[i], layerSizes[i + 1]);
		}
	}

	public NetCVals(CVals[] cvals) {
		this.cvals = cvals;
	}

	public CVals getCVal(int layer) {
		return cvals[layer];
	}

	public CVals[] getCVals() {
		return cvals;
	}
	
	public void sum(NetCVals other) {
		for (int i = 0; i < cvals.length; i++) {
			cvals[i].sum(other.cvals[i]);
		}
	}
	
	public NetCVals merge(NetCVals other) {
		CVals[] merged = new CVals[cvals.length];
		for (int i = 0; i < cvals.length; i++) {
			merged[i] = cvals[i].merge(other.cvals[i]);
		}
		return new NetCVals(merged);
	}
	
	public double[][][][] toArray() {
		double[][][][] arr = new double[cvals.length][][][];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = new double[2][][];
			arr[i][0] = cvals[i].cWeights;
			arr[i][1] = new double[][] {cvals[i].cBiases};
		}
		return arr;
	}
	
	public static NetCVals fromArray(double[][][][] arr) {
		CVals[] cvals = new CVals[arr.length];
		for (int i = 0; i < arr.length; i++) {
			cvals[i] = new CVals(arr[i][0], arr[i][1][0]);
		}
		return new NetCVals(cvals);
	}
	
	public static NetCVals initializeForNet(NeuralNet net) {
		int[] sizes = new int[net.height() + 1];
		sizes[0] = net.getLayers()[0].inSize();
		for (int i = 1; i < sizes.length; i++) sizes[i] = net.getLayers()[i - 1].size();
		return new NetCVals(sizes);
	}
	
}
