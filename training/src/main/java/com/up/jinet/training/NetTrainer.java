package com.up.jinet.training;

import com.up.datavis.grapher.DataRendering;
import com.up.datavis.grapher.DataSet;
import com.up.datavis.grapher.DataSetGroup;
import com.up.datavis.grapher.Grapher;
import com.up.jinet.NetLoader;
import com.up.jinet.NeuralNet;
import com.up.jinet.training.network.HyperParameters;
import com.up.jinet.training.network.data.NetCVals;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.Point2D;
import java.io.*;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.BiConsumer;
import java.util.stream.DoubleStream;

/**
 * Only properly trains for single-class classification currently
 *
 * @author Ricky
 */
public abstract class NetTrainer<T extends NeuralNet, U> {
	
	private T net;
	private final int samples;
	private final int epochs;
	private final int checkpoints;
	private final int iterLen;
	private final boolean randomBatches;
	protected int batch = 0;
	protected HyperParameters params;
	
	private final File saveFolder;
	private final BiConsumer<NetTrainer<T, U>, NetStatistics> batchCallback;

	// TODO: Decide what to do with this since batchSize is part of the hyperparameters directly, this has no way to provide the convenice it did previously.
//	/**
//	 * Set up for use without batching the data.
//	 *
//	 * @param samples Total number of samples that will be in the training data.
//	 * @param epochs Number of times the entire data set should be trained on.
//	 * @param checkpoints How many checkpoints should be taken throughout the run.
//	 * @param params Hyperparameters to train the network with.
//	 * @param saveFolder Where to save the run information and checkpoints.
//	 * @param batchCallback An optional function to call at the end of processing each batch.
//	 */
//	public NetTrainer(int samples, int epochs, int checkpoints, HyperParameters params, File saveFolder, Runnable batchCallback) {
//		this.samples = samples;
//		this.batchSize = samples;
//		this.epochs = epochs;
//		this.checkpoints = checkpoints;
//		this.params = params;
//		this.randomBatches = false;
//		this.saveFolder = saveFolder;
//		this.batchCallback = batchCallback;
//		iterLen = epochs;
//	}

	/**
	 * Set up for batching the data during training.
	 * 
	 * @param net Network to train
	 * @param samples Total number of samples that will be in the training data.
	 * @param epochs Number of times (approximately if random) the entire data set should be trained on.
	 * @param checkpoints How many checkpoints should be taken throughout the run.
	 * @param params Hyperparameters to train the network with.
	 * @param saveFolder Where to save the run information and checkpoints.
	 * @param randomBatches Whether the batches should be randomly selected or taken as consecutive sub-sets of the training data.
	 * @param batchCallback An optional function to call at the end of processing each batch that takes the current batch number.
	 */
	public NetTrainer(T net, int samples, int epochs, int checkpoints, HyperParameters params, boolean randomBatches, File saveFolder, BiConsumer<NetTrainer<T, U>, NetStatistics> batchCallback) {
		this.net = net;
		this.samples = samples;
		this.epochs = epochs;
		this.checkpoints = checkpoints;
		this.params = params;
		this.randomBatches = randomBatches;
		this.saveFolder = saveFolder;
		this.batchCallback = batchCallback;
		iterLen = samples / params.batchSize() * epochs;
		setup(net);
	}

	/**
	 * Starts training the given network.
	 *
	 * @param training Which data to train on.
	 * @param testing Which data should be used for checking progress. Should be different from data passed into training to be useful and preferably at least 1000 samples.
	 */
	public NetStatistics train(NetDataSet training, NetDataSet testing, boolean save, boolean ui, boolean autoHide) {
		if (save) {
			if (!saveFolder.exists()) {
				saveFolder.mkdirs();
				new File(saveFolder, "checkpoints").mkdirs();
				new File(saveFolder, "graphs").mkdirs();
			}
			try {
				saveData(new File(saveFolder, "training.dat"), training);
				saveData(new File(saveFolder, "testing.dat"), testing);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		Frame f = null;
		Grapher g = null;
		if (ui) {
			f = new Frame();
			f.setSize(800, 1000);
			g = new Grapher();
			f.add(g);
			f.setVisible(true);
		}
		ArrayList<DataSet> sets = new ArrayList<>();
		DataSet[] statLayerDeviations = new DataSet[net.height()];
		DataSet[] statLayerMeans = new DataSet[net.height()];
		for (int l = 0; l < net.height(); l++) {
			statLayerDeviations[l] = new DataSet(new Rectangle(1, 1), new CopyOnWriteArrayList<>(), "Mean Deviation Activation Layer " + l, DataRendering.DrawType.RANGE_WITH_POINTS);
			statLayerDeviations[l].setPointStyle(new DataRendering.PointStyle(2, DataRendering.PointStyle.Type.SQUARE));
			statLayerDeviations[l].setEnabled(false);
			sets.add(statLayerDeviations[l]);
		}
		for (int l = 0; l < net.height(); l++) {
			statLayerMeans[l] = new DataSet(new Rectangle(1, 1), new CopyOnWriteArrayList<>(), "Mean Activation Layer " + l, DataRendering.DrawType.POINTS_AND_LINES);
			statLayerMeans[l].setEnabled(false);
			sets.add(statLayerMeans[l]);
			
			statLayerDeviations[l].setAround(statLayerMeans[l]);
		}
		DataSet statTestCorrect = new DataSet(new Rectangle(1, params.batchSize()), new CopyOnWriteArrayList<>(), "Testing Correct", DataRendering.DrawType.POINTS_AND_LINES);
		sets.add(statTestCorrect);
		statTestCorrect.setPointStyle(new DataRendering.PointStyle(5, DataRendering.PointStyle.Type.SQUARE));
		DataSet statCorrect = new DataSet(new Rectangle(1, params.batchSize()), new CopyOnWriteArrayList<>(), "Training Correct", DataRendering.DrawType.POINTS_AND_LINES);
		sets.add(statCorrect);
		statCorrect.setPointStyle(new DataRendering.PointStyle(4, DataRendering.PointStyle.Type.CIRCLE));
		DataSet statCost = new DataSet(new Rectangle(1, 1), new CopyOnWriteArrayList<>(), "Log Sqrt Cost", DataRendering.DrawType.POINTS_AND_LINES);
		statCost.setPointStyle(new DataRendering.PointStyle(4, DataRendering.PointStyle.Type.CIRCLE));
		sets.add(statCost);
		DataSet statTrainRate = new DataSet(new Rectangle(1, 1), new CopyOnWriteArrayList<>(), "Training Rate", DataRendering.DrawType.POINTS_AND_LINES);
		sets.add(statTrainRate);
//		Grapher.DataSet statTargetMeanMean = new Grapher.DataSet(new Rectangle(1, 1), new CopyOnWriteArrayList<>(), "Target Mean-Mean", Grapher.DrawType.POINTS_AND_LINES);
//		sets.add(statTargetMeanMean);
		DataSet statMeanMean = new DataSet(new Rectangle(1, 1), new CopyOnWriteArrayList<>(), "Mean +Layer Mean", DataRendering.DrawType.POINTS_AND_LINES);
		sets.add(statMeanMean);
		DataSetGroup group = new DataSetGroup(new DataRendering.ColorRange(new Color(0.8f, 0.3f, 0.1f), new Color(0.1f, 0.3f, 0.8f)), "Stats");
		if (ui) {
			sets.forEach(group::addDataSet);
			g.addGroup(group);
		}
		
//		System.out.println("Beginning network training");
		NetCVals lastCvals = NetCVals.initializeForNet(net);
		NetStatistics lastStats = null;
//		setup(net);
		for (batch = 0; batch < iterLen; batch++) {
			long startTime = System.nanoTime();
			
			long time = System.nanoTime();
			BatchedDataSet batched = makeBatch(training, params.batchSize(), batch * params.batchSize(), randomBatches);
//			System.out.println("Batched Data (Took " + (System.nanoTime() - time) / 1000000d + "ms)");
			
			time = System.nanoTime();
			U output = evaluate(net, batched);
//			System.out.println("Forward Pass (Took " + (System.nanoTime() - time) / 1000000d + "ms)");
			
			time = System.nanoTime();
			NetCVals netCvals = findError(net, batched, output);
//			System.out.println("Backpropagation (Took " + (System.nanoTime() - time) / 1000000d + "ms)");

//			for (int l = 0; l < net.height(); l++) {
//				errs[batch][l] = DoubleStream.of(netCvals.getCVal(l).cBiases).sum() / netCvals.getCVal(l).cBiases.length;
//			}
			
			time = System.nanoTime();
			NetStatistics stats = statistics(net, batched, output);
//			System.out.println("Calculated statistics (Took " + (System.nanoTime() - time) / 1000000d + "ms)");
			
			NetStatistics testStats = null;
			if (params.testSize() > 0) {
				time = System.nanoTime();
				BatchedDataSet batchedTests = makeBatch(testing, params.testSize(), 0, randomBatches);
				U testOutput = evaluate(net, batchedTests);
				testStats = statistics(net, batchedTests, testOutput);
//			System.out.println("Ran tests and got stats (Took " + (System.nanoTime() - time) / 1000000d + "ms)");
			}
			
			time = System.nanoTime();
			correct(net, netCvals, lastCvals);
//			System.out.println("Corrected batch (Took " + (System.nanoTime() - time) / 1000000d + "ms)");
			
			time = System.nanoTime();
			updateParameters(net, stats);
			
			if (ui) {
				double meanMagLayerMean = DoubleStream.of(stats.layerMean).map(Math::abs).average().orElse(0);
				statMeanMean.getData().add(new Point2D.Double(batch, meanMagLayerMean * params.batchSize()));
//			statTargetMeanMean.getData().add(new Point2D.Double(batch, trainPid.target * params.batchSize()));
				statTrainRate.getData().add(new Point2D.Double(batch, (Math.log10(params.trainRatio()) + 5) / 7 * params.batchSize()));
				
				for (int l = 0; l < net.height(); l++) {
					statLayerDeviations[l].getData().add(new Point2D.Double(batch, stats.layerStdDeviation[l] * params.batchSize()));
					statLayerDeviations[l].getBounds().width = batch + 1;
					statLayerMeans[l].getData().add(new Point2D.Double(batch, stats.layerMean[l] * params.batchSize()));
					statLayerMeans[l].getBounds().width = batch + 1;
				}
//			for (int l = 0; l < net.height(); l++) {
//				points = new Point2D[batch + 1];
//				for (int j = 0; j <= batch; j++) {
//					points[j] = new Point2D.Double(j, errs[j][l] * batchSize);
//				}
//				sets.add(new Grapher.DataSet(new Rectangle(batch + 1, 1), Arrays.asList(points), "Error Layer " + l));
//			}
				
				statCost.getData().add(new Point2D.Double(batch, (Math.log10(Math.sqrt(stats.cost)) - 1) * params.batchSize() / 2d + params.batchSize() / 2d));
				statCost.getBounds().width = batch + 1;
				statCorrect.getData().add(new Point2D.Double(batch, stats.correct));
				statCorrect.getBounds().width = batch + 1;
				
				if (testStats != null) {
					statTestCorrect.getData().add(new Point2D.Double(batch, testStats.correct / params.testSize() * params.batchSize()));
					statTestCorrect.getBounds().width = batch + 1;
				}
			}
			
			if (batchCallback != null) batchCallback.accept(this, stats);
//			System.out.println("Updated statistic stores (Took " + (System.nanoTime() - time) / 1000000d + "ms)");
			
			if (save) {
				try {
					NetLoader.save(net, new File(saveFolder, "current-net.nwa"));
				} catch (IOException e) {
					e.printStackTrace();
				}
				if (checkpoints > 0 && batch % (iterLen / checkpoints) == 0) {
					try {
						saveRun(new File(saveFolder, "run-info.ntd"));
						NetLoader.save(net, new File(saveFolder, "checkpoints/checkpoint-" + batch + ".nwa"));
						if (ui) ImageIO.write(g.getRenderer().render(1920, 1080, 100000), "png", new File(saveFolder, "graphs/graph-" + batch + ".png"));
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			
			lastStats = stats;
			
//			System.out.println("Batch " + batch + ": " + stats.correct + "/" + params.batchSize() + " (Took " + (System.nanoTime() - startTime) / 1000000d + "ms)\n");
		}
		
		cleanup();
		
		if (ui && autoHide) {
			f.setVisible(false);
		}
		
		return lastStats;
	}
	
	public int getBatch() {
		return batch;
	}
	
	protected abstract void setup(T net);
	protected abstract U evaluate(T net, BatchedDataSet data);
	protected abstract NetCVals findError(T net, BatchedDataSet data, U output);
	protected abstract void correct(T net, NetCVals netCvals, NetCVals lastCvals);
	protected abstract NetStatistics statistics(T net, BatchedDataSet data, U output);
	protected abstract void updateParameters(T net, NetStatistics stats);
	protected abstract void cleanup();

//	/**
//	 * Continues training the network from this run.
//	 *
//	 */
//	public void resumeTrain(boolean ui, boolean autoHide) throws IOException, ClassNotFoundException {
//		resumeTrain(NetLoader.load(new File(saveFolder, "current-net.nwa")), ui, autoHide);
//	}

	public void resumeTrain(boolean ui, boolean autoHide) throws IOException, ClassNotFoundException {
		train(loadData(new File(saveFolder, "training.dat")), loadData(new File(saveFolder, "testing.dat")), true, ui, autoHide);
	}
	
	private void saveData(File f, NetDataSet data) throws IOException {
		ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(f));
		os.writeObject(data);
		os.close();
	}

	private NetDataSet loadData(File f) throws IOException, ClassNotFoundException {
		ObjectInputStream is = new ObjectInputStream(new FileInputStream(f));
		NetDataSet data = (NetDataSet)is.readObject();
		is.close();
		return data;
	}
	
	private void saveRun(File f) throws IOException {
		ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(f));
		os.writeInt(samples);
		os.writeInt(epochs);
		os.writeInt(checkpoints);
		os.writeObject(params);
		os.writeBoolean(randomBatches);
		os.writeInt(batch);
		os.close();
	}

	public HyperParameters getParameters() {
		return params;
	}

	public void setParameters(HyperParameters params) {
		this.params = params;
	}
	
	private BatchedDataSet makeBatch(NetDataSet data, int amount, int offset, boolean randomBatches) {
		double[][] inputs = new double[amount][];
		double[][] outputs = new double[amount][];
		for (int i = 0; i < amount; i++) {
			int smpl = randomBatches ? (int)(Math.random() * data.size()) : (offset + i) % data.size();
			inputs[i] = data.getInput(smpl);
			outputs[i] = data.getOutput(smpl);
		}
		return new BatchedDataSet(inputs, outputs, amount);
	}
	
	protected static double diffMag(double[] a, double[] b) {
		double sum = 0;
		for (int i = 0; i < a.length; i++) {
			sum += Math.pow(a[i] - b[i], 2);
		}
		return Math.sqrt(sum);
	}
	
	public static int getOutput(double[] out) {
        double max = -1;
        int res = 0;
        for (int i = 0; i < out.length; i++) {
            if (out[i] > max) {
                max = out[i];
                res = i;
            }
        }
        return res;
    }
	
	public record BatchedDataSet(double[][] inputs, double[][] outputs, int size) {}
	
	public record NetStatistics(double[] layerMean, double[] layerStdDeviation, /*double[] layerError,*/ double cost, double correct) {}

}
