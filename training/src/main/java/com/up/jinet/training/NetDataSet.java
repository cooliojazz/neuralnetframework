package com.up.jinet.training;

import java.io.Serializable;

/**
 *
 * @author Ricky
 */
public class NetDataSet implements Serializable {
	
	double[][] inputs;
	double[][] outputs;

	public NetDataSet(double[][] input, double[][] outputs) {
		if (input.length != outputs.length) throw new RuntimeException("Input and output size must match in data sets.");
		this.inputs = input;
		this.outputs = outputs;
	}

	public int size() {
		return inputs.length;
	}

	public double[] getInput(int i) {
		return inputs[i];
	}

	public double[] getOutput(int i) {
		return outputs[i];
	}
}
