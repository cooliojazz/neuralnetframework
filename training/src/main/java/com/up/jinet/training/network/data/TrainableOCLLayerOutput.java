package com.up.jinet.training.network.data;

import com.up.jinet.data.LayerOutput;

/**
 *
 * @author Ricky
 */
public class TrainableOCLLayerOutput extends LayerOutput {

	private double[] dOut;

	public TrainableOCLLayerOutput(double[] out, double[] dOut) {
		super(out);
		this.dOut = dOut;
	}

	public double[] getDOut() {
		return dOut;
	}
}
