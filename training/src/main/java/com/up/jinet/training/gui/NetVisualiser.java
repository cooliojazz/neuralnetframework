package com.up.jinet.training.gui;

import com.up.jinet.NeuralNet;
import com.up.jinet.Neuron;
import com.up.jinet.NeuronLayer;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.image.BufferedImage;

/**
 *
 * @author Ricky
 */
public class NetVisualiser extends Canvas {

	private boolean showInputs;
	private NeuralNet net;
	private double logScale = 0.1;
	private final double neruonRadius = 6;
	private final double neruonOffset = neruonRadius + 0.5;
	private final int neruonSize = (int)(neruonOffset * 2);
	private Neuron selected = null;

	public NetVisualiser(NeuralNet net, boolean showInputs) {
		this.net = net;
		this.showInputs = showInputs;
		setMinimumSize(new Dimension(200, 150));
		
		addMouseMotionListener(new MouseAdapter() {
				@Override
				public void mouseMoved(MouseEvent e) {
					selected = null;
					int width = net.getLayers().length;
					double xres = (double)getWidth() / (width + 1);
					for (int x = showInputs ? 0 : 1; x < width; x++) {
						NeuronLayer layer = net.getLayers()[x];
						double yres = (double)getHeight() / layer.size();
						for (int y = 0; y < layer.size(); y++) {
							Neuron n = layer.getNeurons()[y];
							if (Math.sqrt(Math.pow(Math.round((x + 1.5) * xres) - e.getX(), 2)+ Math.pow(Math.round((y + 0.5) * yres) - e.getY(), 2)) < neruonOffset) selected = n;
						}
					}
					repaint();
				}

			});
		addMouseWheelListener(new MouseAdapter() {
				@Override
				public void mouseWheelMoved(MouseWheelEvent e) {
					logScale *= Math.exp(e.getPreciseWheelRotation() / 5);
					repaint();
				}
			});
	}

	@Override
	public void paint(Graphics g) {
		BufferedImage img = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_4BYTE_ABGR);
		Graphics ig = img.createGraphics();
		
		ig.setColor(Color.gray.darker());
		ig.fillRect(0, 0, getWidth(), getHeight());
		int width = net.getLayers().length;
		double xRes = (double)getWidth() / (width + 1);
		for (int x = showInputs ? 0 : 1; x < width; x++) {
			NeuronLayer layer = net.getLayers()[x];
			double yRes = (double)getHeight() / layer.size();
			for (int y = 0; y < layer.size(); y++) {
				Neuron n = layer.getNeurons()[y];
				for (int i = 0; i < n.size(); i++) {
					double iRes = (double)getHeight() / n.size();
					double logWeight = Math.log10(Math.abs(n.getWeights()[i] / logScale));

//					ig.setColor(new Color((float)Math.min(1d, Math.max(0d, n.getWeights()[i] < 0 ? logWeight + 0.5 : 0)), (float)Math.min(1d, Math.max(0d, n.getWeights()[i] > 0 ? logWeight + 0.5 : 0)), n.getWeights()[i] == 0 ? 0.5f : 0f));
					double red = n.getWeights()[i] < 0 ? 0.15 + Math.min(1, Math.max(0, logWeight)) * 0.85 : 0;
					double green = n.getWeights()[i] > 0 ? 0.15 + Math.min(1, Math.max(0, logWeight)) * 0.85 : 0;
//					float blue = n.getWeights()[i] == 0 ? 0.5f : 0f;
					double blue = 1 - Math.min(1, Math.max(0, logWeight));
					float alpha = 0.2f + (selected == null ? 0.4f : (n != selected ? 0 : 0.75f));
//					ig.setColor(new Color((float)red, (float)green, (float)blue, alpha));
					ig.setColor(getColor(n.getWeights()[i], logWeight, logWeight, alpha));

//					int lineSize = 3;
//					for (int j = -lineSize; j < lineSize; j++) {
//						ig.drawLine((int)Math.round((x + 1.5) * xRes), (int)Math.round((y + 0.5) * yRes + j),
//								(int)Math.round(((x - 1) + 1.5) * xRes), (int)Math.round((i + 0.5) * iRes + j));
//					}
					ig.drawLine((int)Math.round((x + 1.5) * xRes), (int)Math.round((y + 0.5) * yRes),
							(int)Math.round(((x - 1) + 1.5) * xRes), (int)Math.round((i + 0.5) * iRes));
				}
			}
		}
		
		NeuronLayer inLayer = net.getLayers()[0];
		double inyres = (double)getHeight() / inLayer.getNeurons()[0].size();
		for (int y = 0; y < inLayer.getNeurons()[0].size(); y++) {
			ig.setColor(Color.white);
			ig.fillRect((int)Math.round(0.5 * xRes - neruonOffset), (int)Math.round((y + 0.5) * inyres - neruonOffset), neruonSize, neruonSize);
			ig.setColor(Color.black);
			ig.drawRect((int)Math.round(0.5 * xRes - neruonOffset), (int)Math.round((y + 0.5) * inyres - neruonOffset), neruonSize, neruonSize);
		}
		for (int x = 0; x < width; x++) {
			NeuronLayer layer = net.getLayers()[x];
			double yres = (double)getHeight() / layer.size();
			for (int y = 0; y < layer.size(); y++) {
				Neuron n = layer.getNeurons()[y];
				double logBias = Math.log10(Math.abs(n.getBias()) / logScale);
				double maxWeight = 0;
				double total = 0;
				double nSum = 0, pSum = 0;
				for (int i = 0; i < n.size(); i++) {
					if (Math.log10(Math.abs(n.getWeights()[i]) / logScale) > maxWeight) maxWeight = Math.log10(Math.abs(n.getWeights()[i]) / logScale);
					total += Math.log10(Math.abs(n.getWeights()[i]));

					if (n.getWeights()[i] < 0) nSum += n.getWeights()[i]; // Based on a bad generalization that most activation functions output within the range [-1,1], most likely meaningless for anything else
					if (n.getWeights()[i] > 0) pSum += n.getWeights()[i];
				}
//				double[] stuff = layer.getActivation().get(new double[] {-total, total});
				double[] stuff = layer.getActivation().get(new double[] {nSum, pSum});
//				dead = total > 0;

				double red = n.getBias() < 0 ? 0.15 + Math.min(1, Math.max(0, logBias)) * 0.85 : 0;
				double green = n.getBias() > 0 ? 0.15 + Math.min(1, Math.max(0, logBias)) * 0.85 : 0;
				double blue = 1 - Math.min(1d, Math.max(0d, maxWeight));
//				ig.setColor(new Color((float)Math.min(1d, Math.max(0d, n.getBias() < 0 ? logBias : 0)), (float)Math.min(1d, Math.max(0d, n.getBias() > 0 ? logBias : 0)), dead ? 1f : 0f));
//				ig.setColor(new Color((float)Math.min(1d, Math.max(0d, n.getBias() < 0 ? logBias : 0)), (float)Math.min(1d, Math.max(0d, n.getBias() > 0 ? logBias : 0)), 1 - (float)Math.min(1d, Math.max(0d, maxWeight / n.size()))));
//				ig.setColor(new Color((float)red, (float)green, (float)blue));
//				ig.setColor(getColor(n.getBias(), logBias, maxWeight, 1));
				ig.setColor(getColor(n.getBias(), logBias, Math.max(Math.log10(Math.abs(stuff[0]) / logScale), Math.log10(Math.abs(stuff[1]) / logScale)), 1));

				ig.fillOval((int)Math.round((x + 1.5) * xRes - neruonOffset), (int)Math.round((y + 0.5) * yres - neruonOffset), neruonSize, neruonSize);
				ig.setColor(Color.black);
				ig.drawOval((int)Math.round((x + 1.5) * xRes - neruonOffset), (int)Math.round((y + 0.5) * yres - neruonOffset), neruonSize, neruonSize);
			}
		}
		
		ig.drawString(logScale + "", 5, 15);
		g.drawImage(img, 0, 0, null);
	}

	private Color getColor(double value, double logValue, double activity, float alpha) {
		double d = 0.3 + Math.min(1, Math.max(0, logValue)) * 0.7;
		double a = Math.min(1, Math.max(0, activity));
		double red = (value < 0 ? d : 0) * (a * 0.7 + 0.3);
		double green = (value > 0 ? d : 0)  * (a * 0.7 + 0.3);
		double blue = (1 - d) * a;
		return new Color((float)red, (float)green, (float)blue, alpha);
	}

	@Override
	public void update(Graphics g) {
		paint(g);
	}
	
}
