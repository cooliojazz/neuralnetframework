package com.up.jinet.training.network.data;

/**
 *
 * @author Ricky
 */
public class CVals {
	
    public double[][] cWeights;
    public double[] cBiases;

    public CVals(int inputSize, int layerSize) {
        this.cWeights = new double[layerSize][inputSize];
        this.cBiases = new double[layerSize];
    }

    public CVals(double[][] cws, double[] cbs) {
        this.cWeights = cws;
        this.cBiases = cbs;
    }
	
	/**
	 * Add another CVals into this one.
	 * @param other 
	 */
	public void sum(CVals other) {
		for (int i = 0; i < cWeights.length; i++) {
			for (int j = 0; j < cWeights[0].length; j++) {
				cWeights[i][j] += other.cWeights[i][j];
			}
			cBiases[i] += other.cBiases[i];
		}
	}
	
	public CVals merge(CVals other) {
		CVals merged = new CVals(cWeights[0].length, cWeights.length);
		for (int i = 0; i < cWeights.length; i++) {
			for (int j = 0; j < cWeights[0].length; j++) {
				merged.cWeights[i][j] = cWeights[i][j] + other.cWeights[i][j];
			}
			merged.cBiases[i] = cBiases[i] + other.cBiases[i];
		}
		return merged;
	}
}
