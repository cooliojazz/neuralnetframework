package com.up.jinet.training.network;

import com.jogamp.opencl.*;
import com.up.jinet.NeuralNet;
import com.up.jinet.data.FullNetOutput;
import com.up.jinet.data.LayerOutput;
import com.up.jinet.data.NetOutput;
import com.up.jinet.function.activation.ActivationFunction;
import com.up.jinet.function.cost.CostFunction;
import com.up.jinet.training.network.data.CVals;
import com.up.jinet.training.network.data.NetCVals;
import com.up.jinet.training.network.data.TrainableLayerOutput;
import com.up.jinet.training.network.data.TrainableOCLLayerOutput;
import com.up.jinet.training.CLRunner;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.DoubleBuffer;
import java.util.HashMap;
import java.util.stream.Stream;

/**
 *
 * @author Ricky
 */
public class TrainableOCLNeuralNet implements NeuralNet {

    private static final long serialVersionUID = 1;

    // TODO: Cannot serialize these nets yet because of the buffers; need a custom solution

    private TrainableOCLNeuronLayer[] layers;
	private CostFunction cost;
    CLBuffer<DoubleBuffer> netCvals;

    // For serialization
    protected TrainableOCLNeuralNet() { }

    /**
     * Construct with the layers directly.
     *
     * @param layers All the neuron layers
     * @param cost Cost function for the output
     */
    public TrainableOCLNeuralNet(TrainableOCLNeuronLayer[] layers, CostFunction cost) {
        this.layers = layers;
		this.cost = cost;
    }

	/**
	 * Simpler constructor for a single layer network since it won't have an internal width.
	 *
	 * @param in Width of the input
	 * @param out Width of the output
	 * @param act Activation function for the layer
	 * @param cost Cost function for the output
	 */
    public TrainableOCLNeuralNet(int in, int out, ActivationFunction act, CostFunction cost) {
        this(in, out, 0, 1, act, cost);
    }

    /**
     * Construct with the same activation for each layer.
     *
     * @param in Width of the input
     * @param out Width of the output
     * @param width Width of the internal layers
     * @param height Amount of layers, including output
     * @param act Activation function for all layers
     * @param cost Cost function for the output
     */
    public TrainableOCLNeuralNet(int in, int out, int width, int height, ActivationFunction act, CostFunction cost) {
        layers = new TrainableOCLNeuronLayer[height];
        if (height == 1) {
            layers[0] = new TrainableOCLNeuronLayer(out, in, act);
        } else {
            layers[0] = new TrainableOCLNeuronLayer(width, in, act);
            for (int i = 1; i < height - 1; i++) {
                layers[i] = new TrainableOCLNeuronLayer(width, width, act);
            }
            layers[height - 1] = new TrainableOCLNeuronLayer(out, width, act);
        }
		this.cost = cost;
    }

    /**
     *
     *
     * @param in Width of the input
     * @param out Width of the output
     * @param width Width of all the internal layers
     * @param height Amount of layers, including output
     * @param act Activation function for each layer
     * @param cost Cost function for the output
     */
    public TrainableOCLNeuralNet(int in, int out, int width, int height, ActivationFunction[] act, CostFunction cost) {
        layers = new TrainableOCLNeuronLayer[height];
        if (height == 1) {
            layers[0] = new TrainableOCLNeuronLayer(out, in, act[0]);
        } else {
            layers[0] = new TrainableOCLNeuronLayer(width, in, act[0]);
            for (int i = 1; i < height - 1; i++) {
                layers[i] = new TrainableOCLNeuronLayer(width, width, act[i]);
            }
            layers[height - 1] = new TrainableOCLNeuronLayer(out, width, act[height - 1]);
        }
		this.cost = cost;
    }

    /**
     *
     *
     * @param in Width of the input
     * @param out Width of the output
     * @param width Width of each of the internal layers
     * @param act Activation function for each layer
     * @param cost Cost function for the output
     */
    public TrainableOCLNeuralNet(int in, int out, int[] width, ActivationFunction[] act, CostFunction cost) {
        int height = width.length + 1;
        layers = new TrainableOCLNeuronLayer[height];
        if (height == 1) {
            layers[0] = new TrainableOCLNeuronLayer(out, in, act[0]);
        } else {
            layers[0] = new TrainableOCLNeuronLayer(width[0], in, act[0]);
            for (int i = 1; i < height - 1; i++) {
                layers[i] = new TrainableOCLNeuronLayer(width[i], width[i - 1], act[i]);
            }
            layers[height - 1] = new TrainableOCLNeuronLayer(out, width[height - 2], act[height - 1]);
        }
		this.cost = cost;
    }

	public CostFunction getCost() {
		return cost;
	}

    public FullNetOutput<TrainableLayerOutput> get(double[] input) {
        TrainableLayerOutput[] outs = new TrainableLayerOutput[layers.length];
        for (int i = 0; i < layers.length; i++) {
            outs[i] = layers[i].get(input);
            input = outs[i].getOut();
        }
        return new FullNetOutput<>(outs);
    }

    public FullNetOutput<TrainableLayerOutput>[] getMany(double[][] inputs, int count) {
        TrainableLayerOutput[][] outs = new TrainableLayerOutput[count][layers.length];
        for (int i = 0; i < layers.length; i++) {
            TrainableLayerOutput[] lOuts = layers[i].getMany(inputs, count);
            for (int j = 0; j < count; j++) {
                outs[j][i] = lOuts[j];
                inputs[j] = outs[j][i].getOut();
            }
        }
        return Stream.of(outs).map(FullNetOutput<TrainableLayerOutput>::new).toArray(FullNetOutput[]::new);
    }

    synchronized public void getManyFast(double[][] ins, int count, CLBuffer<DoubleBuffer> netOutputs) {
        long time = System.nanoTime();
        int maxSize = Stream.of(layers).mapToInt(l -> Math.max(l.inSize(), l.size())).max().orElse(-1);
        CLBuffer<DoubleBuffer> inputs = CLRunner.instance.createBuffer(count * maxSize, CLMemory.Mem.READ_ONLY/*, CLMemory.Mem.ALLOCATE_BUFFER*/);
        maxSize = Stream.of(layers).mapToInt(TrainableOCLNeuronLayer::size).max().orElse(-1);
        CLBuffer<DoubleBuffer> internal = CLRunner.instance.createBuffer(count * maxSize, CLMemory.Mem.READ_WRITE/*, CLMemory.Mem.ALLOCATE_BUFFER*/);
        long bufferTime = System.nanoTime() - time;

        time = System.nanoTime();
        DoubleBuffer inBuf = inputs.getBuffer();
        for (double[] in : ins) inBuf.put(in);
        inBuf.flip();
        long loadInputTime = System.nanoTime() - time;


        time = System.nanoTime();
        HashMap<String, CLEventList[]> events = new HashMap<>();
        CLRunner.KernelInfos infos = new CLRunner.KernelInfos(new CLKernel[0], new HashMap<>());

        CLCommandQueue queue = CLRunner.instance.createQueue(CLCommandQueue.Mode.PROFILING_MODE, CLCommandQueue.Mode.OUT_OF_ORDER_MODE);
        events.put("writeInputs", new CLEventList[] {new CLEventList(1)});
        queue.putWriteBuffer(inputs, false, events.get("writeInputs")[0]);
        queue.putBarrier();
        // TODO: Should probably call fill buffer here to clear the output buffer so kernels can be lazy and not write zeroes if they don't need to.
        //       Except fill buffer doenst exist in this api??? Maybe its a werid version casting thing like in jogl?
        //       Well now you can call my version of fill buffer, so this TODO is on the menu again.
        long preSetupTime = System.nanoTime() - time;

        long setupTime = 0;
        long executeTime = 0;
        int layerOffset = 0;
        for (int i = 0; i < layers.length; i++) {
            time = System.nanoTime();
            infos = infos.merge(layers[i].setupForwardKernels(queue, inputs, internal, netOutputs, count, layerOffset));
            int width = layers[i].size();
            layerOffset += count * width * (width + 1);
            setupTime += System.nanoTime() - time;
        }

        time = System.nanoTime();
//        queue.putReadBuffer(netOutputs, false); // Needed to test, disable later. Or replace with other mechanism more? We need to copy this to a smaller output buffer if we need it.
        queue.finish();
        executeTime += System.nanoTime() - time;
        
        infos = infos.merge(new CLRunner.KernelInfos(new CLKernel[0], events));
        
//        time = System.nanoTime();
//        System.out.println("  Kernel Times:");
//        events.forEach((k, v) -> System.out.println("    " + k + " Time : " + Stream.of(v).mapToLong(CLRunner::getProfileTime).sum() / 1000 / 1000d + "ms"));
////        infos.events().forEach((k, v) -> System.out.println("    " + k + " Time : " + Stream.of(v).mapToLong(CLRunner::getProfileTime).sum() / 1000 / 1000d + "ms"));
//        long printEventsTime = System.nanoTime() - time;


        time = System.nanoTime();
        infos.release();
        queue.release();
        inputs.release();
        internal.release();
        long cleanupTime = System.nanoTime() - time;

//        System.out.println("  Buffers: " + bufferTime / 1000 / 1000d + "ms");
//        System.out.println("  Load Input: " + loadInputTime / 1000 / 1000d + "ms");
//        System.out.println("  General Setup: " + preSetupTime / 1000 / 1000d + "ms");
//        System.out.println("  Kernel Setup: " + setupTime / 1000 / 1000d + "ms");
//        System.out.println("  Execute Kernel: " + executeTime / 1000 / 1000d + "ms");
//        System.out.println("  Print Events: " + printEventsTime / 1000 / 1000d + "ms");
//        System.out.println("  Cleanup: " + cleanupTime / 1000 / 1000d + "ms");
    }

    public CLBuffer<DoubleBuffer> createOutputBuffer(int count) {
        int totalSize = Stream.of(layers).mapToInt(TrainableOCLNeuronLayer::size).map(s -> s * (s + 1)).sum();
        return CLRunner.instance.createBuffer(count * totalSize, CLMemory.Mem.READ_ONLY/*, CLMemory.Mem.ALLOCATE_BUFFER*/);
    }

    public FullNetOutput<TrainableOCLLayerOutput>[] asTrainableOutput(CLBuffer<DoubleBuffer> netOutputs, int count) {
        CLCommandQueue queue = CLRunner.instance.createQueue();
        queue.putReadBuffer(netOutputs, true);
        queue.release();

        FullNetOutput<TrainableOCLLayerOutput>[] netOuts = new FullNetOutput[count];
        for (int i = 0; i < count; i++) {
            TrainableOCLLayerOutput[] ls = new TrainableOCLLayerOutput[height()];
            int layerOffset = 0;
            for (int l = 0; l < height(); l++) {
                int width = layers[l].size();
                ls[l] = new TrainableOCLLayerOutput(new double[width], new double[width * width]);
                netOutputs.getBuffer().get(layerOffset + i * width, ls[l].getOut());
                // TODO: Verify index order consistency between this, kernel code, and java activations. will currently only affect softmax.
                netOutputs.getBuffer().get(layerOffset + count * width + i * width * width, ls[l].getDOut());
                layerOffset += count * width * (width + 1);
            }
            netOuts[i] = new FullNetOutput<>(ls);
        }

        return netOuts;
    }


    public FullNetOutput<LayerOutput>[] asLayerOutput(CLBuffer<DoubleBuffer> netOutputs, int count) {
        int totalSize = Stream.of(layers).mapToInt(TrainableOCLNeuronLayer::size).sum();
        CLBuffer<DoubleBuffer> output = CLRunner.instance.createBuffer(count * totalSize, CLMemory.Mem.READ_ONLY/*, CLMemory.Mem.ALLOCATE_BUFFER*/);
        CLCommandQueue queue = CLRunner.instance.createQueue();
        int layerOffset = 0;
        int layerOffset2 = 0;
        for (int l = 0; l < height(); l++) {
            int width = layers[l].size();
            queue.putCopyBuffer(netOutputs, output, layerOffset * 8, layerOffset2 * 8, (long)count * width * 8, null);
            layerOffset += count * width * (width + 1);
            layerOffset2 += count * width;
        }
        queue.putReadBuffer(output, true);

        FullNetOutput<LayerOutput>[] netOuts = new FullNetOutput[count];
        for (int i = 0; i < count; i++) {
            LayerOutput[] ls = new LayerOutput[height()];
            layerOffset2 = 0;
            for (int l = 0; l < height(); l++) {
                int width = layers[l].size();
                ls[l] = new LayerOutput(new double[width]);
                output.getBuffer().get(layerOffset2 + i * width, ls[l].getOut());
                layerOffset2 += count * width;
            }
            netOuts[i] = new FullNetOutput<>(ls);
        }

        queue.release();
        output.release();

        return netOuts;
    }

    public NetOutput<LayerOutput>[] asOutput(CLBuffer<DoubleBuffer> netOutputs, int count) {
        TrainableOCLNeuronLayer outL = layers[layers.length - 1];
        CLBuffer<DoubleBuffer> output = CLRunner.instance.createBuffer(count * outL.size(), CLMemory.Mem.READ_WRITE/*, CLMemory.Mem.ALLOCATE_BUFFER*/);
        CLCommandQueue queue = CLRunner.instance.createQueue();
        int layerOffset = 0;
        for (int l = 0; l < height() - 1; l++) {
            int width = layers[l].size();
            layerOffset += count * width * (width + 1);
        }
        queue.putCopyBuffer(netOutputs, output, layerOffset * 8, 0, (long)count * outL.size() * 8, null);
        queue.putReadBuffer(output, true);

        NetOutput<LayerOutput>[] netOuts = new NetOutput[count];
        for (int i = 0; i < count; i++) {
            int width = outL.size();
            LayerOutput ls = new LayerOutput(new double[width]);
            output.getBuffer().get(i * width, ls.getOut());
            netOuts[i] = new NetOutput<>(ls);
        }

        queue.release();
        output.release();

        return netOuts;
    }

    synchronized public NetCVals backPropogate(double[][] ins, CLBuffer<DoubleBuffer> netOutputs, double[][] acts, int count) {
        long time = System.nanoTime();
        CLBuffer<DoubleBuffer> inputs = CLRunner.instance.createBuffer(count * ins[0].length, CLMemory.Mem.READ_ONLY/*, CLMemory.Mem.ALLOCATE_BUFFER*/);
        CLBuffer<DoubleBuffer> outputs = CLRunner.instance.createBuffer(count * acts[0].length, CLMemory.Mem.READ_ONLY/*, CLMemory.Mem.ALLOCATE_BUFFER*/);
        int maxSize = Stream.of(layers).mapToInt(TrainableOCLNeuronLayer::size).max().orElse(-1);
        CLBuffer<DoubleBuffer> lastError = CLRunner.instance.createBuffer(count * maxSize, CLMemory.Mem.READ_WRITE/*, CLMemory.Mem.ALLOCATE_BUFFER*/);
        CLBuffer<DoubleBuffer> tempError = CLRunner.instance.createBuffer(count * maxSize, CLMemory.Mem.READ_WRITE/*, CLMemory.Mem.ALLOCATE_BUFFER*/);
        int maxNcvsSize = Stream.of(layers).mapToInt(l -> (l.inSize() + 1) * l.size()).max().orElse(-1);
        CLBuffer<DoubleBuffer> layerNetCvals = CLRunner.instance.createBuffer(count * maxNcvsSize, CLMemory.Mem.READ_WRITE/*, CLMemory.Mem.ALLOCATE_BUFFER*/);
        if (netCvals == null) {
            int totalSize = Stream.of(layers).mapToInt(l -> (l.inSize() + 1) * l.size()).sum();
            netCvals = CLRunner.instance.createBuffer(count * totalSize, CLMemory.Mem.READ_WRITE/*, CLMemory.Mem.ALLOCATE_BUFFER*/);
        }
        long bufferTime = System.nanoTime() - time;

        time = System.nanoTime();
        DoubleBuffer inBuf = inputs.getBuffer();
        for (double[] in : ins) inBuf.put(in);
        inBuf.flip();
        DoubleBuffer outBuf = outputs.getBuffer();
        for (double[] act : acts) outBuf.put(act);
        outBuf.flip();
        long loadInputTime = System.nanoTime() - time;
        
        time = System.nanoTime();
        HashMap<String, CLEventList[]> events = new HashMap<>();
        CLRunner.KernelInfos infos = CLRunner.KernelInfos.empty();
        CVals[] cvals = new CVals[layers.length];

        CLCommandQueue queue = CLRunner.instance.createQueue(CLCommandQueue.Mode.PROFILING_MODE, CLCommandQueue.Mode.OUT_OF_ORDER_MODE);
        events.put("writeInputs", new CLEventList[] {new CLEventList(1)});
        queue.putWriteBuffer(inputs, false, events.get("writeInputs")[0]);
        queue.putBarrier();
        infos = infos.merge(CLRunner.instance.putFillBuffer(queue, netCvals, 0, 0, netCvals.getBuffer().capacity()));
        long preSetupTime = System.nanoTime() - time;

        int[] layerOffsets = new int[height()];
        int layerOffset = 0;
        int[] cvalsLayerOffsets = new int[height()];
        int cvalsLayerOffset = 0;
        for (int i = 1; i < height(); i++) {
            int lWidth = layers[i - 1].size();
            layerOffset += count * lWidth * (lWidth + 1);
            layerOffsets[i] = layerOffset;
            cvalsLayerOffset += lWidth * (layers[i - 1].inSize() + 1);
            cvalsLayerOffsets[i] = cvalsLayerOffset;
        }

        long setupTime = 0;
        int i = layers.length - 1;
        time = System.nanoTime();
        infos = infos.merge(layers[i].setupReverseKernelTop(queue, outputs, lastError, netOutputs, count, layerOffset, cost));
        setupTime += System.nanoTime() - time;
        for (; i >= 0; i--) {
            time = System.nanoTime();
            infos = infos.merge(layers[i].setupReverseKernels(queue, inputs, lastError, tempError, netOutputs, layerNetCvals, netCvals, count, layerOffsets[i], i == 0 ? 0 : layerOffsets[i - 1], cvalsLayerOffsets[i], i));
            setupTime += System.nanoTime() - time;
        }

        time = System.nanoTime();
        netCvals.getBuffer().clear();
        events.put("readNetCvals", new CLEventList[] {new CLEventList(1)});
        queue.putReadBuffer(netCvals, false, events.get("readNetCvals")[0]);
        queue.finish();
        long executeTime = System.nanoTime() - time;
        
        time = System.nanoTime();
        DoubleBuffer buff = netCvals.getBuffer();
        buff.rewind();
        for (i = 0; i < layers.length; i++) {
            int width = layers[i].size();

            double[] cbs = new double[width];
            double[][] cws = new double[width][layers[i].inSize()];
            buff.get(cbs);
            for (int j = 0; j < width; j++) {
                buff.get(cws[j]);
            }
            cvals[i] = new CVals(cws, cbs);
        }
        long readTime = System.nanoTime() - time;
        
        infos = infos.merge(new CLRunner.KernelInfos(new CLKernel[0], events));
        
//        time = System.nanoTime();
//        infos.events().forEach((k, v) -> System.out.println("    " + k + " Time : " + Stream.of(v).mapToLong(CLRunner::getProfileTime).sum() / 1000 / 1000d + "ms"));
////        events.forEach((k, v) -> System.out.println("    " + k + " Time : " + Stream.of(v).mapToLong(CLRunner::getProfileTime).sum() / 1000 / 1000d + "ms"));
//        long printTime = System.nanoTime() - time;
        
        time = System.nanoTime();
        infos.release();
        queue.release();
        inputs.release();
        outputs.release();
        lastError.release();
        tempError.release();
        layerNetCvals.release();
        long cleanupTime = System.nanoTime() - time;
        
//        System.out.println("  Buffers: " + bufferTime / 1000 / 1000d + "ms");
//        System.out.println("  Load Input: " + loadInputTime / 1000 / 1000d + "ms");
//        System.out.println("  General Setup: " + preSetupTime / 1000 / 1000d + "ms");
//        System.out.println("  Kernel Setup: " + setupTime / 1000 / 1000d + "ms");
//        System.out.println("  Execute Kernel: " + executeTime / 1000 / 1000d + "ms");
//        System.out.println("  Read Buffer: " + readTime / 1000 / 1000d + "ms");
//        System.out.println("  Print Events: " + printTime / 1000 / 1000d + "ms");
//        System.out.println("  Cleanup: " + cleanupTime / 1000 / 1000d + "ms");

        return new NetCVals(cvals);
    }

    public double[][] getStats(double[][] outputs, CLBuffer<DoubleBuffer> netOutputs, int count) {
        CLBuffer<DoubleBuffer> outs = CLRunner.instance.createBuffer(count * outputs[0].length, CLMemory.Mem.READ_ONLY);
        CLBuffer<DoubleBuffer> results = CLRunner.instance.createBuffer(count * 2, CLMemory.Mem.READ_WRITE);
        CLBuffer<DoubleBuffer> copyOut = CLRunner.instance.createBuffer(2 * (height() + 1), CLMemory.Mem.WRITE_ONLY);

        DoubleBuffer outBuf = outs.getBuffer();
        for (double[] out : outputs) outBuf.put(out);
        outBuf.flip();

        HashMap<String, CLEventList[]> copyEvents = new HashMap<>();
        copyEvents.put("layerCopy1", new CLEventList[] {new CLEventList(height())});
        copyEvents.put("layerCopy2", new CLEventList[] {new CLEventList(height())});
        CLCommandQueue queue = CLRunner.instance.createQueue(CLCommandQueue.Mode.PROFILING_MODE);
        queue.putWriteBuffer(outs, false);
        CLRunner.KernelInfos infos = CLRunner.KernelInfos.empty();
        int layerOffset = 0;
        for (int i = 0; i < height(); i++) {
            infos = infos.merge(layers[i].setupLayerStatsKernel(queue, netOutputs, results, layerOffset, i, count));
            infos = infos.merge(CLRunner.instance.setupBinarySummationKernel(queue, results, 0, count));
            queue.putCopyBuffer(results, copyOut, 0, i * 8 * 2, 8, copyEvents.get("layerCopy1")[0]);
            infos = infos.merge(CLRunner.instance.setupBinarySummationKernel(queue, results, count, count));
            queue.putCopyBuffer(results, copyOut, count * 8, i * 8 * 2 + 8, 8, copyEvents.get("layerCopy2")[0]);

            int width = layers[i].size();
            if (i < height() - 1) layerOffset += count * width * (width + 1);
        }
        copyEvents.put("copy1", new CLEventList[] {new CLEventList(1)});
        copyEvents.put("copy2", new CLEventList[] {new CLEventList(1)});
        infos = infos.merge(setupNetStatsKernel(queue, outs, netOutputs, results, outputs[0].length, layerOffset, count));
        infos = infos.merge(CLRunner.instance.setupBinarySummationKernel(queue, results, 0, count));
        queue.putCopyBuffer(results, copyOut, 0, height() * 8 * 2, 8, copyEvents.get("copy1")[0]);
        infos = infos.merge(CLRunner.instance.setupBinarySummationKernel(queue, results, count, count));
        queue.putCopyBuffer(results, copyOut, count * 8, height() * 8 * 2 + 8, 8, copyEvents.get("copy2")[0]);

        queue.putReadBuffer(copyOut, false);
        queue.finish();

        DoubleBuffer copyBuf = copyOut.getBuffer();
        copyBuf.rewind();
        double[][] stats = new double[height() + 1][];
        for (int i = 0; i < height() + 1; i++) {
            stats[i] = new double[2];
            copyBuf.get(stats[i]);
        }
        
        infos = infos.merge(new CLRunner.KernelInfos(new CLKernel[0], copyEvents));

//        infos.events().forEach((k, v) -> System.out.println("    " + k + " Time : " + Stream.of(v).mapToLong(CLRunner::getProfileTime).sum() / 1000 / 1000d + "ms"));
////        copyEvents.forEach((k, v) -> System.out.println("    " + k + " Time : " + Stream.of(v).mapToLong(CLRunner::getProfileTime).sum() / 1000 / 1000d + "ms"));
        
        infos.release();
        queue.release();
        outs.release();
        results.release();
        copyOut.release();

        return stats;
    }

    public CLRunner.KernelInfos setupNetStatsKernel(CLCommandQueue queue, CLBuffer<DoubleBuffer> outs, CLBuffer<DoubleBuffer> netOutputs, CLBuffer<DoubleBuffer> result, int size, int layerOffset, int count) {
        CLKernel kernel = CLRunner.instance.createKernel("calculateStats");
        kernel.setArgs(outs, netOutputs, result, size, layerOffset, height(), count);

        HashMap<String, CLEventList[]> events = new HashMap<>();
        queue.putWriteBuffer(outs, false);
        events.put("stats", CLRunner.instance.enqueueMultiKernel(kernel, queue, count));

        return new CLRunner.KernelInfos(new CLKernel[] {kernel}, events);
    }
    
    public void correct(NetCVals cvals, NetCVals lastCvals, HyperParameters params) {
        for (int i = 0; i < layers.length; i++) {
            layers[i].correct(cvals.getCVal(i).cWeights, lastCvals.getCVal(i).cWeights, cvals.getCVal(i).cBiases, lastCvals.getCVal(i).cBiases, params);
        }
    }
    
    public TrainableOCLNeuronLayer[] getLayers() {
        return layers;
    }
    
    public int height() {
        return layers.length;
    }
	
//	public double[][][][] toArray() {
//		double[][][][] net = new double[layers.length][][][];
//        for (int l = 0; l < layers.length; l++) {
//            TrainableNeuron[] neurons = layers[l].getNeurons();
//			net[l] = new double[neurons.length][][];
//			for (int n = 0; n < neurons.length; n++) {
//				double[] weights = neurons[n].getWeights();
//				net[l][n] = new double[2][];
//				net[l][n][0] = new double[weights.length];
//				System.arraycopy(weights, 0, net[l][n][0], 0, weights.length);
//				net[l][n][1] = new double[1];
//				net[l][n][1][0] = neurons[n].getBias();
//			}
//		}
//		return net;
//	}
//
	public static TrainableOCLNeuralNet fromArray(double[][][][] net, ActivationFunction[] acts, CostFunction cost) {
		TrainableOCLNeuronLayer[] layers = new TrainableOCLNeuronLayer[net.length];
        for (int l = 0; l < net.length; l++) {
            int nSize = net[l].length;
            int wSize = net[l][0][0].length;
            double[] weights = new double[nSize * wSize];
            double[] biases = new double[nSize];
			for (int n = 0; n < nSize; n++) {
                for (int i = 0; i < wSize; i++) {
                    weights[n * wSize + i] = net[l][n][0][i];
                }
                biases[n] = net[l][n][1][0];
			}
			layers[l] = new TrainableOCLNeuronLayer(nSize, weights, biases, acts[l]);
		}
		return new TrainableOCLNeuralNet(layers, cost);
	}

    private void writeObject(ObjectOutputStream out) throws IOException {
        out.writeInt(layers.length);
        for (TrainableOCLNeuronLayer layer : layers) {
            out.writeInt(layer.size());
            out.writeObject(layer.getWeights());
            out.writeObject(layer.getBiases());
            out.writeObject(layer.getActivation());
        }
        out.writeObject(cost);
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        int depth = in.readInt();
        layers = new TrainableOCLNeuronLayer[depth];
        for (int i = 0; i < depth; i++) {
            layers[i] = new TrainableOCLNeuronLayer(in.readInt(), (double[])in.readObject(), (double[])in.readObject(), (ActivationFunction)in.readObject());
        }
        cost = (CostFunction)in.readObject();
    }

    @Override
    protected void finalize() throws Throwable {
        netCvals.release();
    }
}
