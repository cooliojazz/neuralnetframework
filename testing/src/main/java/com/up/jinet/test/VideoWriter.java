package com.up.jinet.test;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.io.*;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.IntBuffer;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;
import java.util.ArrayList;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class VideoWriter {

    public static void main(String[] args) throws IOException {
        ArrayList<BufferedImage> frames = new ArrayList<>();
        File dir = new File("C:\\Users\\Ricky\\Projects\\neuralnetframework\\runs\\ocl-mandlebrot-og-inited-auto-pid7\\output");
        for (File f : dir.listFiles((f, name) -> name.matches("output-(\\d+).png"))) {
            frames.add(ImageIO.read(f));
        }

        writeVideo(new FileOutputStream("test.y4m"), frames.toArray(BufferedImage[]::new));
    }

    public static void writeVideo(OutputStream os, BufferedImage[] frames) throws IOException {
        int[][] pixels = Stream.of(frames)
                               .map(i -> {
                                   BufferedImage tmp = new BufferedImage(frames[0].getWidth(), frames[0].getHeight(), BufferedImage.TYPE_INT_ARGB);
                                   tmp.createGraphics().drawImage(i, 0, 0, null);
                                   return tmp;
                               })
                               .map(BufferedImage::getRaster)
                               .map(r -> (DataBufferInt)r.getDataBuffer())
                               .map(DataBufferInt::getData)
                               .toArray(int[][]::new);
        byte[] data = assembleYUV4MPEG(frames[0].getWidth(), frames[0].getHeight(), 5, pixels);
        os.write(data);
        os.close();
    }

    private static byte[] assembleYUV4MPEG(int width, int height, int fps, int[][] frames) throws IOException {
        ByteArrayOutputStream video = new ByteArrayOutputStream();

        PrintStream header = new PrintStream(video);
        header.print("YUV4MPEG2");
        header.print(" W" + width);
        header.print(" H" + height);
        header.print(" F" + fps + ":1");
        header.print(" C444");
        header.print("\n");

//        DataOutputStream pixels = new DataOutputStream(video);
//        YCbCrOutputStream pixels = new YCbCrOutputStream(video);
        for (int f = 0; f < frames.length; f++) {
            header.print("FRAME\n");
//            header.flush();
//            IntStream.of(frames[f]).forEach(i -> {try{ pixels.writeInt(i); }catch(Exception e){throw new RuntimeException(e);}});
//            pixels.write(frames[f]);
//            pixels.flush();
            write(video, frames[f]);
        }
        video.flush();

        return video.toByteArray();
    }

    private static int argbToYCbCra(int argb) {
        int a = (argb >> 24) & 0xFF;
        int r = (argb >> 16) & 0xFF;
        int g = (argb >> 8) & 0xFF;
        int b = argb & 0xFF;
        return (((int)(16 + (65.738 * r + 129.057 * g + 25.064 * b) / 256) & 0xFF) << 24) |
               (((int)(128 + (-37.945 * r - 74.494 * g + 112.439 * b) / 256) & 0xFF) << 16) |
               (((int)(128 + (112.439 * r - 94.154 * g - 18.285 * b) / 256) & 0xFF) << 8) |
               a;
    }

    private static void write(OutputStream os, int[] rgbs) throws IOException {
        for (int i : rgbs) {
            i = argbToYCbCra(i);
            os.write(i >> 24);
        }
        for (int i : rgbs) {
            i = argbToYCbCra(i);
            os.write(i >> 16);
        }
        for (int i : rgbs) {
            i = argbToYCbCra(i);
            os.write(i >> 8);
        }
//            for (int i : rgbs) {
//                i = argbToYCbCra(i);
//                write(i);
//            }
    }

//    private static final class YCbCrOutputStream extends BufferedOutputStream {
//
//        public YCbCrOutputStream(OutputStream out) {
//            super(out);
//        }
//
//        public void write(int[] rgbs) throws IOException {
//            for (int i : rgbs) {
//                i = argbToYCbCra(i);
//                write(i >> 24);
//            }
//            for (int i : rgbs) {
//                i = argbToYCbCra(i);
//                write(i >> 16);
//            }
//            for (int i : rgbs) {
//                i = argbToYCbCra(i);
//                write(i >> 8);
//            }
////            for (int i : rgbs) {
////                i = argbToYCbCra(i);
////                write(i);
////            }
//        }
//    }
}
