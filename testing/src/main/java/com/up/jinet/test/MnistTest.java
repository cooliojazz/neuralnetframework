package com.up.jinet.test;

import com.up.jinet.training.network.HyperParameters;
import com.up.jinet.training.network.TrainableNeuralNet;
import com.up.jinet.training.gui.ImageDisplay;
import com.up.jinet.training.NetDataSet;
import com.up.jinet.training.DefaultNetTrainer;
import com.up.jinet.function.activation.ActivationFunction;
import com.up.jinet.function.cost.CrossEntropyCost;
import com.up.jinet.function.activation.LinearActivation;
import com.up.jinet.function.activation.SigmoidActivation;
import com.up.jinet.function.activation.SoftmaxActivation;
import com.up.jinet.training.gui.NetVisualiser;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 *
 * @author Ricky
 */


public class MnistTest {
    
    static final int IMAGE_COUNT = 60000;
    static final int TEST_COUNT = 10000;
    static final int IMAGE_VARIATIONS = 1;
    static final int SAMPLE_COUNT = IMAGE_COUNT * IMAGE_VARIATIONS;
    static double TRAIN_RATIO = 1;
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
//		Graph g = new Graph();
		// TODO: Abstract the meat of this to a NetTrainer class to allow easily reusing the training code
        try {
            System.out.println("Loading image files...");
            double[][] images = getImages(new File("train-images.idx3-ubyte"));
            int[] labels = getLabels(new File("train-labels.idx1-ubyte"));
			
//            NeuralNet net = new NeuralNet(784, 10, 200, 3, new SigmoidActivation(), new MseCost());
//            NeuralNet net = new NeuralNet(784, 10, new SigmoidActivation(), new MseCost());
//            NeuralNet net = new NeuralNet(784, 10, 100, 2, new SigmoidActivation(), new MseCost());
//            NeuralNet net = new NeuralNet(784, 10, new int[] {50, 20}, 3, new ActivationFunction[] {new SigmoidActivation(), new TanhActivation(), new SoftmaxActivation()}, new MseCost());
//            NeuralNet net = new NeuralNet(784, 10, 10, 2, new ActivationFunction[] {new SigmoidActivation(), new SigmoidActivation()}, new MseCost());
//            NeuralNet net = new NeuralNet(784, 10, 100, 2, new ActivationFunction[] {new SigmoidActivation(), new LeakyReluActivation(0.01)}, new CostFunction[] {new MseCost(), new MseCost()});
//            NeuralNet net = new NeuralNet(784, 10, 50, 2, new ActivationFunction[] {new SigmoidActivation(), new SigmoidActivation()}, new CostFunction[] {new MseCost(), new MseCost()});
//            NeuralNet net = new NeuralNet(784, 10, 10, 2, new ActivationFunction[] {new ReluActivation(), new SoftmaxActivation()}, new CrossEntropyCost());
//            NeuralNet net = new NeuralNet(784, 10, 32, 2, new ActivationFunction[] {new ReluActivation(), new SoftmaxActivation()}, new MseCost());
//            NeuralNet net = new NeuralNet(784, 10, 10, 2, new ReluActivation(), new MseCost());
//            NeuralNet net = new NeuralNet(784, 10, 32, 2, new SigmoidActivation(), new CrossEntropyCost());
			TrainableNeuralNet net = new TrainableNeuralNet(784, 10, new int[] {20, 10}, new ActivationFunction[] {new SigmoidActivation(), new SigmoidActivation(), new SoftmaxActivation()}, new CrossEntropyCost());
//            NeuralNet net = NetworkLoadr.load(new File("last-minst.nwa"));


			ImageDisplay id = new ImageDisplay(labels, images, net, null);
			id.setVisible(true);
			Frame f = new Frame();
			NetVisualiser nv = new NetVisualiser(net, false);
			f.add(nv, BorderLayout.CENTER);
			nv.setSize(600, 1000);
			f.pack();
			f.setVisible(true);
			
//			DefaultNetTrainer trainer = new DefaultNetTrainer(SAMPLE_COUNT - TEST_COUNT, 150, 25, new HyperParameters(200, 20, TRAIN_RATIO), true, new File("run"), (t, s) -> {
//					nv.repaint();
//					id.updateThoughts();
//					net.getLayers();
////					net.getLayers()[1].setAct(new LinearActivation());
////					try {
////						NetLoader.save(net, new File("last-minst.nwa"));
////					} catch (IOException ex) {
////						ex.printStackTrace();
////					}
//				});
//			NetDataSet training = new NetDataSet(Stream.of(images).limit(SAMPLE_COUNT - TEST_COUNT).toArray(double[][]::new),
//					IntStream.of(labels).limit(SAMPLE_COUNT - TEST_COUNT).mapToObj(i -> {double[] d = new double[10]; d[i] = 1; return d;}).toArray(double[][]::new));
//			NetDataSet testing = new NetDataSet(Stream.of(images).skip(SAMPLE_COUNT - TEST_COUNT).toArray(double[][]::new),
//					IntStream.of(labels).skip(SAMPLE_COUNT - TEST_COUNT).mapToObj(i -> {double[] d = new double[10]; d[i] = 1; return d;}).toArray(double[][]::new));
//			trainer.train(net, training, testing, true, true, false);
			
			net.getLayers()[1].setActivation(new LinearActivation());
			
			
			throw new ClassNotFoundException();
        } catch (IOException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
    }
	
    public static BufferedImage convertToImage(double[] image) {
		BufferedImage i = new BufferedImage(28, 28, BufferedImage.TYPE_INT_RGB);
		for (int x = 0; x < 28; x++) {
			for (int y = 0; y < 28; y++) {
				i.setRGB(x, y, ((int)(image[x + y * 28] * 255) << 16) | ((int)(image[x + y * 28] * 255) << 8) | (int)(image[x + y * 28] * 255));
			}
		}
		return i;
	}
	
    public static double[][] getImages(File f) throws FileNotFoundException, IOException {
        DataInputStream is = new DataInputStream(new FileInputStream(f));
		byte[] header = new byte[4 * 4];
		is.read(header);
        double[][] images = new double[SAMPLE_COUNT][784];
        for (int i = 0; i < IMAGE_COUNT; i++) {
			byte[] image = new byte[784];
			is.read(image);
            for (int j = 0; j < 784; j++) {
                images[i][j] = (image[j] & 255) / 255d;
//                images[i][j] = (image[j] & 255) > 127 ? 1 : 0;
            }
        }
        for (int i = 1; i < IMAGE_VARIATIONS; i++) {
			for (int j = 0; j < IMAGE_COUNT; j++) {
				images[i * IMAGE_COUNT + j] = noiseVariation(transformVariation(images[j], 5, Math.PI / 5, 0.33), (double)i / IMAGE_VARIATIONS / 2);
			}
		}
        return images;
    }
    
    public static int[] getLabels(File f) throws FileNotFoundException, IOException {
        DataInputStream is = new DataInputStream(new FileInputStream(f));
		byte[] header = new byte[2 * 4];
		is.read(header);
        int[] labels = new int[SAMPLE_COUNT];
        for (int i = 0; i < IMAGE_COUNT; i++) {
			labels[i] = is.read();
			for (int j = 1; j < IMAGE_VARIATIONS; j++) {
				labels[j * IMAGE_COUNT + i] = labels[i];
			}
        }
        return labels;
    }
    
    public static int getOutput(double[] out) {
        double max = 0;
        int res = 0;
        for (int i = 0; i < out.length; i++) {
            if (out[i] > max) {
                max = out[i];
                res = i;
            }
        }
        return res;
    }
	
	private static double[] noiseVariation(double[] image, double noiseFactor) {
		double[] noisy = new double[image.length];
		for (int i = 0; i < image.length; i++) {
			noisy[i] = Math.max(0, Math.min(1, image[i] + (Math.random() * 2 - 1) * noiseFactor));
		}
		return noisy;
	}
	
	private static double[] transformVariation(double[] image, double maxOffset, double angleRange, double scaleRange) {
		double[] transformed = new double[image.length];
		double angle = (Math.random() * 2 - 1) * angleRange;
		double scale = 1 + (Math.random() * 2 - 1) * scaleRange;
		int offx = (int)((Math.random() * 2 - 1) * maxOffset);
		int offy = (int)((Math.random() * 2 - 1) * maxOffset);
		int size = (int)Math.round(28 * scale);
		
		AffineTransform transform = AffineTransform.getTranslateInstance(offx, offy);
		transform.scale(scale, scale);
		transform.rotate(angle, 13.5, 13.5);
		AffineTransformOp op = new AffineTransformOp(transform, AffineTransformOp.TYPE_BILINEAR);
		BufferedImage img = op.filter(convertToImage(image), new BufferedImage(size, size, BufferedImage.TYPE_INT_RGB));
		
		for (int x = 0; x < 28; x++) {
			for (int y = 0; y < 28; y++) {
				int ox = (size - 28) / 2 + x;
				int oy = (size - 28) / 2 + y;
				if (ox >= 0 && ox < img.getWidth() && oy >= 0 && oy < img.getHeight()) transformed[y * 28 + x] = (img.getRGB(ox, oy) & 0x000000FF) / 256d;
			}
		}
		return transformed;
	}
}
