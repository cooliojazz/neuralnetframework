package com.up.jinet.test;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *
 * @author Ricky
 */


public class DeNoising {
    
    static final int BATCH_SIZE = 10000;
    static final int ITER_LEN = 1000;
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        // Needs updating to new systems
//        try {
//            System.out.println("Loading image files...");
//            double[][] images = getImages(new File("train-images.idx3-ubyte"));
//            int[] labels = getLabels(new File("train-labels.idx1-ubyte"));
//            NeuralNet net = new NeuralNet(784, 10, 50, 1);
//            System.out.println("Beginning network training");
//            double[] pers = new double[ITER_LEN];
//            for (int i = 0; i < ITER_LEN; i++) {
//                CVals[][] cvals = new CVals[BATCH_SIZE][];
//                int correct = 0;
//                for (int j = 0; j < BATCH_SIZE; j++) {
//                    int img = (int)(Math.random() * images.length);
//                    double[] out = net.get(images[img]);
//                    if (labels[img] == getOutput(out)) correct++;
////                    System.out.println("Actual: " + labels[img] + " NN: " + getOutput(out));
//                    double[] act = new double[10];
//                    act[labels[img]] = 1;
//                    cvals[j] = net.backPropogate(act);
//                }
//                pers[i] = correct / (double)BATCH_SIZE;
//                System.out.println("Batch " + i + ": " + correct + "/" + BATCH_SIZE);
//                for (int j = 0; j < BATCH_SIZE; j++) {
//                    net.correct(cvals[j], BATCH_SIZE);
//                }
//            }
//            for (int i = 0; i < ITER_LEN; i++) {
//                System.out.println(i + "," + pers[i]);
//            }
//        } catch (IOException ex) {
//            ex.printStackTrace();
//        }
    }
    
    public static double[][] getImages(File f) throws FileNotFoundException, IOException {
        DataInputStream is = new DataInputStream(new FileInputStream(f));
        is.readInt();
        is.readInt();
        is.readInt();
        is.readInt();
        double[][] images = new double[BATCH_SIZE][784];
        for (int i = 0; i < BATCH_SIZE; i++) {
            for (int j = 0; j < 784; j++) {
                images[i][j] = is.read() / 255d;
            }
        }
        return images;
    }
    
    public static int[] getLabels(File f) throws FileNotFoundException, IOException {
        DataInputStream is = new DataInputStream(new FileInputStream(f));
        is.readInt();
        is.readInt();
        int[] labels = new int[BATCH_SIZE];
        for (int i = 0; i < BATCH_SIZE; i++) {
            labels[i] = is.read();
        }
        return labels;
    }
    
    public static int getOutput(double[] out) {
        double max = 0;
        int res = 0;
        for (int i = 0; i < out.length; i++) {
            if (out[i] > max) {
                max = out[i];
                res = i;
            }
        }
        return res;
    }
}
