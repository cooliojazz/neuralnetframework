package com.up.jinet.test;

import com.jogamp.opencl.CLBuffer;
import com.up.jinet.NeuralNet;
import com.up.jinet.function.activation.*;
import com.up.jinet.function.cost.MseCost;
import com.up.jinet.training.OCLNetTrainer;
import com.up.jinet.training.network.TrainableOCLNeuralNet;
import com.up.jinet.training.DefaultNetTrainer;
import com.up.jinet.training.NetTrainer;
import com.up.jinet.training.network.HyperParameters;
import com.up.jinet.training.NetDataSet;
import com.up.jinet.training.network.TrainableNeuralNet;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.nio.DoubleBuffer;
import java.util.stream.Stream;

/**
 *
 * @author Ricky
 */


public class SimpleTest {
    
    static final int SAMPLE_COUNT = 5000;
	
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, ClassNotFoundException {
		double[][] trainingInput = new double[SAMPLE_COUNT][2];
//		int[] trainingLabels = new int[SAMPLE_COUNT];
		double[][] trainingLabels = new double[SAMPLE_COUNT][3];
		for (int i = 0; i < SAMPLE_COUNT; i++) {
			double x = Math.random();
			double y = Math.random();
			trainingInput[i][0] = x;
			trainingInput[i][1] = y;
//			trainingLabels[i][0] = x + y > 1 ? 1 : 0;
//			trainingLabels[i][0] = x * y > 0.4 ? 1 : 0;
//			trainingLabels[i][0] = x * y > 0.15 ? 1 : 0;
//			trainingLabels[i][0] = Math.pow(x, 2) + Math.pow(y, 2) < 0.5 ? 1 : 0;
//			trainingLabels[i][0] = Math.pow(x - 0.5, 2) + Math.pow(y - 0.5, 2) < 0.4 * 0.4 ? 1 : 0;
//			trainingLabels[i] = Math.pow(x - 0.5, 2) + Math.pow(y - 0.5, 2) < 0.4 * 0.4 && Math.pow(x - 0.5, 2) + Math.pow(y - 0.5, 2) > 0.2 * 0.2 ? 1 : 0;
//			trainingLabels[i][0] = mandlebrotTest(x * 2.5 - 2, y * 2 - 1);
			
			boolean c1 = Math.sqrt(Math.pow(x - 0.8, 2) + Math.pow(y - 0.5, 2)) < 0.4;
			boolean c2 = Math.sqrt(Math.pow(x - 0.2, 2) + Math.pow(y - 0.5, 2)) < 0.4;
//			trainingLabels[i][0] = c1 || c2 ? 0 : 1;
//			trainingLabels[i][1] = c1 ? 1d / (c2 ? 2 : 1) : 0;
//			trainingLabels[i][2] = c2 ? 1d / (c1 ? 2 : 1) : 0;
//			trainingLabels[i][0] = c1 ^ c2 ? 0 : 1;
//			trainingLabels[i][1] = c1 && !c2 ? 1 : 0;
//			trainingLabels[i][2] = c2 && !c1 ? 1 : 0;
			trainingLabels[i][0] = c1 || c2 ? 0 : 1;
			trainingLabels[i][1] = c1 ? 1 : 0;
			trainingLabels[i][2] = c2 && !c1 ? 1 : 0;
		}
		NetDataSet training = new NetDataSet(trainingInput, trainingLabels);
		double[][] testingInput = new double[1000][2];
//		int[] testingLabels = new int[SAMPLE_COUNT];
		double[][] testingLabels = new double[1000][3];
		for (int i = 0; i < 1000; i++) {
			double x = Math.random();
			double y = Math.random();
			testingInput[i][0] = x;
			testingInput[i][1] = y;
//			testingLabels[i][0] = x + y > 1 ? 1 : 0;
//			testingLabels[i][0] = x * y > 0.4 ? 1 : 0;
//			testingLabels[i][0] = x * y > 0.15 ? 1 : 0;
//			testingLabels[i][0] = Math.pow(x, 2) + Math.pow(y, 2) < 0.5 ? 1 : 0;
//			testingLabels[i][0] = Math.pow(x - 0.5, 2) + Math.pow(y - 0.5, 2) < 0.4 * 0.4 ? 1 : 0;
//			testingLabels[i] = Math.pow(x - 0.5, 2) + Math.pow(y - 0.5, 2) < 0.4 * 0.4 && Math.pow(x - 0.5, 2) + Math.pow(y - 0.5, 2) > 0.2 * 0.2 ? 1 : 0;
//			testingLabels[i][0] = mandlebrotTest(x * 2.5 - 2, y * 2 - 1);
			
			boolean c1 = Math.sqrt(Math.pow(x - 0.8, 2) + Math.pow(y - 0.5, 2)) < 0.4;
			boolean c2 = Math.sqrt(Math.pow(x - 0.2, 2) + Math.pow(y - 0.5, 2)) < 0.4;
//			testingLabels[i][0] = c1 || c2 ? 0 : 1;
//			testingLabels[i][1] = c1 ? 1d / (c2 ? 2 : 1) : 0;
//			testingLabels[i][2] = c2 ? 1d / (c1 ? 2 : 1) : 0;
//			testingLabels[i][0] = c1 ^ c2 ? 0 : 1;
//			testingLabels[i][1] = c1 && !c2 ? 1 : 0;
//			testingLabels[i][2] = c2 && !c1 ? 1 : 0;
			testingLabels[i][0] = c1 || c2 ? 0 : 1;
			testingLabels[i][1] = c1 ? 1 : 0;
			testingLabels[i][2] = c2 && !c1 ? 1 : 0;
		}
		NetDataSet testing = new NetDataSet(testingInput, testingLabels);
		
//		NeuralNet net = new NeuralNet(2, 1, new SigmoidActivation(), new MseCost());
//		NeuralNet net = new NeuralNet(2, 1, 10, 2, new SigmoidActivation(), new MseCost());
//		NeuralNet net = new NeuralNet(2, 2, 10, 2, new ActivationFunction[] {new SigmoidActivation(), new SoftmaxActivation()}, new MseCost());
//		NeuralNet net = new NeuralNet(2, 1, new int[] {25, 10, 3}, new ActivationFunction[] {new SigmoidActivation(), new TanhActivation(), new SigmoidActivation(), new LeakyReluActivation(0.01)}, new MseCost());
//		NeuralNet net = new NeuralNet(2, 1, 30, 2, new ActivationFunction[] {new LeakyReluActivation(0.001), new TanhActivation()}, new MseCost());
//		NeuralNet net = new NeuralNet(2, 1, 50, 2, new ActivationFunction[] {new TanhActivation(), new SigmoidActivation()}, new MseCost());
//		TrainableNeuralNet net = new TrainableNeuralNet(2, 1, new int[] {32, 8}, new ActivationFunction[] {new TanhActivation(), new TanhActivation(), new SigmoidActivation()}, new MseCost());
//		TrainableNeuralNet net = new TrainableNeuralNet(2, 1, new int[] {24, 12, 6}, new ActivationFunction[] {new TanhActivation(), new TanhActivation(), new SigmoidActivation(), new LeakyReluActivation(0.001)}, new MseCost());
//		TrainableNeuralNet net = new TrainableNeuralNet(2, 1, new int[] {24, 12}, new ActivationFunction[] {new ReluActivation(), new ReluActivation(), new ReluActivation()}, new MseCost());
//		TrainableNeuralNet net = new TrainableNeuralNet(2, 1, new int[] {48}, new ActivationFunction[] {new ReluActivation(), new SigmoidActivation()}, new MseCost());
//		TrainableNeuralNet net = new TrainableNeuralNet(2, 1, new int[] {24, 16, 8, 3}, new ActivationFunction[] {new ReluActivation(), new ReluActivation(), new ReluActivation(), new ReluActivation(), new ReluActivation()}, new MseCost());
//		TrainableNeuralNet net = new TrainableNeuralNet(2, 1, new int[] {12, 8, 4}, new ActivationFunction[] {new LeakyReluActivation(0.01), new LeakyReluActivation(0.01), new LeakyReluActivation(0.01), new LeakyReluActivation(0.01)}, new MseCost());
//		TrainableNeuralNet net = new TrainableNeuralNet(2, 1, new int[] {24, 16, 8, 3}, new ActivationFunction[] {new SwishActivation(1), new SwishActivation(1), new SwishActivation(1), new SwishActivation(1), new SwishActivation(1)}, new MseCost());
//		net = new TrainableNeuralNet(2, 1, new int[] {24, 16, 8, 3}, new ActivationFunction[] {new ReluActivation(), new ReluActivation(), new ReluActivation(), new ReluActivation(), new SwishActivation(0.5)}, new MseCost());
//		net = new TrainableNeuralNet(2, 1, new int[] {24, 16, 8, 3}, new ActivationFunction[] {new SwishActivation(1.5), new SwishActivation(1.625), new SwishActivation(1.75), new SwishActivation(1.875), new SwishActivation(2)}, new MseCost());
//		TrainableNeuralNet net = NetLoader.load(new File("runs/mandlebrot-best-swish", "current-net.nwa"));
//		TrainableNeuralNet tNet = NetLoader.load(new File("runs/mandlebrot-best-swish", "current-net.nwa"));
//		net = new TrainableNeuralNet(2, 1, new int[] {3}, new ActivationFunction[] {new SwishActivation(1.5), new SwishActivation(2)}, new MseCost());
//		TrainableOCLNeuralNet net = TrainableOCLNeuralNet.fromArray(tNet.toArray(), Stream.of(tNet.getLayers()).map(NeuronLayer::getActivation).toArray(ActivationFunction[]::new), tNet.getCost());
//		TrainableOCLNeuralNet net = new TrainableOCLNeuralNet(2, 1, new int[] {13, 8, 5, 3}, new ActivationFunction[] {new SwishActivation(1.5), new SwishActivation(1.625), new SwishActivation(1.75), new SwishActivation(1.875), new SwishActivation(2)}, new MseCost());
//		TrainableOCLNeuralNet net = new TrainableOCLNeuralNet(2, 1, new int[] {24, 16, 8, 3}, new ActivationFunction[] {new SwishActivation(1.5), new SwishActivation(1.625), new SwishActivation(1.75), new SwishActivation(1.875), new SwishActivation(2)}, new MseCost());
//		TrainableOCLNeuralNet net = new TrainableOCLNeuralNet(2, 1, new int[] {50, 30, 15, 5}, new ActivationFunction[] {new SwishActivation(1.5), new SwishActivation(1.625), new SwishActivation(1.75), new SwishActivation(1.875), new SwishActivation(2)}, new MseCost());
//		TrainableOCLNeuralNet net = new TrainableOCLNeuralNet(2, 1, 5, 2, new SwishActivation(2), new MseCost());
//		TrainableOCLNeuralNet net = new TrainableOCLNeuralNet(2, 3, 6, 2, new ActivationFunction[] {new SwishActivation(2), new SoftmaxActivation()}, new CrossEntropyCost());
//		TrainableOCLNeuralNet net = new TrainableOCLNeuralNet(2, 3, 10, 2, new ActivationFunction[] {new SwishActivation(1.5), new SwishActivation(2.5)}, new MseCost());
		TrainableOCLNeuralNet net = new TrainableOCLNeuralNet(2, 3, 10, 2, new SwishActivation(2), new MseCost());
		TrainableNeuralNet nrealNet = new TrainableNeuralNet(2, 3, 10, 2, new SwishActivation(2), new MseCost());
//		TrainableNeuralNet nrealNet = new TrainableNeuralNet(2, 3, 10, 2, new SoftmaxActivation(), new CrossEntropyCost());
//		TrainableOCLNeuralNet net = NetLoader.load(new File("runs/ocl-mandlebrot-og-inited-auto-pid3", "current-net.nwa"));
//		TrainableNeuralNet nrealNet = new TrainableNeuralNet(2, 1, new int[] {24, 16, 8, 3}, new ActivationFunction[] {new SwishActivation(1.5), new SwishActivation(1.625), new SwishActivation(1.75), new SwishActivation(1.875), new SwishActivation(2)}, new MseCost());

//		Frame f = new Frame();
//		Panel p = new Panel();
//		FunctionDrawer c = new FunctionDrawer(net, trainingInput, trainingLabels);
////		FunctionDrawer c = new FunctionDrawer(nrealNet, trainingInput, trainingLabels);
//
//		Checkbox bgCheck = new Checkbox("Background");
//		bgCheck.addItemListener(e -> {c.background = e.getStateChange() == ItemEvent.SELECTED; c.repaint();});
//		p.add(bgCheck);
//		Checkbox pCheck = new Checkbox("Predictions");
//		pCheck.addItemListener(e -> {c.predictions = e.getStateChange() == ItemEvent.SELECTED; c.repaint();});
//		p.add(pCheck);
//		f.add(p, BorderLayout.NORTH);
//
//		c.setSize(500, 400);
//		f.add(c, BorderLayout.CENTER);
//		
//		NetVisualiser nv = new NetVisualiser(net, true);
////		NetVisualiser nv = new NetVisualiser(nrealNet, true);
//		f.add(nv, BorderLayout.SOUTH);
//		nv.setSize(500, 400);
//
//		f.pack();
//		f.setVisible(true);
////		new Thread(() -> {
////				while (true) {
////					c.repaint();
////					nv.repaint();
////					try {
////						Thread.sleep(10);
////					} catch (InterruptedException ex) {}
////				}
////			}).start();

//		int[] i = new int[] {0};
//		for (i[0] = 0; i[0] < 10; i[0]++) {
//			File dir = new File("runs/swish-beta-test-mixed-moving/beta-" + i[0] + "/");
//			net = new TrainableNeuralNet(2, 1, new int[] {24, 16, 8, 3}, new ActivationFunction[] {new SwishActivation(1 + i[0] / 10d), new SwishActivation(1.125 + i[0] / 10d), new SwishActivation(1.25 + i[0] / 10d), new SwishActivation(1.375 + i[0] / 10d), new SwishActivation(1.5 + i[0] / 10d)}, new MseCost());
////			net = new TrainableNeuralNet(2, 1, new int[] {24, 16, 8, 3}, new ActivationFunction[] {new SwishActivation(2 - i[0] / 10d), new SwishActivation(1.75 - i[0] / 20d), new SwishActivation(1.5), new SwishActivation(1.25 + i[0] / 20d), new SwishActivation(1 + i[0] / 10d)}, new MseCost());
//			int[] gen = new int[] {0};
//			NetTrainer trainer = new NetTrainer(SAMPLE_COUNT, 100, 10, new HyperParameters(1000, 0.15), false, dir, () -> {
//					c.repaint();
//	//				nv.repaint();
//					try {
//						if (gen[0] % 1000 == 0) ImageIO.write(renderOutput(net, 500), "png", new File(dir, "output-" + gen[0] + ".png"));
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
//					gen[0]++;
//				});
//			trainer.train(net, training, testing);
//		}

		long time = System.nanoTime();
		File dir = new File("runs/another-test");
//		File dir = new File("runs/quick-test");
		File outputDir = new File(dir, "output");
		OCLNetTrainer trainer = new OCLNetTrainer(net, SAMPLE_COUNT, 100, 0, new HyperParameters(2000, 0, 0.01, 0.00), true, dir, (t, s) -> {
//				c.repaint();
//				nv.repaint();
//				try {
//					if (!outputDir.exists()) outputDir.mkdirs();
//					if (t.getBatch() % 1000 == 0) ImageIO.write(renderMegaOutput(net, 500, new Rectangle2D.Double(0, 0, 1, 1)), "png", new File(outputDir, "output-" + t.getBatch() + ".png"));
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
			});
//		OCLNetTrainer trainer = OCLNetTrainer.fromRun(dir, i -> {
//				c.repaint();
//				nv.repaint();
//				try {
//					if (!outputDir.exists()) outputDir.mkdirs();
//					if (i % 2000 == 0) ImageIO.write(renderMegaOutput(net, 1000, new Rectangle2D.Double(0, 0, 1, 1)), "png", new File(outputDir, "output-" + i + ".png"));
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			});
//		File dir = new File("runs/ocl-mandlebrot-og-inited");
		DefaultNetTrainer trainer2 = new DefaultNetTrainer(nrealNet, SAMPLE_COUNT, 100, 0, new HyperParameters(2000, 0, 0.01, 1), true, dir, (t, s) -> {
//					c.repaint();
//					nv.repaint();
//					try {
////						if (t.batch % 1000 == 0) ImageIO.write(renderOutput(net, 500), "png", new File(dir, "output-" + b + ".png"));
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
////					try {Thread.sleep(50);} catch (Exception e) {}
				});
//		NetTrainer trainer2 = NetTrainer.fromRun(new File("runs/mandlebrot-swish"), () -> {
//				c.repaint();
//				nv.repaint();
//			});
//		System.out.println("Setup time: " + (System.nanoTime() - time) / 1000000 / 1000d);
		time = System.nanoTime();
//		boolean[] done = new boolean[] {false, false};
		trainer.train(training, testing, false, false, false);
//		new Thread(() -> {trainer.train(training, testing, false, false, false); done[0] = true;}).start();
//		System.out.println("Total time: " + (System.nanoTime() - time) / 1000000 / 1000d);
//		new Thread(() -> trainer.train(net, training, testing)).start();
//		time = System.nanoTime();
		trainer2.train(training, testing, false, false, false);
//		new Thread(() -> {trainer2.train(training, testing, false, false, false); done[1] = true;}).start();
//		trainer.resumeTrain(net);
//		while (!done[0] && !done[1]) {
//			try {Thread.sleep(10);} catch (Exception e) {}
//		}
		System.out.println("Total time: " + (System.nanoTime() - time) / 1000000 / 1000d);
    }
	
	private static final int escapeLimit = 10000;
	private static double mandlebrotTest(double x, double y) {
		int i = 0;
		Complex c = new Complex(x, y);
		Complex z = new Complex(0, 0);
		while (i < escapeLimit && z.magnitude() < 4) {
			z = z.mult(z).add(c);
			i++;
		}
//		return i >= 10000;
//		return (double)i / escapeLimit;
		return Math.log(i) / Math.log(escapeLimit);
	}

	private static void renderData(Graphics ig, double[][] input, double[][] labels, int width, int height, Rectangle2D.Double bounds, int limit) {
		int rendered = 0;
		for (int i = 0; rendered < limit && i < SAMPLE_COUNT; i++) {
			if (	(input[i][0] + bounds.x) / bounds.width > 0 && (input[i][0] + bounds.x) / bounds.width < 1 &&
					(input[i][1] + bounds.y) / bounds.height > 0 && (input[i][1] + bounds.y) / bounds.height < 1) {
				for (int j = 0; j < labels[i].length; j++) {
					float c = (float)Math.max(0, Math.min(1, labels[i][j]));
					ig.setColor(new Color(0, c, (float)j / labels[i].length, 0.25f));

//				ig.setColor(new Color(0.5f, 0.5f, 0.5f, 0.5f));
//				ig.setColor(new Color((float)rendered / limit, 0.5f, 0.5f, 0.5f));
					ig.fillRect((int)Math.round((input[i][0] + bounds.x) / bounds.width * width - 2.5), (int)Math.round((input[i][1] + bounds.y) / bounds.height * height - 2.5), 5, 5);
//				ig.setColor(new Color(0, c, c, 0.25f));
					ig.fillOval((int)Math.round((input[i][0] + bounds.x) / bounds.width * width - 1.5), (int)Math.round((input[i][1] + bounds.y) / bounds.height * height - 1.5), 3, 3);
					rendered++;
				}
			}
		}
	}

	//TODO: This is ignoring the bounds
	private static BufferedImage renderOutput(NeuralNet net, int size, Rectangle2D.Double bounds) {
		BufferedImage img = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
		int[] pixels = ((DataBufferInt)img.getData().getDataBuffer()).getData();
		long time = System.nanoTime();
		for (int x = 0; x < size; x++) {
			for (int y = 0; y < size; y++) {
//				TrainableNeuralNet copy = TrainableNeuralNet.fromArray(net.toArray(), Stream.of(net.getLayers()).map(TrainableNeuronLayer::getActivation).toArray(ActivationFunction[]::new), net.getCost());
				double[] out = net.get(new double[] {(x + 0.5) / size, (y + 0.5) / size}).getOutput().getOut();
//				img.setRGB(x, y, new Color((float)Math.max(0, Math.min(1, out[0])), 0.15f, 0.15f, 0.7f).getRGB());
//				pixels[y * size + x] = new Color((float)Math.max(0, Math.min(1, out[0])), 0.15f, 0.15f, 0.7f).getRGB();
				pixels[y * size + x] = new Color((float)NetTrainer.getOutput(out) / out.length, 0.15f, 0.15f, 0.7f).getRGB();
			}
		}
		System.out.println("Evaluated in " + (System.nanoTime() - time) / 1000000 + "ms");
		return new BufferedImage(img.getColorModel(),
								 (WritableRaster)Raster.createRaster(img.getRaster().getSampleModel().createCompatibleSampleModel(size, size), new DataBufferInt(pixels, pixels.length), null),
								 false, null);
	}

	private static BufferedImage renderMegaOutput(TrainableOCLNeuralNet net, int size, Rectangle2D.Double bounds) {
		BufferedImage img = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
		int[] pixels = ((DataBufferInt)img.getData().getDataBuffer()).getData();
		double[][] ins = new double[size * size][];
		for (int x = 0; x < size; x++) {
			for (int y = 0; y < size; y++) {
				ins[y * size + x] = new double[] {((double)x / size) * bounds.getWidth() - bounds.getX(), ((double)y / size) * bounds.getHeight() - bounds.getY()};
			}
		}
		long time = System.nanoTime();
//		double[][] outs = Stream.of(net.getMany(ins, ins.length)).map(o -> o.getOutput().getOut()).toArray(double[][]::new);
		CLBuffer<DoubleBuffer> buff = net.createOutputBuffer(ins.length);
		net.getManyFast(ins, ins.length, buff);
		double[][] outs = Stream.of(net.asOutput(buff, ins.length)).map(o -> o.getOutput().getOut()).toArray(double[][]::new);
		buff.release();
		System.out.println("Evaluated in " + (System.nanoTime() - time) / 1000000 + "ms");
		for (int x = 0; x < size; x++) {
			for (int y = 0; y < size; y++) {
//				pixels[y * size + x] = new Color((float)Math.max(0, Math.min(1, outs[y * size + x][0])), 0.15f, 0.15f, 0.7f).getRGB();
				pixels[y * size + x] = new Color((float)NetTrainer.getOutput(outs[y * size + x]) / outs[y * size + x].length, 0.15f, 0.15f, 0.7f).getRGB();
			}
		}
		return new BufferedImage(img.getColorModel(),
								 (WritableRaster)Raster.createRaster(img.getRaster().getSampleModel().createCompatibleSampleModel(size, size), new DataBufferInt(pixels, pixels.length), null),
								 false, null);
	}

	private static class FunctionDrawer extends Canvas {

		private NeuralNet net;
		private double[][] trainingInput;
		private double[][] trainingLabels;
		public boolean background = false;
		public boolean predictions = false;
		private Point2D.Double offset = new Point2D.Double(0, 0);
		private double scale = 1;
		private MouseAdapter events = new MouseAdapter() {

				private Point last = new Point(0, 0);

				@Override
				public void mousePressed(MouseEvent e) {
					last = e.getPoint();
				}

				@Override
				public void mouseDragged(MouseEvent e) {
					offset = new Point2D.Double(offset.x + (e.getX() - last.x) * scale / getWidth(), offset.y + (e.getY() - last.y) * scale / getHeight());
					last = e.getPoint();
					repaint();
				}

				@Override
				public void mouseWheelMoved(MouseWheelEvent e) {
					scale *= Math.pow(2, e.getPreciseWheelRotation() / 10);
					repaint();
				}
			};
		private double realtimeLimit = 20;
		private int limitCooldown = 0;

		public FunctionDrawer(NeuralNet net, double[][] trainingInput, double[][] trainingLabels) {
			this.net = net;
			this.trainingInput = trainingInput;
			this.trainingLabels = trainingLabels;

			addMouseListener(events);
			addMouseMotionListener(events);
			addMouseWheelListener(events);

			addKeyListener(new KeyAdapter() {
					@Override
					public void keyTyped(KeyEvent e) {
						if (e.getKeyChar() == ',') {
							realtimeLimit *= 0.75;
							limitCooldown = 10;
							repaint();
						}
						if (e.getKeyChar() == '.') {
							realtimeLimit *= 4.0 / 3;
							limitCooldown = 10;
							repaint();
						}
					}
				});
		}

		@Override
		public void paint(Graphics g) {
			BufferedImage img = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_ARGB);
			Graphics ig = img.createGraphics();
			ig.clearRect(0, 0, getWidth(), getHeight());
			Rectangle2D.Double bounds = new Rectangle2D.Double(offset.x, offset.y, scale, scale);
			if (background) {
				renderData(ig, trainingInput, trainingLabels, getWidth(), getHeight(), bounds, (int)(realtimeLimit * realtimeLimit));
			}
			if (predictions) {
				ig.drawImage(renderMegaOutput((TrainableOCLNeuralNet)net, (int)realtimeLimit, bounds), 0, 0, getWidth(), getHeight(), null);
//				ig.drawImage(renderOutput(net, (int)realtimeLimit, bounds), 0, 0, getWidth(), getHeight(), null);
			}
			if (limitCooldown > 0) {
				ig.setColor(new Color(1, 1, 1, limitCooldown / 10f));
				ig.drawString("Render Limit: " + (int)realtimeLimit, 20, 40);
				limitCooldown--;
			}
			g.drawImage(img, 0, 0, null);
		}

		@Override
		public void update(Graphics g) {
			paint(g);
		}

	}
}

class Complex implements Comparable<Complex> {
	
	private final double real;
	private final double imag;

	public Complex(double real, double imag) {
		this.real = real;
		this.imag = imag;
	}

	public double magnitude() {
		return Math.sqrt(real * real + imag * imag);
	}

	public Complex add(Complex c) {
		return new Complex(real + c.real, imag + c.imag);
	}

	public Complex mult(double d) {
		return new Complex(real * d, imag * d);
	}

	public Complex mult(Complex c) {
		return new Complex(real * c.real - imag * c.imag, real * c.imag + c.real * imag);
	}

	@Override
	public int compareTo(Complex o) {
		throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
	}
	
}
