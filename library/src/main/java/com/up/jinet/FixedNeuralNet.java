package com.up.jinet;

import com.up.jinet.data.*;
import com.up.jinet.function.activation.ActivationFunction;
import com.up.jinet.function.cost.CostFunction;

/**
 *
 * @author Ricky
 */
public record FixedNeuralNet(FixedNeuronLayer[] layers) implements NeuralNet {

	private static final long serialVersionUID = 1;

    @Override
    public NetOutput<LayerOutput> get(double[] input) {
        for (int i = 0; i < layers.length; i++) {
			input = layers[i].get(input).getOut();
        }
        return new NetOutput<>(new LayerOutput(input));
    }

    @Override
    public FixedNeuronLayer[] getLayers() {
        return layers;
    }
    
    @Override
    public int height() {
        return layers.length;
    }
	
	public double[][][][] toArray() {
		double[][][][] net = new double[layers.length][][][];
        for (int l = 0; l < layers.length; l++) {
            FixedNeuron[] neurons = layers[l].getNeurons();
			net[l] = new double[neurons.length][][];
			for (int n = 0; n < neurons.length; n++) {
				double[] weights = neurons[n].getWeights();
				net[l][n] = new double[2][];
				net[l][n][0] = new double[weights.length];
				System.arraycopy(weights, 0, net[l][n][0], 0, weights.length);
				net[l][n][1] = new double[1];
				net[l][n][1][0] = neurons[n].getBias();
			}
		}
		return net;
	}
	
	public static FixedNeuralNet fromArray(double[][][][] net, ActivationFunction[] acts) {
        FixedNeuronLayer[] layers = new FixedNeuronLayer[net.length];
        for (int l = 0; l < net.length; l++) {
            FixedNeuron[] neurons = new FixedNeuron[net[l].length];
			for (int n = 0; n < neurons.length; n++) {
				neurons[n] = new FixedNeuron(net[l][n][0], net[l][n][1][0]);
			}
			layers[l] = new FixedNeuronLayer(neurons, acts[l]);
		}
		return new FixedNeuralNet(layers);
	}
}
