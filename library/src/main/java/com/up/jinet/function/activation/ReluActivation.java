package com.up.jinet.function.activation;

/**
 *
 * @author Ricky
 */
public class ReluActivation implements ActivationFunction {

	@Override
	public double[] get(double[] xs) {
		double[] ret = new double[xs.length];
        for (int i = 0; i < xs.length; i++) {
			ret[i] = Math.max(0, xs[i]);
		}
		return ret;
	}

	@Override
	public double[] getSimpleDerivative(double[] xs) {
		double[] ret = new double[xs.length];
        for (int i = 0; i < xs.length; i++) {
			ret[i] = xs[i] > 0 ? 1 : 0.0;
		}
		return ret;
	}

	@Override
	public double randomRange(int inputSize, int layerSize) {
//		return 1;
		return Math.sqrt(12d / (inputSize + layerSize));
	}
	
}
