package com.up.jinet.function.cost;

/**
 * Not sure this is quite right still, but it seems to work well with softmax at least, so maybe it's fine.
 * @author Ricky
 */
public class CrossEntropyCost implements CostFunction {
	
	public double get(double[] xs, double[] as) {
		double ret = 0;
        for (int i = 0; i < xs.length; i++) {
			ret += Math.log(xs[i]) * as[i];
		}
		return -ret;
	}

	@Override
	public double getDerivative(double x, double a) {
		return -a / x;
	}
	
}