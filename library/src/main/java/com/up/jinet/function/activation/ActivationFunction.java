package com.up.jinet.function.activation;

import java.io.Serializable;

/**
 *
 * @author Ricky
 */
public interface ActivationFunction extends Serializable {
	
	public double[] get(double[] xs);
	
	public default double[] getSimpleDerivative(double[] xs) {
		return null;
	}
	
	// Some activation derivatives require the full Jacobian matrix. We can simplify this to create a diagonal matrix of the derivative vector though when not needed.
	public default double[][] getDerivative(double[] xs) {
		double[] diag = getSimpleDerivative(xs);
		double[][] ret = new double[xs.length][xs.length];
		for (int i = 0; i < xs.length; i++) {
			for (int j = 0; j < xs.length; j++) {
				ret[i][j] = 0;
			}
			ret[i][i] = diag[i];
		}
		return ret;
	}

	public double randomRange(int inputSize, int layerSize);

	public default double randomWeight(int inputSize, int layerSize) {
		return (Math.random() * 2 - 1) * randomRange(inputSize, layerSize);
	}
	
}
