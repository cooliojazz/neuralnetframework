package com.up.jinet.function.activation;

/**
 *
 * @author Ricky
 */
public class TanhActivation implements ActivationFunction {

	@Override
	public double[] get(double[] xs) {
		double[] ret = new double[xs.length];
        for (int i = 0; i < xs.length; i++) {
			ret[i] = Math.tanh(xs[i]);
		}
		return ret;
	}

	@Override
	public double[] getSimpleDerivative(double[] xs) {
		double[] ret = new double[xs.length];
        for (int i = 0; i < xs.length; i++) {
			ret[i] = 1 - Math.pow(Math.tanh(xs[i]), 2);
		}
		return ret;
	}

	@Override
	public double randomRange(int inputSize, int layerSize) {
		return 4 * Math.sqrt(6d / (inputSize + layerSize));
	}
	
}
