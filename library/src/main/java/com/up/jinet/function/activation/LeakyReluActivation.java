package com.up.jinet.function.activation;

/**
 *
 * @author Ricky
 */
public class LeakyReluActivation implements ActivationFunction {
	
	private double alpha;

	public LeakyReluActivation(double alpha) {
		this.alpha = alpha;
	}

	@Override
	public double[] get(double[] xs) {
		double[] ret = new double[xs.length];
        for (int i = 0; i < xs.length; i++) {
			ret[i] = (xs[i] > 0 ? 1 : -alpha) * xs[i];
		}
		return ret;
	}

	@Override
	public double[] getSimpleDerivative(double[] xs) {
		double[] ret = new double[xs.length];
        for (int i = 0; i < xs.length; i++) {
			ret[i] = xs[i] > 0 ? 1 : -alpha;
		}
		return ret;
	}

	@Override
	public double randomRange(int inputSize, int layerSize) {
		return Math.sqrt(12d / (inputSize + layerSize));
	}
	
}
