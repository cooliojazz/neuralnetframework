package com.up.jinet.function.activation;

/**
 *
 * @author Ricky
 */
public class SwishActivation implements ActivationFunction {

	private final double beta;

	public SwishActivation(double beta) {
		this.beta = beta;
	}

	public double getBeta() {
		return beta;
	}

	@Override
	public double[] get(double[] xs) {
		double[] ret = new double[xs.length];
        for (int i = 0; i < xs.length; i++) {
			ret[i] = xs[i] / (1 + Math.exp(-beta * xs[i]));
		}
		return ret;
	}

	@Override
	public double[] getSimpleDerivative(double[] xs) {
		double[] ret = new double[xs.length];
        for (int i = 0; i < xs.length; i++) {
			ret[i] = (Math.exp(beta * xs[i]) * (1 + Math.exp(beta * xs[i]) + beta * xs[i])) / Math.pow(1 + Math.exp(beta * xs[i]), 2);
		}
		return ret;
	}

	@Override
	public double randomRange(int inputSize, int layerSize) {
		return Math.sqrt(6d / (inputSize + layerSize));
	}
	
}
