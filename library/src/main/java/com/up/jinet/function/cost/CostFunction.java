package com.up.jinet.function.cost;

import java.io.Serializable;

/**
 *
 * @author Ricky
 */
public interface CostFunction extends Serializable {
	
	public double get(double[] xs, double[] as);
	public double getDerivative(double x, double a);
	
}
