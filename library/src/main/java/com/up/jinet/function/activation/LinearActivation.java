package com.up.jinet.function.activation;

/**
 * Essentially a no-op activation function
 * @author Ricky
 */
public class LinearActivation implements ActivationFunction {

	@Override
	public double[] get(double[] xs) {
		return xs;
	}

	@Override
	public double[] getSimpleDerivative(double[] xs) {
		double[] ret = new double[xs.length];
        for (int i = 0; i < xs.length; i++) {
			ret[i] = 1;
		}
		return ret;
	}

	@Override
	public double randomRange(int inputSize, int layerSize) {
		return 1;
	}
	
}
