package com.up.jinet.function.cost;

/**
 *
 * @author Ricky
 */
public class MseCost implements CostFunction {

	public double get(double[] xs, double[] as) {
		double ret = 0;
        for (int i = 0; i < xs.length; i++) {
			ret += Math.pow(as[i] - xs[i], 2) / 2;
		}
		return ret / xs.length;
	}

	@Override
	public double getDerivative(double x, double a) {
        return x - a;
	}
	
}