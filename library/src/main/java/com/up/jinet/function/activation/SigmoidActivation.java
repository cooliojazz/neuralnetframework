package com.up.jinet.function.activation;

/**
 *
 * @author Ricky
 */
public class SigmoidActivation implements ActivationFunction {

	@Override
	public double[] get(double[] xs) {
		double[] ret = new double[xs.length];
        for (int i = 0; i < xs.length; i++) {
			ret[i] = 1 / (1 + Math.exp(-xs[i]));
		}
		return ret;
	}

	@Override
	public double[] getSimpleDerivative(double[] xs) {
		double[] ret = new double[xs.length];
        for (int i = 0; i < xs.length; i++) {
			ret[i] = Math.exp(xs[i]) / Math.pow(1 + Math.exp(xs[i]), 2);
		}
		return ret;
	}

	@Override
	public double randomRange(int inputSize, int layerSize) {
		return Math.sqrt(6d / (inputSize + layerSize));
	}
	
}
