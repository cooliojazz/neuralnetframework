package com.up.jinet.function.cost;

/**
 *
 * @author Ricky
 */
public class MaeCost implements CostFunction {
	
	public double get(double[] xs, double[] as) {
		double ret = 0;
        for (int i = 0; i < xs.length; i++) {
			ret += Math.abs(as[i] - xs[i]);
		}
		return ret / xs.length;
	}

	@Override
	public double getDerivative(double x, double a) {
        return x > 0 ? 1 : -1;
	}
	
}