package com.up.jinet.function.activation;

/**
 * 
 * @author Ricky
 */
public class SoftmaxActivation implements ActivationFunction {

	@Override
	public double[] get(double[] xs) {
		double total = 0;
        for (int i = 0; i < xs.length; i++) {
			total += Math.exp(xs[i]);
		}
		double[] ret = new double[xs.length];
		if (total != 0) { // NaN avoidance, unsure if best way to handle for best training though, since I think this means they could  get suck at zero
			for (int i = 0; i < xs.length; i++) {
				ret[i] = Math.exp(xs[i]) / total;
			}
		}
		return ret;
	}

	// Compute the Jacobian partial derivative matrix for Softmax
	@Override
	public double[][] getDerivative(double[] xs) {
		double[] outs = get(xs);
		double[][] ret = new double[xs.length][xs.length];
        for (int i = 0; i < xs.length; i++) {
			for (int j = 0; j < xs.length; j++) {
				ret[i][j] = outs[j] * ((i == j ? 1 : 0) - outs[i]);
			}
		}
		return ret;
	}

	@Override
	public double randomRange(int inputSize, int layerSize) {
		return Math.sqrt(6d / (inputSize + layerSize));
	}
	
}