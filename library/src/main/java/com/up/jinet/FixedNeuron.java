package com.up.jinet;

/**
 *
 * @author Ricky
 */
public record FixedNeuron(double[] weights, double bias) implements Neuron {

    private static final long serialVersionUID = 1;
    
    public double getInternal(double[] input) {
        if (input.length != weights.length) throw new IndexOutOfBoundsException("Input size must match weight size");
        double total = 0;
        for (int i = 0; i < weights.length; i++) {
            total += weights[i] * input[i];
        }
        return total + bias;
    }
    
    public int size() {
        return weights.length;
    }
    
    public double[] getWeights() {
        return weights;
    }

	public double getBias() {
		return bias;
	}

}
