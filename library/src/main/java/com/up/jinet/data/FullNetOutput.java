package com.up.jinet.data;

/**
 *
 * @author Ricky
 */
public class FullNetOutput<T extends LayerOutput> extends NetOutput<T> {

	private final T[] outs;

	public FullNetOutput(T[] outs) {
		super(outs[outs.length - 1]);
		this.outs = outs;
	}

	public T[] getLayerOutputs() {
		return outs;
	}
	
}
