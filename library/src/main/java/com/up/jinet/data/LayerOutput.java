package com.up.jinet.data;

/**
 *
 * @author Ricky
 */
public class LayerOutput {
	
	private double[] out;

	public LayerOutput(double[] out) {
		this.out = out;
	}

	public double[] getOut() {
		return out;
	}
}
