package com.up.jinet.data;

/**
 *
 * @author Ricky
 */
public class NetOutput<T extends LayerOutput> {

	private final T out;

    public NetOutput(T out) {
        this.out = out;
    }

    public T getOutput() {
		return out;
	}
	
}
