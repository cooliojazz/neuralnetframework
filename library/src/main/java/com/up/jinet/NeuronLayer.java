package com.up.jinet;

import com.up.jinet.data.LayerOutput;
import com.up.jinet.function.activation.ActivationFunction;
import com.up.jinet.function.cost.CostFunction;

import java.io.Serializable;

/**
 *
 * @author Ricky
 */
public interface NeuronLayer extends Serializable {

	public ActivationFunction getActivation();
    
    public LayerOutput get(double[] input);
    
    public int inSize();
    
    public int size();
    
    public Neuron[] getNeurons();
}
