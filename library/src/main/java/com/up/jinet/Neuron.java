package com.up.jinet;

import java.io.Serializable;

/**
 *
 * @author Ricky
 */
public interface Neuron extends Serializable {

    public double getInternal(double[] input);
    
    public int size();
    
    public double[] getWeights();

	public double getBias();
}
