package com.up.jinet;

import com.up.jinet.function.activation.ActivationFunction;

import java.io.*;
import java.util.stream.Stream;

// TODO: Add support for the original nwa (raw weight array) files for easier interoperability between network definitions
public class NetLoader {

    public static <T extends NeuralNet> T load(File file) throws IOException, ClassNotFoundException {
        return load(new FileInputStream(file));
    }

    public static <T extends NeuralNet> T load(InputStream stream) throws IOException, ClassNotFoundException {
        ObjectInputStream os = new ObjectInputStream(stream);
        T net = (T)os.readObject();
        os.close();
        return net;
    }

    public static <T extends NeuralNet> void save(T net, File file) throws IOException {
        save(net, new FileOutputStream(file));
    }

    public static <T extends NeuralNet> void save(T net, OutputStream stream) throws IOException {
        ObjectOutputStream os = new ObjectOutputStream(stream);
        os.writeObject(net);
        os.close();
    }

}
