package com.up.jinet;

import com.up.jinet.data.LayerOutput;
import com.up.jinet.function.activation.ActivationFunction;
import com.up.jinet.function.cost.CostFunction;

/**
 *
 * @author Ricky
 */
public record FixedNeuronLayer(FixedNeuron[] neurons, ActivationFunction act) implements NeuronLayer {

    private static final long serialVersionUID = 1;

	public ActivationFunction getActivation() {
		return act;
	}
    
    public LayerOutput get(double[] input) {
        double[] ret;
        double[] zs = new double[neurons.length];
		
        for (int i = 0; i < neurons.length; i++) {
			zs[i] = neurons[i].getInternal(input);
        }
		
		ret = act.get(zs);

        return new LayerOutput(ret);
    }
    
    public int inSize() {
        return neurons[0].size();
    }
    
    public int size() {
        return neurons.length;
    }
    
    public FixedNeuron[] getNeurons() {
        return neurons;
    }
}
