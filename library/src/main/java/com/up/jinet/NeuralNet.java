package com.up.jinet;

import com.up.jinet.data.NetOutput;

import java.io.Serializable;

/**
 *
 * @author Ricky
 */
public interface NeuralNet extends Serializable {
    
    public NetOutput<?> get(double[] input);
    
    public NeuronLayer[] getLayers();
    
    public int height();

}
