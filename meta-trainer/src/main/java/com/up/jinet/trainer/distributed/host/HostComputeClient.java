package com.up.jinet.trainer.distributed.host;

import com.up.datavis.grapher.DataRendering;
import com.up.datavis.grapher.DataSet;
import com.up.gf.Genome;
import com.up.jinet.trainer.net.NetGene;

import java.awt.*;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.net.Socket;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;

public class HostComputeClient extends HostClient {
    
    private final Info info;
    private final HashMap<Integer, Genome<NetGene>> tasks = new HashMap<>();
    private final DataSet taskProportionData;
    private final DataSet taskRateData;
    private int taskCounter = 0;
    private double taskRate = 0;
    private long dispatchTime = -1;
    
    public HostComputeClient(Info info, Socket connection, ObjectInputStream inStream, DataSet taskProportionData, DataSet taskRateData) throws IOException {
        super(connection, inStream);
        this.info = info;
        this.taskProportionData = taskProportionData;
        this.taskRateData = taskRateData;
    }
    
    public HostComputeClient(Info info, Socket connection, ObjectInputStream inStream) throws IOException {
        this(info, connection, inStream,
             new DataSet(new Rectangle(1, 1), new CopyOnWriteArrayList<>(), info.hostname() + " Task Proportion", DataRendering.DrawType.POINTS_AND_LINES),
             new DataSet(new Rectangle(1, 1), new CopyOnWriteArrayList<>(), info.hostname() + " Task Rate", DataRendering.DrawType.POINTS_AND_LINES));
    }
    
    public Info getInfo() {
        return info;
    }
    
    public HashMap<Integer, Genome<NetGene>> getTasks() {
        return tasks;
    }
    
    public DataSet getTaskProportionData() {
        return taskProportionData;
    }
    
    public DataSet getTaskRateData() {
        return taskRateData;
    }
    
    public int getTaskCounter() {
        return taskCounter;
    }
    
    public void resetTaskCounter() {
        taskCounter = 0;
    }
    
    public void updateTaskCounter(int tasks) {
        taskCounter += tasks;
    }
    
    public double getTaskRate() {
        return taskRate;
    }
    
    public void updateTaskRate(int tasks) {
        this.taskRate = taskRate * 0.9 + tasks * 0.1;
    }
    
    public long getDispatchTime() {
        return dispatchTime;
    }
    
    public void setDispatchTime(long dispatchTime) {
        this.dispatchTime = dispatchTime;
    }
    
    public record Info(UUID id, String hostname, String os, int cores) implements Serializable {}
    
    public record TrainingInfo(double targetTime, int epochs, int batchRatio, double initTrainRate, int correctScale, int maxSize, double sizePenalty) implements Serializable {}
}
