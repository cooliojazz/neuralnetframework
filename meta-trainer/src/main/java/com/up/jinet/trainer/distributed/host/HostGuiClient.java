package com.up.jinet.trainer.distributed.host;

import com.up.datavis.grapher.DataSet;
import com.up.datavis.grapher.DataSetGroup;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.HashSet;
import java.util.stream.StreamSupport;

public class HostGuiClient extends HostClient {
    
    private final HashSet<String> trackedSets = new HashSet<>();
    private final HashSet<String> trackedGroups = new HashSet<>();
    
    public HostGuiClient(Socket connection, ObjectInputStream inStream) throws IOException {
        super(connection, inStream);
    }
    
    public void trackGroup(DataSetGroup group) {
        trackedGroups.add(group.getLabel());
        trackedSets.addAll(StreamSupport.stream(group.sets().spliterator(), false).map(DataSet::getLabel).toList());
    }
    
    public boolean isTrackingSet(String set) {
        return trackedSets.contains(set);
    }
    
    public HashSet<String> getTrackedGroups() {
        return trackedGroups;
    }
}
