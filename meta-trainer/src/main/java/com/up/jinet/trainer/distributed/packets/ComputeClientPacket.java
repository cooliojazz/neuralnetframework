package com.up.jinet.trainer.distributed.packets;

import java.io.Serializable;

public record ComputeClientPacket(Type type, Object... data) implements Serializable {
    
    public enum Type {SAMPLES, INFO, TASKS, NOTIFY_AVAILABLE, TASK_UNAVAILABLE, REQUEST_TASKS, FINISH_TASK, IDENTITY}
}
