package com.up.jinet.trainer.net;

import com.up.gf.Genome;
import com.up.jinet.Neuron;
import com.up.jinet.NeuronLayer;
import com.up.jinet.trainer.GeneticTrainer;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

public class NetGeneVisualiser extends Canvas {
    
    private boolean showInputs;
    private final Supplier<List<Genome<NetGene>>> genomes;
    private FreeformNeuralNet net = null;
    private Genome<NetGene> current = null;
    private int viewIndex = 0;
    private double logScale = 0.1;
    private final double neruonRadius = 6;
    private final double neruonOffset = neruonRadius + 0.5;
    private final int neruonSize = (int)(neruonOffset * 2);
    private Neuron selected = null;
    
    public NetGeneVisualiser(Supplier<List<Genome<NetGene>>> genomes, boolean showInputs) {
        this.genomes = genomes;
        this.showInputs = showInputs;
        setMinimumSize(new Dimension(200, 150));
        
        addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                selected = null;
                if (net != null) {
                    int width = net.getLayers().length;
                    double xres = (double)getWidth() / (width + 1);
                    for (int x = showInputs ? 0 : 1; x < width; x++) {
                        NeuronLayer layer = net.getLayers()[x];
                        double yres = (double)getHeight() / layer.size();
                        for (int y = 0; y < layer.size(); y++) {
                            Neuron n = layer.getNeurons()[y];
                            if (Math.sqrt(Math.pow(Math.round((x + 1.5) * xres) - e.getX(), 2) + Math.pow(Math.round((y + 0.5) * yres) - e.getY(), 2)) < neruonOffset)
                                selected = n;
                        }
                    }
                }
                repaint();
            }
            
        });
        addMouseWheelListener(new MouseAdapter() {
            @Override
            public void mouseWheelMoved(MouseWheelEvent e) {
                logScale *= Math.exp(e.getPreciseWheelRotation() / 5);
                repaint();
            }
        });
        addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                var pop = genomes.get();
                if (e.getKeyChar() == 'w')
                    viewWorst();
                if (e.getKeyChar() == 'b')
                    viewBest();
                if (e.getKeyChar() == ',') {
                    viewIndex = Math.min(pop.size() - 1, Math.max(0, viewIndex - 1));
                    viewGenome(pop.get(viewIndex));
                }
                if (e.getKeyChar() == '.') {
                    viewIndex = Math.min(pop.size() - 1,Math.max(0,  viewIndex + 1));
                    viewGenome(pop.get(viewIndex));
                }
                repaint();
            }
        });
    }
    
    public void viewWorst() {
        viewGenome(genomes.get().stream().min(Comparator.comparingInt(Genome::getFitness)).orElse(null));
    }
    
    public void viewBest() {
        viewGenome(genomes.get().stream().max(Comparator.comparingInt(Genome::getFitness)).orElse(null));
    }
    
    public void viewGenome(Genome<NetGene> genome) {
        current = genome;
        this.net = FreeformNeuralNet.constructFrom(genome);
    }
    
    @Override
    public void paint(Graphics g) {
        BufferedImage img = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_4BYTE_ABGR);
        Graphics ig = img.createGraphics();
        
        ig.setColor(Color.gray.darker());
        ig.fillRect(0, 0, getWidth(), getHeight());
        if (net != null) {
            int width = net.getLayers().length;
            double xRes = (double)getWidth() / (width + 1);
            for (int x = showInputs ? 0 : 1; x < width; x++) {
                FreeformNeuronLayer layer = net.getLayers()[x];
                for (Map.Entry<FreeformNeuron, Double> pn : layer.getRealNeurons().entrySet()) {
                    FreeformNeuron n = pn.getKey();
                    for (FreeformNeuron.PositionedWeight pw : n.getRealWeights()) {
                        double logWeight = Math.log10(Math.abs(pw.weight() / logScale));
                        float alpha = 0.2f + (selected == null ? 0.4f : (n != selected ? 0 : 0.75f));
                        ig.setColor(getColor(pw.weight(), logWeight, logWeight, alpha));
                        ig.drawLine((int)Math.round((x + 1.5) * xRes), (int)Math.round(pn.getValue() * getHeight()),
                                    (int)Math.round(((x - 1) + 1.5) * xRes), (int)Math.round(pw.position() * getHeight()));
                    }
                }
            }
            
            double inYRes = (double)getHeight() / GeneticTrainer.IN_LENGTH;
            ig.setColor(Color.GRAY);
            ig.drawLine((int)Math.round(0.5 * xRes), 0, (int)Math.round(0.5 * xRes), getHeight());
            for (int y = 0; y < GeneticTrainer.IN_LENGTH; y++) {
                ig.setColor(Color.white);
                if (y < 11) ig.setColor(Color.yellow);
                if (y < 9) ig.setColor(Color.blue);
                if (y < 6) ig.setColor(Color.red);
                if (y < 3) ig.setColor(Color.green);
                ig.fillRect((int)Math.round(0.5 * xRes - neruonOffset), (int)Math.round((y + 0.5) * inYRes - neruonOffset), neruonSize, neruonSize);
                ig.setColor(Color.black);
                ig.drawRect((int)Math.round(0.5 * xRes - neruonOffset), (int)Math.round((y + 0.5) * inYRes - neruonOffset), neruonSize, neruonSize);
            }
            for (int x = 0; x < width; x++) {
                ig.setColor(Color.GRAY);
                ig.drawLine((int)Math.round((x + 1.5) * xRes), 0, (int)Math.round((x + 1.5) * xRes), getHeight());
                FreeformNeuronLayer layer = net.getLayers()[x];
                for (Map.Entry<FreeformNeuron, Double> pn : layer.getRealNeurons().entrySet()) {
                    FreeformNeuron n = pn.getKey();
                    double logBias = Math.log10(Math.abs(n.getBias()) / logScale);
                    double maxWeight = 0;
                    double nSum = 0, pSum = 0;
                    for (int i = 0; i < n.size(); i++) {
                        if (Math.log10(Math.abs(n.getWeights()[i]) / logScale) > maxWeight)
                            maxWeight = Math.log10(Math.abs(n.getWeights()[i]) / logScale);
                        
                        if (n.getWeights()[i] < 0)
                            nSum += n.getWeights()[i]; // Based on a bad generalization that most activation functions output within the range [-1,1], most likely meaningless for anything else
                        if (n.getWeights()[i] > 0) pSum += n.getWeights()[i];
                    }
                    double[] stuff = layer.getActivation().get(new double[] {nSum, pSum});
                    ig.setColor(getColor(n.getBias(), logBias, Math.max(Math.log10(Math.abs(stuff[0]) / logScale), Math.log10(Math.abs(stuff[1]) / logScale)), 1));
                    ig.fillOval((int)Math.round((x + 1.5) * xRes - neruonOffset), (int)Math.round(pn.getValue() * getHeight() - neruonOffset), neruonSize, neruonSize);
                    ig.setColor(Color.black);
                    ig.drawOval((int)Math.round((x + 1.5) * xRes - neruonOffset), (int)Math.round(pn.getValue() * getHeight() - neruonOffset), neruonSize, neruonSize);
                }
            }
            
            ig.drawString(current.getFitness() + "", 5, 30);
        }
        
        ig.drawString(logScale + "x", 5, 15);
        g.drawImage(img, 0, 0, null);
    }
    
    private Color getColor(double value, double logValue, double activity, float alpha) {
        double d = 0.3 + Math.min(1, Math.max(0, logValue)) * 0.7;
        double a = Math.min(1, Math.max(0, activity));
        double red = (value < 0 ? d : 0) * (a * 0.7 + 0.3);
        double green = (value > 0 ? d : 0) * (a * 0.7 + 0.3);
        double blue = (1 - d) * a;
        return new Color((float)red, (float)green, (float)blue, alpha);
    }
    
    @Override
    public void update(Graphics g) {
        paint(g);
    }
    
}
