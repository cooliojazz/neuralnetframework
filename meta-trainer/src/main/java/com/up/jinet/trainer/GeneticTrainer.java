package com.up.jinet.trainer;

import com.up.datavis.grapher.DataRendering;
import com.up.datavis.grapher.DataSet;
import com.up.datavis.grapher.DataSetGroup;
import com.up.datavis.grapher.Grapher;
import com.up.gf.Genome;
import com.up.gf.Population;
import com.up.jinet.*;
import com.up.jinet.function.activation.*;
import com.up.jinet.function.cost.CrossEntropyCost;
import com.up.jinet.trainer.net.NetGeneVisualiser;
import com.up.jinet.trainer.net.FreeformNeuralNet;
import com.up.jinet.trainer.net.NetGene;
import com.up.jinet.training.DefaultNetTrainer;
import com.up.jinet.training.NetDataSet;
import com.up.jinet.training.NetTrainer;
import com.up.jinet.training.network.HyperParameters;
import com.up.jinet.training.network.TrainableNeuralNet;

import java.awt.*;
import java.awt.geom.Point2D;
import java.io.File;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.DoubleStream;
import java.util.stream.Stream;

public class GeneticTrainer {

    private static Genome<NetGene> okStart = new Genome<>(new ArrayList<>(Arrays.asList(new NetGene(NetGene.Type.START_NEURON, 0.5, 0.15), new NetGene(NetGene.Type.WEIGHT, 9 / 13d, -0.1), new NetGene(NetGene.Type.WEIGHT, 10 / 13d, -0.1))));
    private static NetDataSet samples = generateSamples(1500);
    
    static Frame f = new Frame();
    static Grapher graph = new Grapher();
    static NetGeneVisualiser nv;
    
    static DataSet devStats = new DataSet(new Rectangle(1, 1), new CopyOnWriteArrayList<>(), "Dev Fitness", DataRendering.DrawType.RANGE_WITH_POINTS);
    static DataSet meanStats = new DataSet(new Rectangle(1, 1), new CopyOnWriteArrayList<>(), "Mean Fitness", DataRendering.DrawType.POINTS_AND_LINES);
    static DataSet medianStats = new DataSet(new Rectangle(1, 1), new CopyOnWriteArrayList<>(), "Median Fitness", DataRendering.DrawType.POINTS_AND_LINES);
    static DataSet firstQuartileStats = new DataSet(new Rectangle(1, 1), new CopyOnWriteArrayList<>(), "First Quartile Fitness", DataRendering.DrawType.POINTS_AND_LINES);
    static DataSet thirdQuartileStats = new DataSet(new Rectangle(1, 1), new CopyOnWriteArrayList<>(), "Third Quartile Fitness", DataRendering.DrawType.POINTS_AND_LINES);
    static DataSet worstStats = new DataSet(new Rectangle(1, 1), new CopyOnWriteArrayList<>(), "Worst Fitness", DataRendering.DrawType.POINTS_AND_LINES);
    static DataSet secondWorstStats = new DataSet(new Rectangle(1, 1), new CopyOnWriteArrayList<>(), "Second Worst Fitness", DataRendering.DrawType.POINTS_AND_LINES);
    static DataSet bestStats = new DataSet(new Rectangle(1, 1), new CopyOnWriteArrayList<>(), "Best Fitness", DataRendering.DrawType.POINTS_AND_LINES);
    static DataSet secondBestStats = new DataSet(new Rectangle(1, 1), new CopyOnWriteArrayList<>(), "Second Best Fitness", DataRendering.DrawType.POINTS_AND_LINES);
    static DataSet thirdBestStats = new DataSet(new Rectangle(1, 1), new CopyOnWriteArrayList<>(), "Third Best Fitness", DataRendering.DrawType.POINTS_AND_LINES);
    static DataSet mutRateStats = new DataSet(new Rectangle(1, 1), new CopyOnWriteArrayList<>(), "Mutation Rate", DataRendering.DrawType.POINTS_AND_LINES);
    static DataSet combRateStats = new DataSet(new Rectangle(1, 1), new CopyOnWriteArrayList<>(), "Combination Rate", DataRendering.DrawType.POINTS_AND_LINES);

    public static void main(String[] args) {
        Population<NetGene> pop = new Population<>(75, 3, 30, NetGene::new, GeneticTrainer::evaluate, 0.6, 0.1);
//        okStart.setFitness(evaluate(okStart));
//        pop.getGenomes().set(0, okStart);
        
        f.add(graph);
        // TODO: Figure out how to resolve differences with old api that didn't require updates and new one that does.
//        nv = new NetGeneVisualiser(pop, true);
//        nv.viewGenome(okStart);
//        nv.setSize(400, 1000);
//        f.add(nv, BorderLayout.WEST);
        f.setSize(1200, 1000);
        f.setVisible(true);
        
        DataSetGroup group = new DataSetGroup(new DataRendering.ColorRange(new Color(0.8f, 0.3f,  0.1f), new Color(0.1f, 0.3f,  0.8f)), "Stats");
        devStats.setPointStyle(new DataRendering.PointStyle(1.5, DataRendering.PointStyle.Type.SQUARE));
        group.addDataSet(devStats);
        firstQuartileStats.setPointStyle(new DataRendering.PointStyle(2.5, DataRendering.PointStyle.Type.SQUARE));
        group.addDataSet(firstQuartileStats);
        medianStats.setPointStyle(new DataRendering.PointStyle(3.5, DataRendering.PointStyle.Type.CIRCLE));
        group.addDataSet(medianStats);
        thirdQuartileStats.setPointStyle(new DataRendering.PointStyle(2.5, DataRendering.PointStyle.Type.SQUARE));
        group.addDataSet(thirdQuartileStats);
        group.addDataSet(meanStats);
        devStats.setAround(meanStats);
        secondWorstStats.setPointStyle(new DataRendering.PointStyle(2.5, DataRendering.PointStyle.Type.SQUARE));
        group.addDataSet(secondWorstStats);
        group.addDataSet(worstStats);
        thirdBestStats.setPointStyle(new DataRendering.PointStyle(2, DataRendering.PointStyle.Type.SQUARE));
        group.addDataSet(thirdBestStats);
        secondBestStats.setPointStyle(new DataRendering.PointStyle(2.5, DataRendering.PointStyle.Type.SQUARE));
        group.addDataSet(secondBestStats);
        group.addDataSet(bestStats);
        group.addDataSet(mutRateStats);
        group.addDataSet(combRateStats);
        graph.addGroup(group);
        
        for (int i = 0; i < 10000; i++) {
            runGeneration(i, pop);
        }
    }
    
    private static void runGeneration(int gen, Population<NetGene> pop) {
        long time = System.nanoTime();
        pop.runGeneration(false);
        pop.mutationRate *= 0.996;
        pop.combinationRate *= 1.001;
        
        int secondWorst = pop.getGenomes().stream().mapToInt(Genome::getFitness).sorted().skip(1).min().orElse(0);
        int worst = pop.getGenomes().stream().mapToInt(Genome::getFitness).min().orElse(0);
        int best = pop.getGenomes().stream().mapToInt(Genome::getFitness).max().orElse(0);
        int secondBest = pop.getGenomes().stream().mapToInt(Genome::getFitness).sorted().limit(pop.getGenomes().size() - 1).max().orElse(0);
        int thirdBest = pop.getGenomes().stream().mapToInt(Genome::getFitness).sorted().limit(pop.getGenomes().size() - 2).max().orElse(0);
        double mean = pop.getGenomes().stream().mapToInt(Genome::getFitness).average().orElse(0);
        int quartile1 = pop.getGenomes().stream().mapToInt(Genome::getFitness).sorted().limit(pop.getGenomes().size() / 4).max().orElse(0);
        int median = pop.getGenomes().stream().mapToInt(Genome::getFitness).sorted().limit(pop.getGenomes().size() / 2).max().orElse(0);
        int quartile3 = pop.getGenomes().stream().mapToInt(Genome::getFitness).sorted().limit(pop.getGenomes().size() * 3 / 4).max().orElse(0);
        double dev = Math.sqrt(pop.getGenomes().stream().mapToInt(Genome::getFitness).mapToDouble(v -> Math.pow(mean - v, 2)).sum());
        
        addDataPoint(secondWorstStats, new Point2D.Double(gen, secondWorst));
        addDataPoint(worstStats, new Point2D.Double(gen, worst));
        addDataPoint(bestStats, new Point2D.Double(gen, best));
        addDataPoint(secondBestStats, new Point2D.Double(gen, secondBest));
        addDataPoint(thirdBestStats, new Point2D.Double(gen, thirdBest));
        addDataPoint(firstQuartileStats, new Point2D.Double(gen, quartile1));
        addDataPoint(medianStats, new Point2D.Double(gen, median));
        addDataPoint(thirdQuartileStats, new Point2D.Double(gen, quartile3));
        addDataPoint(meanStats, new Point2D.Double(gen, mean));
        addDataPoint(devStats, new Point2D.Double(gen, dev / 5));
        addDataPoint(mutRateStats, new Point2D.Double(gen, pop.mutationRate * samples.size() / 10 * 10000));
        addDataPoint(combRateStats, new Point2D.Double(gen, pop.combinationRate * samples.size() / 10 * 10000));
        
//        worstStats.getData().add(new Point2D.Double(gen, worst));
//        worstStats.getBounds().width = gen;
////        if (worst < worstStats.getBounds().y) worstStats.getBounds().y = worst;
//        
//        bestStats.getData().add(new Point2D.Double(gen, best));
//        bestStats.getBounds().width = gen;
//        if (best * 1.1 > bestStats.getBounds().height) bestStats.getBounds().height = (int)(best * 1.1);
//        
//        secondBestStats.getData().add(new Point2D.Double(gen, secondBest));
//        secondBestStats.getBounds().width = gen;
//        if (secondBest > secondBestStats.getBounds().height) secondBestStats.getBounds().height = secondBest;
//        
//        thirdBestStats.getData().add(new Point2D.Double(gen, thirdBest));
//        thirdBestStats.getBounds().width = gen;
//        if (thirdBest > thirdBestStats.getBounds().height) thirdBestStats.getBounds().height = thirdBest;
//        
//        medianStats.getData().add(new Point2D.Double(gen, median));
//        medianStats.getBounds().width = gen;
//        if (median > medianStats.getBounds().height) medianStats.getBounds().height = median;
//        
//        medianStats.getData().add(new Point2D.Double(gen, quartile3));
//        medianStats.getBounds().width = gen;
//        if (quartile3 > medianStats.getBounds().height) medianStats.getBounds().height = quartile3;
//        
//        meanStats.getData().add(new Point2D.Double(gen, mean));
//        meanStats.getBounds().width = gen;
//        if (best > meanStats.getBounds().height) meanStats.getBounds().height = (int)mean;
//        
//        devStats.getData().add(new Point2D.Double(gen, dev / 5));
//        devStats.getBounds().width = gen;
////            if (dev > devStats.getBounds().height) devStats.getBounds().height = (int)dev;
        
        Genome<NetGene> bestG = pop.getGenomes().stream().max(Comparator.comparingInt(Genome::getFitness)).orElse(null);
        if (bestG != null) nv.viewGenome(bestG);
        nv.repaint();
        graph.repaint();
        System.out.println("Completed generation " + gen + " in " + (System.nanoTime() - time) / 1000 / 1000d + "ms.");
        
    }
    
    private static void addDataPoint(DataSet set, Point2D p) {
        set.getData().add(p);
        if (p.getX() > set.getBounds().width) set.getBounds().width = (int)p.getX();
        if (p.getY() * 1.05 > set.getBounds().height) set.getBounds().height = (int)(p.getY() * 1.05);
    }

    private static NetDataSet generateSamples(int count) {
        double[][] inputs = new double[count][2];
//        double[][] outputs = new double[count][1];
        double[][] outputs = new double[count][3];
        for (int i = 0; i < count; i++) {
            inputs[i][0] = Math.random();
            inputs[i][1] = Math.random();
            boolean c1 = Math.sqrt(Math.pow(inputs[i][0] - 0.8, 2) + Math.pow(inputs[i][1] - 0.5, 2)) < 0.4;
            boolean c2 = Math.sqrt(Math.pow(inputs[i][0] - 0.2, 2) + Math.pow(inputs[i][1] - 0.5, 2)) < 0.4;
//            outputs[i][0] = c || c2 ? 1 : 0;
            outputs[i][0] = c1 || c2 ? 0 : 1;
            outputs[i][1] = c1 ? 1d / (c2 ? 2 : 1) : 0;
            outputs[i][2] = c2 ? 1d / (c1 ? 2 : 1) : 0;
        }
        return new NetDataSet(inputs, outputs);
    }

    public static final int IN_LENGTH = 15;
    private static int evaluate(Genome<NetGene> genome) {
        FreeformNeuralNet trainerNet = FreeformNeuralNet.constructFrom(genome);
//        TrainableOCLNeuralNet subjectNet = new TrainableOCLNeuralNet(2, 1, 2 + (int)(Math.random() * 4), 2 + (int)(Math.random() * 1), new SwishActivation(2), new MseCost());
//        TrainableNeuralNet subjectNet = new TrainableNeuralNet(2, 1, 2 + (int)(Math.random() * 3), 2 + (int)(Math.random() * 1), new LeakyReluActivation(0.01), new MseCost());
        TrainableNeuralNet subjectNet = new TrainableNeuralNet(2, 1, 2 + (int)(Math.random() * 3), 2 + (int)(Math.random() * 1), new SoftmaxActivation(), new CrossEntropyCost());
        int netSize = Stream.of(subjectNet.getLayers()).mapToInt(NeuronLayer::size).sum();
//        ArrayDeque<double[]> deque = new ArrayDeque<>();
        double[][] lStats = new double[1][IN_LENGTH];
        File dir = new File("runs/temp-testing-net");
        DefaultNetTrainer trainer = new DefaultNetTrainer(subjectNet, samples.size(), 15, 0, new HyperParameters(samples.size() / 10, 1, 0.0002, 1), true, dir, (t, s) -> {
//        OCLNetTrainer trainer = new OCLNetTrainer(samples.size(), 20, 0, new HyperParameters(samples.size() / 10, 1, 0.01, 0), true, dir, (t, s) -> {
                HyperParameters params = t.getParameters();
//                double[] stats = new double[] {layerMean100, layerMean10, layerMean, layerDMean100, layerDMean10, layerDMean, correct, cost, neuronCount, layerCount};
                double mean = DoubleStream.of(s.layerMean()).sum() / s.layerMean().length;
//                double[] stats = new double[] {deque.stream().parallel().mapToDouble(ds -> ds[2]).sum() / 100, deque.stream().limit(10).mapToDouble(ds -> ds[2]).sum() / 10, mean,
//                                               deque.stream().parallel().mapToDouble(ds -> ds[5]).sum() / 100, deque.stream().limit(10).mapToDouble(ds -> ds[5]).sum() / 10, DoubleStream.of(s.layerStdDeviation()).sum() / s.layerStdDeviation().length,
//                                               deque.stream().parallel().mapToDouble(ds -> ds[8]).sum() / 100, deque.stream().limit(10).mapToDouble(ds -> ds[8]).sum() / 10, mean - (deque.size() > 0 ? deque.peek()[2] : 0),
//                                               s.correct(), s.cost(),
//                                               netSize, subjectNet.height()};
//                deque.push(stats);
//                if (deque.size() > 100) deque.removeLast();
                double dev =  DoubleStream.of(s.layerStdDeviation()).sum() / s.layerStdDeviation().length;
                double delta = mean - lStats[0][2];
                double[] stats = new double[] {lStats[0][0] * 0.99 + mean * 0.01,  lStats[0][1] * 0.9 + mean * 0.1,  mean,
                                               lStats[0][3] * 0.99 + dev * 0.01,   lStats[0][4] * 0.9 + dev * 0.1,   dev,
                                               lStats[0][6] * 0.99 + delta * 0.01, lStats[0][7] * 0.9 + delta * 0.1, delta,
                                               netSize, subjectNet.height(),
                                               s.correct(), s.cost(),
                                               params.trainRatio() - lStats[0][14], params.trainRatio()};
                lStats[0] = stats;
                double[] out = trainerNet.get(stats).getOutput().getOut();
                double ratioChange = out.length > 0 ? out[0] : Math.random() - 0.5;
                t.setParameters(new HyperParameters(params.batchSize(), params.testSize(), params.trainRatio() * ratioChange, 1));
            });
        NetTrainer.NetStatistics stats = trainer.train(samples, samples, false, false, false);
        double correct = Double.isNaN(stats.correct()) ? 0 : stats.correct();
        return Math.max(0, (int)Math.round(correct * 10000));
//        return (int)Math.round(correct * 1000);
    }
    
}
