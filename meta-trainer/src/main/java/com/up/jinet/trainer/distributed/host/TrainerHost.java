package com.up.jinet.trainer.distributed.host;

import com.up.datavis.grapher.DataRendering;
import com.up.datavis.grapher.DataSet;
import com.up.datavis.grapher.DataSetGroup;
import com.up.gf.Genome;
import com.up.gf.Population;
import com.up.jinet.trainer.distributed.packets.ComputeClientPacket;
import com.up.jinet.trainer.distributed.packets.GuiClientPacket;
import com.up.jinet.trainer.net.NetGene;
import com.up.jinet.training.NetDataSet;

import java.awt.*;
import java.awt.geom.Point2D;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Stream;

public class TrainerHost {

    private Genome<NetGene> okStart = new Genome<>(new ArrayList<>(Arrays.asList(new NetGene(NetGene.Type.START_NEURON, 0.5, 0.15), new NetGene(NetGene.Type.WEIGHT, 9 / 13d, -0.1), new NetGene(NetGene.Type.WEIGHT, 10 / 13d, -0.1))));
    private NetDataSet samples = generateSamples(5000);
    
    private HashMap<String, DataSetGroup> groups = new HashMap<>();
    private DataSetGroup fitGroup = new DataSetGroup(new DataRendering.ColorRange(new Color(0.8f, 0.3f, 0.1f), new Color(0.4f, 0.7f, 0.2f)), "Fitness Statistics");
    private DataSet devStats = new DataSet(new Rectangle(1, 1), new CopyOnWriteArrayList<>(), "Dev Fitness", DataRendering.DrawType.RANGE_WITH_POINTS);
    private DataSet meanStats = new DataSet(new Rectangle(1, 1), new CopyOnWriteArrayList<>(), "Mean Fitness", DataRendering.DrawType.POINTS_AND_LINES);
    private DataSet medianStats = new DataSet(new Rectangle(1, 1), new CopyOnWriteArrayList<>(), "Median Fitness", DataRendering.DrawType.POINTS_AND_LINES);
    private DataSet firstQuartileStats = new DataSet(new Rectangle(1, 1), new CopyOnWriteArrayList<>(), "First Quartile Fitness", DataRendering.DrawType.POINTS_AND_LINES);
    private DataSet thirdQuartileStats = new DataSet(new Rectangle(1, 1), new CopyOnWriteArrayList<>(), "Third Quartile Fitness", DataRendering.DrawType.POINTS_AND_LINES);
    private DataSet worstStats = new DataSet(new Rectangle(1, 1), new CopyOnWriteArrayList<>(), "Worst Fitness", DataRendering.DrawType.POINTS_AND_LINES);
    private DataSet secondWorstStats = new DataSet(new Rectangle(1, 1), new CopyOnWriteArrayList<>(), "Second Worst Fitness", DataRendering.DrawType.POINTS_AND_LINES);
    private DataSet bestStats = new DataSet(new Rectangle(1, 1), new CopyOnWriteArrayList<>(), "Best Fitness", DataRendering.DrawType.POINTS_AND_LINES);
    private DataSet secondBestStats = new DataSet(new Rectangle(1, 1), new CopyOnWriteArrayList<>(), "Second Best Fitness", DataRendering.DrawType.POINTS_AND_LINES);
    private DataSet thirdBestStats = new DataSet(new Rectangle(1, 1), new CopyOnWriteArrayList<>(), "Third Best Fitness", DataRendering.DrawType.POINTS_AND_LINES);
    
    private DataSetGroup trainGroup = new DataSetGroup(new DataRendering.ColorRange(new Color(0.3f, 0.8f, 0.1f), new Color(0f, 0.6f, 0.4f)), "Training Info");
    private DataSet mutRateStats = new DataSet(new Rectangle(1, 1), new CopyOnWriteArrayList<>(), "Mutation Rate", DataRendering.DrawType.POINTS_AND_LINES);
    private DataSet combRateStats = new DataSet(new Rectangle(1, 1), new CopyOnWriteArrayList<>(), "Combination Rate", DataRendering.DrawType.POINTS_AND_LINES);
    private DataSet genTimeStats = new DataSet(new Rectangle(1, 1), new CopyOnWriteArrayList<>(), "Generation Time", DataRendering.DrawType.POINTS_AND_LINES);
    
    private DataSetGroup clientGroup = new DataSetGroup(new DataRendering.ColorRange(new Color(0.2f, 0.6f, 0.7f), new Color(0.1f, 0.3f, 0.9f)), "Client Rates");
    
    private ConcurrentLinkedDeque<Genome<NetGene>> tasks = new ConcurrentLinkedDeque<>();
    private ConcurrentHashMap<Integer, Genome<NetGene>> taskIds = new ConcurrentHashMap<>();
    private ConcurrentHashMap<Genome<NetGene>, Integer> results = new ConcurrentHashMap<>();
    
    private HostComputeClient.TrainingInfo trainingInfo = new HostComputeClient.TrainingInfo(15, 400, 5, 0.01, 10000, 20, 0.1);
    
    private ComputeClientManager ccMan = new ComputeClientManager();
    private GuiClientManager gcMan = new GuiClientManager();
    private Population<NetGene> pop;
    
    public void setup() throws IOException {
        ccMan.setup();
        gcMan.setup();
        
        pop = new Population<>(100, 3, trainingInfo.maxSize(), ccMan::evaluate, NetGene::new, 0.2, 0.7);
//        pop = new Population<>(readPopulation(new File("last.gad")), 3, 20, ccMan::evaluate, NetGene::new, 0.25, 0.6);
//        pop.getGenomes().set(0, okStart);
        
        devStats.setPointStyle(new DataRendering.PointStyle(1.5, DataRendering.PointStyle.Type.SQUARE));
        fitGroup.addDataSet(devStats);
        fitGroup.addDataSet(worstStats);
        secondWorstStats.setPointStyle(new DataRendering.PointStyle(2.5, DataRendering.PointStyle.Type.SQUARE));
        fitGroup.addDataSet(secondWorstStats);
        firstQuartileStats.setPointStyle(new DataRendering.PointStyle(2.5, DataRendering.PointStyle.Type.SQUARE));
        fitGroup.addDataSet(firstQuartileStats);
        medianStats.setPointStyle(new DataRendering.PointStyle(3.5, DataRendering.PointStyle.Type.CIRCLE));
        fitGroup.addDataSet(medianStats);
        fitGroup.addDataSet(meanStats);
        devStats.setAround(meanStats);
        thirdQuartileStats.setPointStyle(new DataRendering.PointStyle(2.5, DataRendering.PointStyle.Type.SQUARE));
        fitGroup.addDataSet(thirdQuartileStats);
        thirdBestStats.setPointStyle(new DataRendering.PointStyle(2, DataRendering.PointStyle.Type.SQUARE));
        fitGroup.addDataSet(thirdBestStats);
        secondBestStats.setPointStyle(new DataRendering.PointStyle(2.5, DataRendering.PointStyle.Type.SQUARE));
        fitGroup.addDataSet(secondBestStats);
        fitGroup.addDataSet(bestStats);
        trainGroup.addDataSet(mutRateStats);
        trainGroup.addDataSet(combRateStats);
        trainGroup.addDataSet(genTimeStats);
        
        groups.put(fitGroup.getLabel(), fitGroup);
        groups.put(trainGroup.getLabel(), trainGroup);
        groups.put(clientGroup.getLabel(), clientGroup);
    }
    
    public void start() {
        for (int i = 0; i < 10000; i++) {
            runGeneration(i, pop);
            // TODO: Need to save population records (records as in best? What did I mean by this?)
            try {
                writePopulation(new File("last.gad"), pop);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private NetDataSet generateSamples(int count) {
        double[][] inputs = new double[count][2];
//        double[][] outputs = new double[count][1];
        double[][] outputs = new double[count][3];
        for (int i = 0; i < count; i++) {
            inputs[i][0] = Math.random();
            inputs[i][1] = Math.random();
            boolean c1 = Math.sqrt(Math.pow(inputs[i][0] - 0.8, 2) + Math.pow(inputs[i][1] - 0.5, 2)) < 0.4;
            boolean c2 = Math.sqrt(Math.pow(inputs[i][0] - 0.2, 2) + Math.pow(inputs[i][1] - 0.5, 2)) < 0.4;
//            outputs[i][0] = c || c2 ? 1 : 0;
//            outputs[i][0] = c1 || c2 ? 0 : 1;
//            outputs[i][1] = c1 ? 1d / (c2 ? 2 : 1) : 0;
//            outputs[i][2] = c2 ? 1d / (c1 ? 2 : 1) : 0;
            outputs[i][0] = c1 || c2 ? 0 : 1;
            outputs[i][1] = c1 ? 1 : 0;
            outputs[i][2] = c2 && !c1 ? 1 : 0;
        }
        return new NetDataSet(inputs, outputs);
    }
    
    private void writePopulation(File f, Population<NetGene> pop) throws IOException {
        ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(f));
        os.writeObject(pop.getGenomes());
        os.close();
    }
    
    private Collection<Genome<NetGene>> readPopulation(File f) throws IOException {
        try {
            ObjectInputStream is = new ObjectInputStream(new FileInputStream(f));
            Collection<Genome<NetGene>> pop = (Collection<Genome<NetGene>>)is.readObject();
            is.close();
            return pop;
        } catch (ClassNotFoundException e) {
            throw new IOException("Could not load class for reading object.", e);
        }
    }
    
    private void runGeneration(int gen, Population<NetGene> pop) {
        long time = System.nanoTime();
        pop.runGeneration(false);
        long genTime = System.nanoTime() - time;
//        if (pop.mutationRate > 0.1) pop.mutationRate *= 0.9993;
//        if (pop.combinationRate < 0.5) pop.combinationRate *= 1.0007;
        
        int secondWorst = pop.getGenomes().stream().mapToInt(Genome::getFitness).sorted().skip(1).min().orElse(0);
        int worst = pop.getGenomes().stream().mapToInt(Genome::getFitness).min().orElse(0);
        int best = pop.getGenomes().stream().mapToInt(Genome::getFitness).max().orElse(0);
        int secondBest = pop.getGenomes().stream().mapToInt(Genome::getFitness).sorted().limit(pop.getGenomes().size() - 1).max().orElse(0);
        int thirdBest = pop.getGenomes().stream().mapToInt(Genome::getFitness).sorted().limit(pop.getGenomes().size() - 2).max().orElse(0);
        double mean = pop.getGenomes().stream().mapToInt(Genome::getFitness).average().orElse(0);
        int quartile1 = pop.getGenomes().stream().mapToInt(Genome::getFitness).sorted().limit(pop.getGenomes().size() / 4).max().orElse(0);
        int median = pop.getGenomes().stream().mapToInt(Genome::getFitness).sorted().limit(pop.getGenomes().size() / 2).max().orElse(0);
        int quartile3 = pop.getGenomes().stream().mapToInt(Genome::getFitness).sorted().limit(pop.getGenomes().size() * 3 / 4).max().orElse(0);
        double dev = Math.sqrt(pop.getGenomes().stream().mapToInt(Genome::getFitness).mapToDouble(v -> Math.pow(mean - v, 2)).sum() / pop.getGenomes().size());
        
        addDataPoint(secondWorstStats, new Point2D.Double(gen, secondWorst));
        addDataPoint(worstStats, new Point2D.Double(gen, worst));
        addDataPoint(bestStats, new Point2D.Double(gen, best));
        addDataPoint(secondBestStats, new Point2D.Double(gen, secondBest));
        addDataPoint(thirdBestStats, new Point2D.Double(gen, thirdBest));
        addDataPoint(firstQuartileStats, new Point2D.Double(gen, quartile1));
        addDataPoint(medianStats, new Point2D.Double(gen, median));
        addDataPoint(thirdQuartileStats, new Point2D.Double(gen, quartile3));
        addDataPoint(meanStats, new Point2D.Double(gen, mean));
        addDataPoint(devStats, new Point2D.Double(gen, dev));
        addDataPoint(mutRateStats, new Point2D.Double(gen, pop.mutationRate * samples.size() / 10 * 10000));
        addDataPoint(combRateStats, new Point2D.Double(gen, pop.combinationRate * samples.size() / 10 * 10000));
        addDataPoint(genTimeStats, new Point2D.Double(gen, (genTime / 1000000) * 10));
        
        for (HostComputeClient c : ccMan.getClients()) {
            addDataPoint(c.getTaskProportionData(), new Point2D.Double(gen, c.getTaskCounter() * 10000));
            addDataPoint(c.getTaskRateData(), new Point2D.Double(gen, c.getTaskRate() * 10000));
            c.resetTaskCounter();
        }
        
        try {
            gcMan.sendToAll(new GuiClientPacket(GuiClientPacket.Type.GENOMES, pop.getGenomes()));
        } catch (IOException e) {
            System.err.println("Error sending genomes to clients.");
        }
        System.out.println("Completed generation " + gen + " in " + (System.nanoTime() - time) / 1000 / 1000d + "ms.");
        
    }
    
    private void addDataPoint(DataSet set, Point2D p) {
        set.getData().add(p);
        if (p.getX() > set.getBounds().width) set.getBounds().width = (int)p.getX();
        if (p.getY() * 1.05 > set.getBounds().height) set.getBounds().height = (int)(p.getY() * 1.05);
        for (HostGuiClient c : gcMan.getClients()) {
            if (c.isTrackingSet(set.getLabel())) {
                try {
                    gcMan.sendToClient(c, new GuiClientPacket(GuiClientPacket.Type.UPDATE_SET, set.getLabel(), p, set.getBounds()));
                } catch (IOException e) {
                    System.err.println("Error sending point to client " + c.getConnection().getInetAddress().toString());
                }
            }
        }
    }
    
    class ComputeClientManager {
        
        private ServerSocket socket;
        private final CopyOnWriteArrayList<HostComputeClient> clients = new CopyOnWriteArrayList<>();
        private final ArrayList<HostComputeClient> pastClients = new ArrayList<>();
        
        public CopyOnWriteArrayList<HostComputeClient> getClients() {
            return clients;
        }
        
        public void setup() throws IOException {
            socket = new ServerSocket(47382);
            new Thread(() -> {
                    while (true) {
                        try {
                            Socket con = socket.accept();
                            ObjectInputStream is = new ObjectInputStream(con.getInputStream());
                            HostComputeClient.Info info = initListener(is);
                            HostComputeClient client;
                            HostComputeClient prev = pastClients.stream().filter(c -> c.getInfo().id().equals(info.id())).findAny().orElse(null);
                            if (prev == null) {
                                client = new HostComputeClient(info, con, is);
                                clientGroup.addDataSet(client.getTaskProportionData());
                                clientGroup.addDataSet(client.getTaskRateData());
                            } else {
                                client = new HostComputeClient(info, con, is, prev.getTaskProportionData(), prev.getTaskRateData());
                            }
                            new Thread(listenerThread(client), "Client Listener " + client.hashCode()).start();
                            clients.add(client);
                            sendToClient(client, new ComputeClientPacket(ComputeClientPacket.Type.INFO, trainingInfo));
                            sendToClient(client, new ComputeClientPacket(ComputeClientPacket.Type.SAMPLES, samples));
                            clientPrintln(client, "Client connected: " + info.id() + ", " + info.hostname());
                        } catch (IOException | ClassNotFoundException e) {
                            e.printStackTrace();
                        }
                    }
                }, "Client Acceptor").start();
            new Thread(() -> {
                    while (true) {
                        for (HostComputeClient client : clients) {
                            if (client.getConnection().isClosed() || client.getConnection().isInputShutdown() || client.getConnection().isOutputShutdown()) {
                                cleanupClient(client);
                            } else if (client.getDispatchTime() != -1 && (System.nanoTime() - client.getDispatchTime()) / 1000000 > 60000) {
                                cleanupClient(client);
                            }
                        }
                    }
                }, "Client Cleanup").start();
        }
        
        private int[] evaluate(Genome<NetGene>[] genomes) {
            try {
                tasks = new ConcurrentLinkedDeque<>(Arrays.asList(genomes));
                results = new ConcurrentHashMap<>();
                sendToAll(new ComputeClientPacket(ComputeClientPacket.Type.NOTIFY_AVAILABLE));
                gcMan.sendToAll(new GuiClientPacket(GuiClientPacket.Type.PROGRESS_UPDATE, genomes.length, 0, 0));
                
                while (!tasks.isEmpty() || results.size() < genomes.length) {
                    gcMan.sendToAll(new GuiClientPacket(GuiClientPacket.Type.PROGRESS_UPDATE, genomes.length, results.size() + clients.stream().mapToInt(c -> c.getTasks().size()).sum(), results.size()));
                    try {
                        Thread.sleep(100);
                    } catch (Exception e) {}
                }
                
                return Stream.of(genomes).mapToInt(results::get).toArray();
            } catch (Exception e) {
                e.printStackTrace();
                return new int[genomes.length];
            }
        }
        
        private HostComputeClient.Info initListener(ObjectInputStream is) throws IOException, ClassNotFoundException {
            while (true) {
                ComputeClientPacket packet = (ComputeClientPacket)is.readObject();
                if (packet.type() == ComputeClientPacket.Type.IDENTITY) {
                    return (HostComputeClient.Info)packet.data()[0];
                }
            }
        }
        
        private Runnable listenerThread(HostComputeClient client) {
            return () -> {
                try {
                    while (true) {
                        ComputeClientPacket packet = (ComputeClientPacket)client.getInStream().readObject();
                        receivePacket(client, packet);
                    }
                } catch (Exception e) {
                    cleanupClient(client);
                    e.printStackTrace();
                }
            };
        }
        
        private void cleanupClient(HostComputeClient client) {
            if (!client.getConnection().isClosed()) try {client.getConnection().close();} catch (Exception e2) {}
            clients.remove(client);
            pastClients.add(client);
            if (!client.getTasks().isEmpty()) {
                tasks.addAll(client.getTasks().values());
                try {sendToAll(new ComputeClientPacket(ComputeClientPacket.Type.NOTIFY_AVAILABLE));} catch (Exception e2) {}
            }
            clientPrintln(client, "Client disconnected!");
        }
        
        private void receivePacket(HostComputeClient client, ComputeClientPacket packet) throws IOException {
            if (packet.type() == ComputeClientPacket.Type.REQUEST_TASKS) {
                if (tasks.isEmpty()) {
                    sendToClient(client, new ComputeClientPacket(ComputeClientPacket.Type.TASK_UNAVAILABLE));
                    clientPrintln(client, "Client requested task but there were none.");
                } else {
                    client.updateTaskRate((int)packet.data()[0]);
                    client.getTasks().clear();
                    int i;
                    for (i = 0; i < (int)packet.data()[0] && !tasks.isEmpty(); i++) {
                        Genome<NetGene> genome = nextTask();
                        int id = (int)(Math.random() * Integer.MAX_VALUE);
                        taskIds.put(id, genome);
                        client.getTasks().put(id, genome);
                    }
                    sendToClient(client, new ComputeClientPacket(ComputeClientPacket.Type.TASKS, client.getTasks()));
                    client.setDispatchTime(System.nanoTime());
                    clientPrintln(client, "Client requested " + (int)packet.data()[0] + " tasks and got " + i + ".");
                }
            }
            if (packet.type() == ComputeClientPacket.Type.FINISH_TASK) {
                client.setDispatchTime(-1);
                HashMap<Integer, Integer> clientResults = (HashMap<Integer, Integer>)packet.data()[0];
                clientPrintln(client, "Client returned " + clientResults.size() + "/" + client.getTasks().size() + " results.");
                for (int id : clientResults.keySet()) {
                    Genome<NetGene> genome = taskIds.remove(id);
                    results.put(genome, clientResults.get(id));
                    client.getTasks().remove(id);
                }
                client.updateTaskCounter(clientResults.size());
                // Actually don't think this can happen, it was an attempted patch for what ended up being a different issue
                for (int id : client.getTasks().keySet()) {
                    tasks.add(client.getTasks().get(id));
                }
                clientPrintln(client, "Client finished task.");
            }
        }
        
        private Genome<NetGene> nextTask() {
    //    private synchronized Genome<NetGene> nextTask() {
            return tasks.removeFirst();
        }
        
        private void sendToAll(ComputeClientPacket packet) throws IOException {
            for (HostComputeClient c : clients) sendToClient(c, packet);
        }
        
        private void sendToClient(HostComputeClient client, ComputeClientPacket packet) throws IOException {
            synchronized (client) {
                ObjectOutputStream os = client.getOutStream();
                os.reset();
                os.writeObject(packet);
                os.flush();
            }
        }
        
        private void clientPrintln(HostComputeClient client, String s) {
            System.out.println("[" + client.getInfo().hostname() + " (" + client.getConnection().getInetAddress() + ")] " + s);
        }
    }
    
    class GuiClientManager {
        
        private ServerSocket socket;
        private final CopyOnWriteArrayList<HostGuiClient> clients = new CopyOnWriteArrayList<>();
        
        public CopyOnWriteArrayList<HostGuiClient> getClients() {
            return clients;
        }
        
        public void setup() throws IOException {
            socket = new ServerSocket(47383);
            new Thread(() -> {
                while (true) {
                    try {
                        Socket con = socket.accept();
                        ObjectInputStream is = new ObjectInputStream(con.getInputStream());
                        HostGuiClient client = new HostGuiClient(con, is);
                        new Thread(listenerThread(client), "GUI Client Listener " + client.hashCode()).start();
                        clients.add(client);
                        sendToClient(client, new GuiClientPacket(GuiClientPacket.Type.GROUPS, (Object)groups.keySet().toArray(String[]::new)));
                        sendToClient(client, new GuiClientPacket(GuiClientPacket.Type.GENOMES, pop.getGenomes()));
                        clientPrintln(client, "GUI client connected: " + client.getConnection().getInetAddress().getHostAddress());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }, "GUI Client Acceptor").start();
            new Thread(() -> {
                while (true) {
                    for (HostGuiClient client : clients) {
                        if (client.getConnection().isClosed() || client.getConnection().isInputShutdown() || client.getConnection().isOutputShutdown()) {
                            cleanupClient(client);
                        }
                    }
                }
            }, "GUI Client Cleanup").start();
        }
        
        private Runnable listenerThread(HostGuiClient client) {
            return () -> {
                try {
                    while (true) {
                        GuiClientPacket packet = (GuiClientPacket)client.getInStream().readObject();
                        receivePacket(client, packet);
                    }
                } catch (Exception e) {
                    cleanupClient(client);
                    e.printStackTrace();
                }
            };
        }
        
        private void cleanupClient(HostGuiClient client) {
            if (!client.getConnection().isClosed()) try {client.getConnection().close();} catch (Exception e2) {}
            clients.remove(client);
            clientPrintln(client, "GUI client disconnected!");
        }
        
        private void receivePacket(HostGuiClient client, GuiClientPacket packet) throws IOException {
            if (packet.type() == GuiClientPacket.Type.GROUPS) {
                sendToClient(client, new GuiClientPacket(GuiClientPacket.Type.GROUPS, (Object)groups.keySet().toArray(String[]::new)));
            }
            if (packet.type() == GuiClientPacket.Type.GROUP) {
                String group = (String)packet.data()[0];
                client.trackGroup(groups.get(group));
                sendToClient(client, new GuiClientPacket(GuiClientPacket.Type.GROUP, groups.get(group)));
            }
        }
        
        private void sendToAll(GuiClientPacket packet) throws IOException {
            for (HostGuiClient c : clients) sendToClient(c, packet);
        }
        
        private void sendToClient(HostGuiClient client, GuiClientPacket packet) throws IOException {
            synchronized (client) {
                ObjectOutputStream os = client.getOutStream();
                os.reset();
                os.writeObject(packet);
                os.flush();
            }
        }
        
        private void clientPrintln(HostGuiClient client, String s) {
            System.out.println("[" + client.getConnection().getInetAddress() + "] " + s);
        }
    }
}
