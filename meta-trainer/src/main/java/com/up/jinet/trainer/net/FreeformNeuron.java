package com.up.jinet.trainer.net;

import com.up.jinet.Neuron;

import java.util.ArrayList;
import java.util.List;

public class FreeformNeuron implements Neuron {
    
    //        private int inSize;
    private double bias;
    private ArrayList<PositionedWeight> weights;
    
    public FreeformNeuron(double bias, ArrayList<PositionedWeight> weights) {
//            this.inSize = inSize;
        this.bias = bias;
        this.weights = weights;
    }
    
    @Override
    public double getInternal(double[] input) {
        double sum = bias;
        if (input.length > 0) {
            for (PositionedWeight weight : weights) {
                sum += input[Math.max(Math.min((int)Math.round(weight.position * input.length), input.length - 1), 0)] * weight.weight;
            }
        }
        return sum;
    }
    
    @Override
    public int size() {
        return weights.size();
    }
    
    @Override
    public double[] getWeights() {
        return weights.stream().mapToDouble(PositionedWeight::weight).toArray();
    }
    
    public List<PositionedWeight> getRealWeights() {
        return weights;
    }
    
    @Override
    public double getBias() {
        return bias;
    }
    
    public record PositionedWeight(double position, double weight) {
        
    }
}
