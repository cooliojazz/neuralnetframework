package com.up.jinet.trainer.net;

import com.up.gf.Genome;
import com.up.jinet.NeuralNet;
import com.up.jinet.data.LayerOutput;
import com.up.jinet.data.NetOutput;
import com.up.jinet.function.activation.SwishActivation;
import com.up.jinet.trainer.GeneticTrainer;

import java.util.ArrayList;
import java.util.HashMap;

public class FreeformNeuralNet implements NeuralNet {
    
    private FreeformNeuronLayer[] layers;
    
    public FreeformNeuralNet(FreeformNeuronLayer[] layers) {
        this.layers = layers;
    }
    
    // TODO: Need some mechanism to fit to a desired output size
    @Override
    public NetOutput<LayerOutput> get(double[] input) {
        LayerOutput output = layers[0].get(input);
        for (int i = 0; i < height(); i++) {
            output = layers[i].get(output.getOut());
        }
        return new NetOutput<>(output);
    }
    
    @Override
    public FreeformNeuronLayer[] getLayers() {
        return layers;
    }
    
    @Override
    public int height() {
        return layers.length;
    }
    
    public static FreeformNeuralNet constructFrom(Genome<NetGene> genome) {
        ArrayList<FreeformNeuronLayer> layers = new ArrayList<>();
        HashMap<FreeformNeuron, Double> neurons = new HashMap<>();
        ArrayList<FreeformNeuron.PositionedWeight> weights = null;
        double pos = 0.5;
        double bias = 0;
        for (NetGene gene : genome.getAlleles()) {
            switch (gene.getType()) {
                case START_LAYER -> {
                    // TODO: Probably should add activation customization to the genome?
                    if (weights != null) {
                        neurons.put(new FreeformNeuron(bias, weights), pos);
                    }
                    weights = new ArrayList<>();
                    pos = 0.5;
                    bias = 0;
                    
                    layers.add(new FreeformNeuronLayer(neurons, new SwishActivation(1.75), layers.size() > 0 ? layers.getLast().size() : GeneticTrainer.IN_LENGTH));
                    neurons = new HashMap<>();
                }
                case START_NEURON -> {
                    if (weights != null) {
                        neurons.put(new FreeformNeuron(bias, weights), pos);
                    }
                    weights = new ArrayList<>();
                    pos = gene.getLocation();
                    bias = gene.getData();
                }
                case WEIGHT -> {
                    if (weights == null) {
                        weights = new ArrayList<>();
                    }
                    weights.add(new FreeformNeuron.PositionedWeight(gene.getLocation(), gene.getData()));
                }
                case JUNK -> {
                    // Do something mildly bad to discourage accumulation, or just do nothing to not punish having safety buffers?
                }
            }
        }
        if (weights != null) {
            neurons.put(new FreeformNeuron(bias, weights), pos);
        }
        layers.add(new FreeformNeuronLayer(neurons, new SwishActivation(1.75), layers.size() > 0 ? layers.getLast().size() : GeneticTrainer.IN_LENGTH));
        return new FreeformNeuralNet(layers.toArray(FreeformNeuronLayer[]::new));
    }
}
