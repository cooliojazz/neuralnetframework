package com.up.jinet.trainer.distributed.packets;

import java.io.Serializable;

public record GuiClientPacket(Type type, Object... data) implements Serializable {
    
    public enum Type {GROUPS, GROUP, GENOMES, UPDATE_SET, PROGRESS_UPDATE}
}
