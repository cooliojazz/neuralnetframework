package com.up.jinet.trainer.distributed;

import com.up.jinet.trainer.distributed.client.TrainerComputeClient;
import com.up.jinet.trainer.distributed.client.TrainerGuiClient;
import com.up.jinet.trainer.distributed.host.TrainerHost;

import java.io.IOException;
import java.util.concurrent.ForkJoinPool;

public class DistributedGeneticTrainerRunner {
    
    public static void main(String[] args) {
        System.out.println(ForkJoinPool.commonPool().getParallelism()); 
        switch (args[0]) {
            case "host" -> {
                try {
                    TrainerHost host = new TrainerHost();
                    host.setup();
                    host.start();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            case "client" -> {
                TrainerComputeClient client = new TrainerComputeClient();
                while (true) {
                    try {
                        if (!client.isRunning()) client.start(args[1], Integer.parseInt(args[2]), args.length > 3 ? args[3].equals("enableOcl") : false);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    try {Thread.sleep(10000);} catch (Exception e) {}
                }
            }
            case "gui" -> {
                TrainerGuiClient client = new TrainerGuiClient();
                try {
                    client.start(args[1]);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            default -> System.err.println("Invalid run type specified: " + args[0] + ". Must be one of [host, client, gui].");
        }
    }
    
}
