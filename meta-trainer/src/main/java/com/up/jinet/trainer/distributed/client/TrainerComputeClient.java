package com.up.jinet.trainer.distributed.client;

import com.up.gf.Genome;
import com.up.jinet.NeuralNet;
import com.up.jinet.NeuronLayer;
import com.up.jinet.function.activation.ActivationFunction;
import com.up.jinet.function.activation.SoftmaxActivation;
import com.up.jinet.function.activation.SwishActivation;
import com.up.jinet.function.cost.CrossEntropyCost;
import com.up.jinet.trainer.distributed.host.HostComputeClient;
import com.up.jinet.training.OCLNetTrainer;
import com.up.jinet.training.network.TrainableOCLNeuralNet;
import com.up.jinet.trainer.GeneticTrainer;
import com.up.jinet.trainer.distributed.packets.ComputeClientPacket;
import com.up.jinet.trainer.net.FreeformNeuralNet;
import com.up.jinet.trainer.net.NetGene;
import com.up.jinet.training.DefaultNetTrainer;
import com.up.jinet.training.NetDataSet;
import com.up.jinet.training.NetTrainer;
import com.up.jinet.training.network.HyperParameters;
import com.up.jinet.training.network.TrainableNeuralNet;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.HashMap;
import java.util.NoSuchElementException;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.function.BiConsumer;
import java.util.stream.DoubleStream;
import java.util.stream.Stream;

public class TrainerComputeClient {
    
    // TODO: Sometimes still gets stuck; maybe it should just kill it's current task and move on if it takes something like more than 10x the target time?
    
    private int taskAmount;
    private Socket socket;
    private ObjectOutputStream os;
    private HostComputeClient.TrainingInfo info = null;
    private NetDataSet samples = null;
    private ConcurrentLinkedDeque<Integer> tasksIds = new ConcurrentLinkedDeque<>();
    private HashMap<Integer, Genome<NetGene>> tasks = new HashMap<>();
    private ConcurrentHashMap<Integer, Integer> asyncResults = new ConcurrentHashMap<>();
    private boolean waiting = false;
    private boolean requested = false;
    private boolean running = false;
    private final UUID id = UUID.randomUUID();
    private long requestTime = -1;
    private int cpuCounter = 0;
    private int oclCounter = 0;
    
    public void start(String host, int taskAmount, boolean useOcl) throws IOException {
        this.taskAmount = taskAmount;
        socket = new Socket(host, 47382);
        os = new ObjectOutputStream(socket.getOutputStream());
        sendToServer(new ComputeClientPacket(ComputeClientPacket.Type.IDENTITY, new HostComputeClient.Info(id, InetAddress.getLocalHost().getHostName(), System.getProperty("os.name"), Runtime.getRuntime().availableProcessors())));
        running = true;
        Thread listenerThread = new Thread(this::listenerThread, "Server Listener");
        listenerThread.start();
        Thread taskThread = new Thread(this::taskManagementThread, "Task Management");
        taskThread.start();
        Thread cpuTaskThread = new Thread(this::cpuTaskThread, "CPU Task Runner");
        cpuTaskThread.start();
        if (useOcl) {
            Thread oclTaskThread = new Thread(this::oclTaskThread, "OCL Task Runner");
            oclTaskThread.start();
        }
    }
    
    public void stop() {
        running = false;
        waiting = false;
        requested = false;
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        samples = null;
        tasks.clear();
    }
    
    public boolean isRunning() {
        return running;
    }
    
    private void listenerThread() {
        try {
            ObjectInputStream is = new ObjectInputStream(socket.getInputStream());
            while (running) {
//                try {
                    ComputeClientPacket packet = (ComputeClientPacket)is.readObject();
                    receivePacket(packet);
//                } catch (IOException e) {
//                    // TODO: Tell server packet failed and reset state?
//                    // Just resetting works for now, but it would be more efficient to find some way to handle just the failed packet instead of recreating the whole connection,
//                    e.printStackTrace();
//                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            stop();
        }
    }
    
    private void taskManagementThread() {
        try {
            while (info == null || samples == null) {
                Thread.sleep(100);
            }
            while (running) {
                if (!waiting) {
                    if (tasks.isEmpty()) {
                        if (!requested) {
                            sendToServer(new ComputeClientPacket(ComputeClientPacket.Type.REQUEST_TASKS, taskAmount));
                            requested = true;
                        } else if ((System.nanoTime() - requestTime) / 1000000000d > 10) {
                            // TODO: This patches the server sometimes not sending a response back to some requests, but the underlying reason for that still needs investigation.
                            System.out.println("Timed out waiting for response, rejecting request.");
                            requested = false;
                        }
                    } else {
                        HashMap<Integer, Integer> results = new HashMap<>();
                        asyncResults.clear();
                        long time = System.nanoTime();
//                        for (int id : tasks.keySet()) {
//                            results.put(id, evaluate(tasks.get(id), samples));
//                        }
                        while (!tasksIds.isEmpty() || asyncResults.size() < tasks.size()) {
                            try {Thread.sleep(10);} catch (Exception e) {}
                        }
                        results.putAll(asyncResults);
                        time = System.nanoTime() - time;
                        System.out.println("Ran " + tasks.size() + " tasks in " + time / 1000000 / 1000d + " seconds (avg. " + time / tasks.size() / 1000 / 1000d + "ms/task).");
                        System.out.println("Current stats: cpu - " + cpuCounter + ", ocl - " + oclCounter);
                        sendToServer(new ComputeClientPacket(ComputeClientPacket.Type.FINISH_TASK, results));
                        System.out.println("Sent result.");
                        // Try to adjust task amount until it is processing an amount that takes it approximately one second
                        if (tasks.size() == taskAmount) {
                            if (time / 1000000d < info.targetTime() * 0.9 * 1000) {
                                taskAmount++;
                            } else if (time / 1000000d > info.targetTime() * 1.1 * 1000) {
                                if (taskAmount > 1) taskAmount--;
                            }
                        }
                        tasks.clear();
                    }
                }
                Thread.sleep(100);
            }
        } catch (Exception e) {
            e.printStackTrace();
            stop();
        }
    }
    
    private void cpuTaskThread() {
        while (running) {
            if (!tasksIds.isEmpty()) {
                try {
                    Integer id = tasksIds.pop();
                    asyncResults.put(id, evaluate(tasks.get(id), samples));
                    cpuCounter++;
                } catch (NoSuchElementException ex) { }
            }
        }
    }
    
    private void oclTaskThread() {
        while (running) {
            if (!tasksIds.isEmpty()) {
                try {
                    Integer id = tasksIds.pop();
                    asyncResults.put(id, evaluateOCL(tasks.get(id), samples));
                    oclCounter++;
                } catch (NoSuchElementException ex) { }
            }
        }
    }
    
    private void receivePacket(ComputeClientPacket packet) throws IOException {
        if (packet.type() == ComputeClientPacket.Type.NOTIFY_AVAILABLE) {
            waiting = false;
//            requested = false; // Shouldn't be necessary here, but resets it just in case
            System.out.println("Server has tasks.");
        }
        if (packet.type() == ComputeClientPacket.Type.TASK_UNAVAILABLE) {
            waiting = true;
            requested = false;
            System.out.println("Server did not have tasks.");
        }
        if (packet.type() == ComputeClientPacket.Type.TASKS) {
            requested = false;
//            taskId = (int)packet.data()[0];
//            task = (Genome<NetGene>)packet.data()[1];
            tasks = (HashMap<Integer, Genome<NetGene>>)packet.data()[0];
            if (!tasksIds.isEmpty()) throw new RuntimeException("Taskids not empty yet!!");
            tasksIds.addAll(tasks.keySet());
            System.out.println("Server provided " + tasks.size() + " task(s).");
        }
        if (packet.type() == ComputeClientPacket.Type.INFO) {
            info = (HostComputeClient.TrainingInfo)packet.data()[0];
            System.out.println("Server training info.");
        }
        if (packet.type() == ComputeClientPacket.Type.SAMPLES) {
            samples = (NetDataSet)packet.data()[0];
            System.out.println("Server provided samples.");
        }
    }
    
    private void sendToServer(ComputeClientPacket packet) throws IOException {
        requestTime = System.nanoTime();
        os.reset();
        os.writeObject(packet);
        os.flush();
    }
    
    private int evaluate(Genome<NetGene> genome, NetDataSet samples) {
        FreeformNeuralNet trainerNet = FreeformNeuralNet.constructFrom(genome);
        TrainableNeuralNet subjectNet = new TrainableNeuralNet(2, 3, 4 + (int)(Math.random() * 4), 2 + (int)(Math.random() * 1), new ActivationFunction[] {new SwishActivation(1.75), new SoftmaxActivation()}, new CrossEntropyCost());
        DefaultNetTrainer trainer = new DefaultNetTrainer(subjectNet, samples.size(), info.epochs(), 0, new HyperParameters(samples.size() / info.batchRatio(), 0, info.initTrainRate(), 1), false, null, setupUpdater(subjectNet, trainerNet));
        return score(genome, trainer.train(samples, samples, false, false, false));
    }
    
    private int evaluateOCL(Genome<NetGene> genome, NetDataSet samples) {
        FreeformNeuralNet trainerNet = FreeformNeuralNet.constructFrom(genome);
        TrainableOCLNeuralNet subjectNet = new TrainableOCLNeuralNet(2, 3, 4 + (int)(Math.random() * 4), 2 + (int)(Math.random() * 1), new ActivationFunction[] {new SwishActivation(1.75), new SoftmaxActivation()}, new CrossEntropyCost());
        OCLNetTrainer trainer = new OCLNetTrainer(subjectNet, samples.size(), info.epochs(), 0, new HyperParameters(samples.size() / info.batchRatio(), 0, info.initTrainRate(), 1), true, null, setupUpdater(subjectNet, trainerNet));
        return score(genome, trainer.train(samples, samples, false, false, false));
    }
    
    private int score(Genome<NetGene> genome, NetTrainer.NetStatistics stats) {
        double correct = Double.isNaN(stats.correct()) ? 0 : stats.correct();
        return Math.max(0, (int)Math.round(correct * info.correctScale() * (1 - (double)genome.getSize() / info.maxSize() * info.sizePenalty())));
    }
    
    private <T extends NetTrainer<?, ?>> BiConsumer<T, NetTrainer.NetStatistics> setupUpdater(NeuralNet subjectNet, NeuralNet trainerNet) {
        int netSize = Stream.of(subjectNet.getLayers()).mapToInt(NeuronLayer::size).sum();
        double[][] lStats = new double[1][GeneticTrainer.IN_LENGTH];
        return (t, s) -> {
                HyperParameters params = t.getParameters();
                double mean = DoubleStream.of(s.layerMean()).sum() / s.layerMean().length;
                double dev = DoubleStream.of(s.layerStdDeviation()).sum() / s.layerStdDeviation().length;
                double dMean = mean - lStats[0][2];
                double[] stats = new double[] {lStats[0][0] * 0.99 + mean * 0.01, lStats[0][1] * 0.9 + mean * 0.1, mean,
                                               lStats[0][3] * 0.99 + dev * 0.01, lStats[0][4] * 0.9 + dev * 0.1, dev,
                                               lStats[0][6] * 0.99 + dMean * 0.01, lStats[0][7] * 0.9 + dMean * 0.1, dMean,
                                               netSize, subjectNet.height(),
                                               s.correct(), s.cost(),
                                               params.trainRatio() - lStats[0][14], params.trainRatio()};
                lStats[0] = stats;
                double[] out = trainerNet.get(stats).getOutput().getOut();
                double ratioChange = out.length > 0 ? out[0] : Math.random() - 0.5;
                t.setParameters(new HyperParameters(params.batchSize(), params.testSize(), params.trainRatio() * ratioChange, 1));
            };
    }
    
}
