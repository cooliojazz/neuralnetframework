package com.up.jinet.trainer.distributed.host;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class HostClient {
    
    private final Socket connection;
    private final ObjectInputStream inStream;
    private final ObjectOutputStream outStream;
    
    public HostClient(Socket connection, ObjectInputStream inStream) throws IOException {
        this.connection = connection;
        this.inStream = inStream;
        this.outStream = new ObjectOutputStream(connection.getOutputStream());
    }
    
    public Socket getConnection() {
        return connection;
    }
    
    public ObjectInputStream getInStream() {
        return inStream;
    }
    
    public ObjectOutputStream getOutStream() {
        return outStream;
    }
}
