package com.up.jinet.trainer.net;

import com.up.gf.Gene;

import java.io.Serializable;
import java.util.Objects;

public class NetGene implements Gene<NetGene>, Serializable {
    
    private Type type;
    private double location;
    private double data;
    
    public NetGene() {
    }
    
    public NetGene(Type type, double location, double data) {
        this.type = type;
        this.location = location;
        this.data = data;
    }
    
    public Type getType() {
        return type;
    }
    
    public double getLocation() {
        return location;
    }
    
    public double getData() {
        return data;
    }
    
    private static final Type[] weightedTypes = new Type[] {Type.START_LAYER,
                                                            Type.START_NEURON, Type.START_NEURON, Type.START_NEURON,
                                                            Type.WEIGHT, Type.WEIGHT, Type.WEIGHT, Type.WEIGHT, Type.WEIGHT, Type.WEIGHT, Type.WEIGHT,
                                                            Type.JUNK, Type.JUNK};
    
    @Override
    public void mutate(double rate) {
        if (Math.random() < rate) {
            type = weightedTypes[(int)(Math.random() * weightedTypes.length)];
        }
//        if (Math.random() < rate) {
//            location = Math.max(0, Math.min(1, location + Math.random() - 0.5));
//        }
//        if (Math.random() < rate) {
//            data += Math.random() - 0.5;
//        }
        location = Math.max(0, Math.min(1, location + (Math.random() - 0.5) * rate));
        data += (Math.random() - 0.5) * rate;
    }
    
    @Override
    public void randomReset() {
        type = weightedTypes[(int)(Math.random() * weightedTypes.length)];
        location = Math.random();
        data = Math.random() - 0.5;
    }
    
    @Override
    public double distance(NetGene gene) {
        return (gene.type == type ? 0 : 10) + Math.sqrt(Math.pow(gene.location - location, 2) + Math.pow(gene.data - data, 2));
    }
    
    @Override
    public NetGene clone() {
        return new NetGene(type, location, data);
    }
    
    @Override
    public boolean equals(Object o) {
        if (o instanceof NetGene ng) {
            return location == ng.location && data == ng.data && type == ng.type;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(type, location, data);
    }
    
    public enum Type {
        START_LAYER, START_NEURON, WEIGHT, JUNK
    }
}
