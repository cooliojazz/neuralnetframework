package com.up.jinet.trainer.distributed.client;

import com.up.datavis.grapher.DataSet;
import com.up.datavis.grapher.DataSetGroup;
import com.up.datavis.grapher.Grapher;
import com.up.gf.Genome;
import com.up.jinet.trainer.distributed.packets.GuiClientPacket;
import com.up.jinet.trainer.net.NetGene;
import com.up.jinet.trainer.net.NetGeneVisualiser;

import java.awt.*;
import java.awt.geom.Point2D;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Collections;
import java.util.List;
import java.util.stream.StreamSupport;

public class TrainerGuiClient {
    
    private Socket socket;
    private ObjectOutputStream os;
    private Frame f = new Frame();
    private Grapher graph = new Grapher();
    private ProgressDisplay progress = new ProgressDisplay();
    private NetGeneVisualiser viewer;
    private boolean running = false;
    private String[] availableGroups = new String[0];
    private List<Genome<NetGene>> genomes = Collections.singletonList(new Genome<>(0, 20, NetGene::new));
    
    public TrainerGuiClient() {
        Panel p = new Panel(new BorderLayout());
        p.add(graph);
        progress.setSize(800, 30);
        p.add(progress, BorderLayout.NORTH);
        f.add(p);
        viewer = new NetGeneVisualiser(() -> genomes, true);
        viewer.setSize(400, 1000);
        f.add(viewer, BorderLayout.WEST);
        f.setSize(1200, 1000);
        f.setVisible(true);
    }
    
    public void start(String host) throws IOException {
        socket = new Socket(host, 47383);
        os = new ObjectOutputStream(socket.getOutputStream());
        running = true;
        Thread listenerThread = new Thread(this::listenerThread, "Server Listener");
        listenerThread.start();
    }
    
    private void listenerThread() {
        try {
            ObjectInputStream is = new ObjectInputStream(socket.getInputStream());
            while (running) {
                GuiClientPacket packet = (GuiClientPacket)is.readObject();
                receivePacket(packet);
            }
        } catch (Exception e) {
            e.printStackTrace();
//            reconnect();
        }
    }
    
    private void receivePacket(GuiClientPacket packet) throws IOException {
        switch (packet.type()) {
            case GuiClientPacket.Type.GROUPS -> {
                availableGroups = (String[])packet.data()[0];
                for (String g : availableGroups) {
                    sendToServer(new GuiClientPacket(GuiClientPacket.Type.GROUP, g));
                }
            }
            case GuiClientPacket.Type.GROUP -> {
                graph.addGroup((DataSetGroup)packet.data()[0]);
            }
            case GuiClientPacket.Type.UPDATE_SET -> {
                DataSet set = graph.getGroups().stream().flatMap(g -> StreamSupport.stream(g.sets().spliterator(), false)).filter(s -> s.getLabel().equals(packet.data()[0])).findFirst().orElse(null);
                if (set != null) {
                    set.getData().add((Point2D)packet.data()[1]);
                    set.setBounds((Rectangle)packet.data()[2]);
                }
                graph.repaint();
            }
            case GuiClientPacket.Type.GENOMES -> {
                genomes = (List<Genome<NetGene>>)packet.data()[0];
                viewer.viewBest();
                viewer.repaint();
            }
            case GuiClientPacket.Type.PROGRESS_UPDATE -> {
                progress.total = (int)packet.data()[0];
                progress.sent = (int)packet.data()[1];
                progress.complete = (int)packet.data()[2];
                progress.repaint();
            }
        }
    }
    
    private void sendToServer(GuiClientPacket packet) throws IOException {
        os.reset();
        os.writeObject(packet);
        os.flush();
    }
    
    private static class ProgressDisplay extends Canvas {
        
        public int total = 0;
        public int sent = 0;
        public int complete = 0;
        
        @Override
        public void paint(Graphics g) {
            g.setColor(Color.white);
            g.fillRect(0, 0, getWidth(), getHeight());
            g.setColor(Color.blue.darker());
            g.fillRect(0, 0, sent * getWidth() / total, getHeight());
            g.setColor(Color.green.darker());
            g.fillRect(0, 0, complete * getWidth() / total, getHeight());
        }
        
        @Override
        public void update(Graphics g) {
            paint(g);
        }
    }
}
