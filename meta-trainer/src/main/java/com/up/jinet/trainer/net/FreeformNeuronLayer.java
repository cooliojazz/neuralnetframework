package com.up.jinet.trainer.net;

import com.up.jinet.Neuron;
import com.up.jinet.NeuronLayer;
import com.up.jinet.data.LayerOutput;
import com.up.jinet.function.activation.ActivationFunction;

import java.util.Map;
import java.util.TreeMap;

public class FreeformNeuronLayer implements NeuronLayer {
    
    private final int inSize;
    private final Map<FreeformNeuron, Double> neurons;
    private ActivationFunction activation;
    
    public FreeformNeuronLayer(Map<FreeformNeuron, Double> neurons, ActivationFunction activation, int inSize) {
        this.neurons = neurons;
        this.activation = activation;
        this.inSize = inSize;
    }
    
    @Override
    public ActivationFunction getActivation() {
        return activation;
    }
    
    @Override
    public LayerOutput get(double[] input) {
        TreeMap<Double, Double> posOut = new TreeMap<>();
        for (FreeformNeuron neuron : neurons.keySet()) {
            posOut.put(neurons.get(neuron), neuron.getInternal(input));
        }
        double[] out = new double[neurons.size()];
        for (int i = 0; i < size(); i++) {
            double pos = (double)i / size();
            Double lower = 0d;
            Double upper = 0d;
            for (Double d : posOut.keySet()) {
                if (d > lower && d < pos) lower = d;
                if (d < upper && d > pos) upper = d;
            }
            // TODO: Make cubic hermite spline?
            out[i] = lerp(posOut.getOrDefault(lower, 0d), posOut.getOrDefault(upper, 0d), upper - lower != 0 ? (pos - lower) / (upper - lower) : 0);
        }
        return new LayerOutput(out);
    }
    
    private double lerp(double a, double b, double t) {
        return a + t * (b - a);
    }
    
    @Override
    public int inSize() {
        return inSize;
    }
    
    @Override
    public int size() {
        return neurons.size();
    }
    
    @Override
    public Neuron[] getNeurons() {
        return neurons.keySet().toArray(new FreeformNeuron[0]);
    }
    
    public Map<FreeformNeuron, Double> getRealNeurons() {
        return neurons;
    }
}
